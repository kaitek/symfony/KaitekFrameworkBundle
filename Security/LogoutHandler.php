<?php
namespace Kaitek\Bundle\FrameworkBundle\Security;

use Symfony\Component\Security\Http\Logout\LogoutHandlerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

class LogoutHandler implements LogoutHandlerInterface
{
    protected $csrf_provider;

    public function __construct($csrf_provider) {
        $this->csrf_provider = $csrf_provider;
    }

    public function logout(Request $request, Response $response, TokenInterface $token)
    {
        if($request->getMethod() === Request::METHOD_POST && $response->getStatusCode() == "102"){
            $csrfToken = $this->csrf_provider->getToken('authenticate')->getValue();

            $response->setData(array('success' => true, '_csrf_token'=>$csrfToken));
            $response->setStatusCode("200");
        }
        return $response;
    }
}
