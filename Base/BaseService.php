<?php

namespace Kaitek\Bundle\FrameworkBundle\Base;

use Doctrine\Common\Annotations\AnnotationReader;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

abstract class BaseService //extends ContainerAwareTrait
{  
    use ContainerAwareTrait;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }
}
