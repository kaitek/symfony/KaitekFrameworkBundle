<?php
namespace Kaitek\Bundle\FrameworkBundle\EventListener;

use Kaitek\Bundle\FrameworkBundle\Base\BaseService;
use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTCreatedEvent;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\EntityManager;

class JWTCreatedListener extends BaseService
{
    /**
     * @var RequestStack
     */
    private $requestStack;

    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @param RequestStack $requestStack
     */
    public function __construct(RequestStack $requestStack, ContainerInterface $container, EntityManager $em)
    {
        $this->requestStack = $requestStack;
        $this->container = $container;
        $this->em = $em;
    }

    /**
     * @param JWTCreatedEvent $event
     *
     * @return void
     */
    public function onJWTCreated(JWTCreatedEvent $event)
    {
        $request = $this->requestStack->getCurrentRequest();

        $payload       = $event->getData();
        $payload['ip'] = $request->getClientIp();

        $user = $event->getUser();

        $repository=$this->em->getRepository('KaitekFrameworkBundle:Menu');
        $af = $this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        $ru = $this->container->get('security.authorization_checker')->isGranted('ROLE_USER');
        $ra = $this->container->get('security.authorization_checker')->isGranted('ROLE_ADMIN');
        $tr = $this->container->get('translator');

        $userId = $user->getId();

        $menus = $repository->loadUserMenuQB($this->em,$af,$ru,$ra,$tr,$userId);
        $payload['menus'] = $menus;

        $event->setData($payload);
    }
}