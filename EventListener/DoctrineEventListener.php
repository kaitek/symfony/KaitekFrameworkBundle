<?php

namespace Kaitek\Bundle\FrameworkBundle\EventListener;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Symfony\Component\DependencyInjection\ContainerInterface;

class DoctrineEventListener implements EventSubscriber
{
    protected $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function getSubscribedEvents()
    {
        return array(
          'postPersist',
          'postUpdate',
        );
    }

    /**
     * This method will called on Doctrine postPersist event
     */
    public function postPersist(LifecycleEventArgs $args)
    {
        # Avoid to log the logging process
        if (!$args->getEntity() instanceof Log) {
            # Handle the log creation
            $this->createLog($args, 'persist');
        }
    }

    /**
     * This method will called on Doctrine postUpdate event
     */
    public function postUpdate(LifecycleEventArgs $args)
    {
        # Handle the log creation
        $this->createLog($args, 'update');
    }

    /**
     * Handle the log creation
     */
    public function createLog(LifecycleEventArgs $args, $action)
    {
        return;
        # Entity manager
        $em = $args->getEntityManager();
        $entity = $args->getEntity();

        # Set DateTime()
        $dateTime = new \DateTime();

        # Get user
        if (method_exists($this->container->get('security.context'), 'getUser')):

            # Get current user
            $user = $this->container->get('security.context')->getToken()->getUser();



        endif;
    }

}
