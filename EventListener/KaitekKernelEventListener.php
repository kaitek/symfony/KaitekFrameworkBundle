<?php

/*
 * Copyright (c) 2016, oguzhan
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

namespace Kaitek\Bundle\FrameworkBundle\EventListener;

use Kaitek\Bundle\FrameworkBundle\Base\BaseService;
use Kaitek\Bundle\FrameworkBundle\Entity\LogDetail;

use Doctrine\ORM\EntityManager;
use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class KaitekKernelEventListener extends BaseService
{
    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var Serializer
     */
    protected $serializer;

    /**
     * Constructor
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
        $encoders = array(new XmlEncoder(), new JsonEncoder());
        $normalizers = array(new ObjectNormalizer());

        $this->serializer = new Serializer($normalizers, $encoders);
    }

    public function onKernelRequest(RequestEvent $event)
    {
        $request = $event->getRequest();
        $userId = 0;
        $sessionId = "";
        $method = "";
        $path = "";
        $paramsrequest = "";
        $paramsquery = "";
        $paramsattributes = "";
        $paramsfiles = "";
        try {
            if ($request->getSession() != null && $request->getSession()->getId() !== "") {
                $sessionId = $request->getSession()->getId();
                $onlineUser = $this->em->getRepository('KaitekFrameworkBundle:Online')->loadOnlineBySessionId($sessionId);
                $user = (null !== $onlineUser) ? $onlineUser->getUser() : 0;
                if (gettype($user) == 'integer') {
                    $userId = $user;
                    $user = $this->em->getRepository('KaitekFrameworkBundle:User')->findOneBy(array('id' => $userId));
                } else {
                    $userId = $user->getId();
                }
                if ($userId >= 0) {
                    try {
                        $method = $request->getMethod();
                        $path = $request->getPathInfo();
                        $paramsrequest = $this->serializer->serialize($request->request->all(), 'json').'-'.$request->getContent();
                        $paramsquery = $this->serializer->serialize($request->query->all(), 'json');
                        $paramsattributes = $this->serializer->serialize($request->attributes->all(), 'json');
                        //TODO: upload esnasında hata verdiği için şimdilik kapatıldı. İhtiyaç halinde hata giderilerek açılacak.
                        //$paramsfiles = $this->serializer->serialize($request->files->all(), 'json');
                    } catch (Exception $e) {
                        //throw $e;
                    }
                    //$user = $this->em->getRepository('KaitekFrameworkBundle:User')->findOneBy(array('id' => $userId));
                    $log = new LogDetail();
                    if($user) {
                        $log->setUser($user);
                    }
                    $log->setSessionId($sessionId);
                    $log->setMethod($method);
                    $log->setPath($path);
                    $log->setParamsrequest($paramsrequest);
                    $log->setParamsquery($paramsquery);
                    $log->setParamsattributes($paramsattributes);
                    //TODO: upload esnasında hata verdiği için şimdilik kapatıldı. İhtiyaç halinde hata giderilerek açılacak.
                    //$log->setParamsfiles($paramsfiles);
                    $log->setIp($request->getClientIp());
                    $log->setTime(new \DateTime());

                    $this->em->persist($log);
                    $this->em->flush();
                }
            }
        } catch (Exception $ex) {

        }
    }

    public function onKernelResponse(ResponseEvent $event)
    {
        $response = $event->getResponse();
        $response->headers->set('X-Powered-By', 'Kaitek App Manager v:2.0');
    }

}
