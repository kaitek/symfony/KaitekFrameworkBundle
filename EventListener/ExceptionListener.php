<?php

namespace Kaitek\Bundle\FrameworkBundle\EventListener;

use Kaitek\Bundle\FrameworkBundle\Base\BaseService;
use Kaitek\Bundle\FrameworkBundle\Entity\LogException;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\Routing\Router;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;


class ExceptionListener extends BaseService
{
    /**
     * @var EntityManager
     */
    protected $em;
    
    /**
     * @var Serializer
     */
    protected $serializer;
    
    /**
     * @var Router $router
     */
    protected $router;
    /**
     * Constructor
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em, Router $router)
    {
        $this->em = $em;
        $this->router = $router;
        $encoders = array(new XmlEncoder(), new JsonEncoder());
        $normalizers = array(new ObjectNormalizer());

        $this->serializer = new Serializer($normalizers, $encoders);
    }
    
    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        // You get the exception object from the received event
        $exception = $event->getException();
        $efile = $exception->getFile();
        $eline = $exception->getLine();
        $_code = $exception->getCode();
        $ecode = $_code && $_code != '0' ? $_code : (method_exists($exception, 'getStatusCode') ? $exception->getStatusCode() : 500);
        $emessage = mb_convert_encoding($exception->getMessage(), "UTF-8");
        $request = $event->getRequest();
        $session = $request->getSession();
        $locale = $session->get('_locale');
        if($this->em->isOpen()){
            $userId = 0;
            $sessionId = "";
            $method = "";
            $path = "";
            $paramsrequest = "";
            $paramsquery = "";
            $paramsattributes = "";   
            $paramsfiles = "";
            try {
                if($session!=null&&$session->getId()!==""){
                    $sessionId = $session->getId();
                    $onlineUser = $this->em->getRepository('KaitekFrameworkBundle:Online')->loadOnlineBySessionId($sessionId);
                    $user = (null !== $onlineUser) ? $onlineUser->getUser() : null;
                    if($user != null){
                        $userId = $user->getId();
                    }
                }
                if($userId>=0){
                    $method = $request->getMethod();
                    $path = $request->getPathInfo();
                    $paramsrequest = $this->serializer->serialize($request->request->all(), 'json');
                    $paramsquery = $this->serializer->serialize($request->query->all(), 'json');
                    $paramsattributes = $this->serializer->serialize($request->attributes->all(), 'json');
                    //TODO: upload esnasında hata verdiği için şimdilik kapatıldı. İhtiyaç halinde hata giderilerek açılacak.
                    //$paramsfiles = $this->serializer->serialize($request->files->all(), 'json');

                    $log = new LogException();
                    if($user){
                        $log->setUser($user);
                    }
                    $log->setSessionId($sessionId);
                    $log->setMethod($method);
                    $log->setPath($path);
                    $log->setParamsrequest($paramsrequest);
                    $log->setParamsquery($paramsquery);
                    $log->setParamsattributes($paramsattributes);
                    //TODO: upload esnasında hata verdiği için şimdilik kapatıldı. İhtiyaç halinde hata giderilerek açılacak.
                    //$log->setParamsfiles($paramsfiles);
                    $log->setCode($ecode);
                    $log->setFile($efile);
                    $log->setLine($eline);
                    $log->setMessage($emessage);
                    $log->setTime(new \DateTime());
                    $log->setIp($request->getClientIp());

                    $this->em->persist($log);
                    $this->em->flush();
                }
                
            } catch (Exception $ex) {

            }
        }
        $mtitle = ($this->_container==null?$this->container:$this->_container)->get('translator')->trans('err.main.message_title', array(), 'KaitekFrameworkBundle');
        if ($request->isXmlHttpRequest()) {
            $content = array(
                            'title' => $mtitle,
                            'success' => false,
                            'file' => $efile,
                            'line' => $eline,
                            'message' => $emessage
        	);
        	$response = new JsonResponse($content, $ecode);
            $newCsrfToken = ($this->_container==null?$this->container:$this->_container)->get('security.csrf.token_manager')->getToken('authenticate');
            $response->headers->setCookie(new Cookie('XSRF-TOKEN', $newCsrfToken->getValue()));
        }else{
        	if ($exception instanceof MethodNotAllowedHttpException && $request->getMethod() == 'OPTIONS') {
        	    //https://developer.mozilla.org/en-US/docs/Glossary/Preflight_request
                //Not: Access-Control-Allow-Methods header'ı httpd.conf ya da htaccess üzerinden ekleniyor.
                $response = new JsonResponse(array('msg' => 'ok'), 200);
                $response->headers->set('X-Status-Code', 200);
                //$response->headers->set('Access-Control-Allow-Methods', implode(', ', $exception->getAllowedMethods()));
                $event->setResponse($response);
            } elseif ($exception instanceof NotFoundHttpException) {

                    /** Choose your router here */
                    $route = 'kaitek-framework-home-get';

                    if ($route === $event->getRequest()->get('_route')) {
                        return;
                    }
                    if($locale == "") {
                        $locale = "tr";
                    }
                    $url = $this->router->generate($route,array('_locale' => $locale));
                    $response = new RedirectResponse($url);
                    $event->setResponse($response);

                }else{
                    $message = sprintf(
        		'%s: %s with code: %s',
        		$mtitle,
                        $emessage,
        		$ecode
        	);
                    // Customize your response object to display the exception details
                    $response = new Response();
                    $response->setContent($message);

                    // HttpExceptionInterface is a special type of exception that
                    // holds status code and header details
                    if ($exception instanceof HttpExceptionInterface) {
                            $response->setStatusCode($exception->getStatusCode());
                            $response->headers->replace($exception->getHeaders());
                    } else {
                            $response->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR);
                    }
                    // Send the modified response object to the event
                }
        }
        $event->setResponse($response);
    }
}