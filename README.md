# Installation
## Step 1: Install Prerequisites

Install [Composer](https://getcomposer.org/download/), the [Symfony Installer](http://symfony.com/download) and [Node.js](https://nodejs.org/en/download/package-manager).

Once Node.js is installed, install [bower](https://bower.io/#install-bower) and [gulp](https://github.com/gulpjs/gulp/blob/master/docs/getting-started.md#1-install-gulp-globally) globally with executing commands as shown below:

```bash
npm install -g bower
npm install -g gulp
```

## Step 2: Create new project
Execute this command:

```bash
symfony new my_project 2.8
```

## Step 3: Modify composer.json

```json
"require" : {
    [...],
    "kaitek/framework-bundle": "1.0.x-dev",
    "ocramius/package-versions": "~1.1.1"
},
"scripts": {
    [...],
    "kaitek-scripts": [
        "Kaitek\\Bundle\\FrameworkBundle\\Composer\\ScriptHandler::initFrontend"
    ],
    "post-install-cmd": [
        "@kaitek-scripts",
        [...]
    ],
    "post-update-cmd": [
        "@kaitek-scripts",
        [...]
    ]
},
"config": {
    "bin-dir": "bin",
    "platform": {
        "php": "7.0.9"
    }
},
"repositories" : [{
    "type" : "git",
    "url" : "https://git.kaitek.com.tr/kaitek/KaitekFrameworkBundle.git"
},{
    "type" : "git",
    "url" : "https://git.kaitek.com.tr/kaitek/KaitekFrontendBundle.git"
}],
"minimum-stability": "dev",
"prefer-stable": true
```

> Install **"ocramius/package-versions": "~1.1.1"** just only if composer gives error on PackageVersions library.


## Step 4: Modify parameters
Edit database connection parameters in app/config/parameters.yml:

```yaml
# app/config/parameters.yml
database_host: YOUR_DB_HOST
database_port: YOUR_DB_PORT
database_name: YOUR_DB_NAME
database_user: YOUR_DB_USER
database_password: YOUR_DB_PASSWORD
```

## Step 5: Enable the bundle
Enable the KaitekFrameworkBundle with FOSUserBundle, FOSJsRoutingBundle, SimpleThingsEntityAuditBundle, KaitekFrontendBundle dependencies in app/AppKernel.php:

```php
// app/AppKernel.php

// ...
class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = array(
            // ...
            new FOS\UserBundle\FOSUserBundle(),
            new FOS\JsRoutingBundle\FOSJsRoutingBundle(),
            new SimpleThings\EntityAudit\SimpleThingsEntityAuditBundle(),
            new Kaitek\Bundle\FrameworkBundle\KaitekFrameworkBundle(),
            new Kaitek\Bundle\FrontendBundle\KaitekFrontendBundle(),
            new Kaitek\Bundle\CalendarBundle\KaitekCalendarBundle()
        );

        // ...
    }

    // ...
}
```

## Step 6: Install KaitekFrameworkBundle and dependencies
Open a command console, enter your project directory and execute the
following command to download the latest stable version of this bundle:

```bash
composer update
```
---
## Further Instructions about FrontendBundle

You can find detailed instructions about configuration of asset handling on [KaitekFrontendBundle's website](http://git.kaitek.com.tr/kaitek/KaitekFrontendBundle).

---
