<?php

namespace Kaitek\Bundle\FrameworkBundle\Command;

use Kaitek\Bundle\FrameworkBundle\Entity\Menu;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class KaiPostInstallCommand extends ContainerAwareCommand
{
     protected function configure()
    {

        $this
            ->setName('kai:post:install')
            ->setDescription('Kaitek menu tablosuna ilk kurulumdaki menu kayitlarini olusturur.')
            ->setHelp(<<<EOT
<info>kai:post:install</info> komutu kurulumdaki menu kayitlarini olusturur:

  <info>php bin/console kai:post:install</info>

EOT
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $menus=array(
            array("sira"=>9998,"parentid"=>0,"menuid"=>"adminMenu"
                ,"title"=>"KaitekFrameworkBundle.menu.admin"
                ,"url"=>"","width"=>null,"height"=>null,"modulename"=>""
                ,"cls"=>"","iconcls"=>"fa-cog","script"=>""
                ,"multiplewindow"=>0,"maximizable"=>0,"resizable"=>0
                ,"mobiledevice"=>1,"parent"=>"")
            ,array("sira"=>9999,"parentid"=>0,"menuid"=>"logout"
                ,"title"=>"KaitekFrameworkBundle.general.logout"
                ,"url"=>"","width"=>null,"height"=>null,"modulename"=>""
                ,"cls"=>"","iconcls"=>"fa-sign-out","script"=>"app.logout();"
                ,"multiplewindow"=>0,"maximizable"=>0,"resizable"=>0
                ,"mobiledevice"=>1,"parent"=>"")
            ,array("sira"=>0,"parentid"=>"","menuid"=>"role"
                ,"title"=>""
                ,"url"=>"","width"=>900,"height"=>400,"modulename"=>"Role"
                ,"cls"=>"","iconcls"=>"fa-lock","script"=>""
                ,"multiplewindow"=>0,"maximizable"=>0,"resizable"=>0
                ,"mobiledevice"=>1,"parent"=>"adminMenu")
            ,array("sira"=>1,"parentid"=>"","menuid"=>"menuManager"
                ,"title"=>""
                ,"url"=>"","width"=>900,"height"=>400,"modulename"=>"Menu"
                ,"cls"=>"","iconcls"=>"fa-bars","script"=>""
                ,"multiplewindow"=>0,"maximizable"=>0,"resizable"=>0
                ,"mobiledevice"=>1,"parent"=>"adminMenu")
            ,array("sira"=>2,"parentid"=>"","menuid"=>"newUser"
                ,"title"=>"title"
                ,"url"=>"","width"=>900,"height"=>400,"modulename"=>"User"
                ,"cls"=>"","iconcls"=>"fa-users","script"=>""
                ,"multiplewindow"=>0,"maximizable"=>0,"resizable"=>0
                ,"mobiledevice"=>1,"parent"=>"adminMenu")
            ,array("sira"=>3,"parentid"=>"","menuid"=>"onlineUser"
                ,"title"=>""
                ,"url"=>"","width"=>900,"height"=>400,"modulename"=>"OnlineUser"
                ,"cls"=>"","iconcls"=>"fa-user-plus","script"=>""
                ,"multiplewindow"=>0,"maximizable"=>0,"resizable"=>0
                ,"mobiledevice"=>1,"parent"=>"adminMenu")
        );
        $em = $this->getContainer()->get('doctrine')->getManager();
        /*$connection = $em->getConnection();
        $dbPlatform = $connection->getDatabasePlatform();
        $connection->beginTransaction();
        try {
            #$connection->query('SET FOREIGN_KEY_CHECKS=0');
            $q = $dbPlatform->getTruncateTableSql('_menu');
            $connection->executeUpdate('TRUNCATE _menu RESTART IDENTITY CASCADE');
            #$connection->query('SET FOREIGN_KEY_CHECKS=1');
            $connection->commit();
        }
        catch (\Exception $e) {
            $connection->rollback();
            $output->writeln(sprintf('HATA: <comment>%s</comment>', $e->getMessage()));
        }*/
        $repository = $em->getRepository('KaitekFrameworkBundle:Menu');
        foreach ($menus as $item) {
            $menuId=$item["menuid"];
            $parentId=$item["parentid"];
            if($item["parent"]!=""){
                $precord=$repository->loadMenuByMenuId($item["parent"]);
                if($precord!=null){
                    $parentId=$precord->getId();
                }else{
                    $parentId=0;
                }
            }
            $record=$repository->loadMenuByMenuId($menuId);
            if($record==null){
                $menu = new Menu();
                $menu->setCreateuserId(0);
                $menu->setSira($item["sira"]);
                $menu->setParentId($parentId);
                $menu->setMenuId($item["menuid"]);
                $menu->setTitle($item["title"]);
                $menu->setUrl($item["url"]);
                $menu->setWidth($item["width"]);
                $menu->setHeight($item["height"]);
                $menu->setModuleName($item["modulename"]);
                $menu->setCls($item["cls"]);
                $menu->setIconCls($item["iconcls"]);
                $menu->setScript($item["script"]);
                $menu->setMultipleWindow($item["multiplewindow"]);
                $menu->setMaximizable($item["maximizable"]);
                $menu->setResizable($item["resizable"]);
                $menu->setMobileDevice($item["mobiledevice"]);
         
                $em->persist($menu);
                $em->flush();
                $output->writeln(sprintf('Olusturulan menu: <comment>%s</comment>', $menuId));
            }else{
                $output->writeln(sprintf('Guncellenen menu: <comment>%s</comment>', $menuId));
            }
        }
        

    }
    
}
