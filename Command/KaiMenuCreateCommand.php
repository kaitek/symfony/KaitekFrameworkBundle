<?php

namespace Kaitek\Bundle\FrameworkBundle\Command;

use Kaitek\Bundle\FrameworkBundle\Entity\Menu;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
#use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;

class KaiMenuCreateCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        /*$this
            ->setName('kai:menu:create')
            ->setDescription('Kaitek menu tablosuna menu kaydı oluşturur.')
            ->addArgument('argument', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option', null, InputOption::VALUE_NONE, 'Option description')
         * 
         * 
                
        ;*/
        $this
            ->setName('kai:menu:create')
            ->setDescription('Kaitek menu tablosuna menu kaydi olusturur.')
            ->setDefinition(array(
                new InputArgument('sira', InputArgument::REQUIRED, 'sıra'),
                new InputArgument('parentid', InputArgument::REQUIRED, 'parentid'),
                new InputArgument('menuId', InputArgument::REQUIRED, 'menuId'),
                new InputArgument('title', InputArgument::REQUIRED, 'title'),
                new InputArgument('url', InputArgument::REQUIRED, 'url'),
                new InputArgument('width', InputArgument::REQUIRED, 'width'),
                new InputArgument('height', InputArgument::REQUIRED, 'height'),
                new InputArgument('modulename', InputArgument::OPTIONAL, 'modulename',''),
                new InputArgument('iconcls', InputArgument::OPTIONAL, 'iconcls',''),
                new InputArgument('multiplewindow', InputArgument::OPTIONAL, 'multiplewindow',0),
                new InputArgument('maximizable', InputArgument::OPTIONAL, 'maximizable',0),
                new InputArgument('resizable', InputArgument::OPTIONAL, 'resizable',0),
                new InputArgument('mobiledevice', InputArgument::OPTIONAL, 'mobiledevice',0),
            ))
            ->setHelp(<<<EOT
<info>kai:menu:create</info> komutu yeni bir menu kaydi olusturur:

  <info>php bin/console kai:menu:create</info>

EOT
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $sira   = $input->getArgument('sira');
        $parentid      = $input->getArgument('parentid');
        $menuId   = $input->getArgument('menuId');
        $title   = $input->getArgument('title');
        $url   = $input->getArgument('url');
        $width   = $input->getArgument('width');
        $height   = $input->getArgument('height');
        $modulename   = $input->getArgument('modulename');
        $iconcls   = $input->getArgument('iconcls');
        $multiplewindow   = $input->getArgument('multiplewindow');
        $maximizable   = $input->getArgument('maximizable');
        $resizable   = $input->getArgument('resizable');
        $mobiledevice   = $input->getArgument('mobiledevice');
        #$superadmin = $input->getOption('super-admin');

        #$manipulator = $this->getContainer()->get('kaitek_framework.util.menu_manipulator');
        #$manipulator->create($sira, $parentid, $menuId, $title, $url, $width, $height, $optparams, $slug, $multiplewindow, $maximizable, $resizable, $mobiledevice);
        #$repository = $this->getDoctrine()->getRepository('KaitekFrameworkBundle:Menu');
        $menu = new Menu();
        
        $menu->setCreateuserId(0);
        $menu->setSira($sira);
        $menu->setParentId($parentid);
        $menu->setMenuId($menuId);
        $menu->setTitle($title);
        $menu->setUrl($url);
        $menu->setWidth($width);
        $menu->setHeight($height);
        $menu->setModuleName($modulename);
        $menu->setIconCls($iconcls);
        $menu->setMultipleWindow($multiplewindow);
        $menu->setMaximizable($maximizable);
        $menu->setResizable($resizable);
        $menu->setMobileDevice($mobiledevice);
        $em = $this->getContainer()->get('doctrine')->getManager();

        $em->persist($menu);
        $em->flush();
        $output->writeln(sprintf('Olusturulan menu: <comment>%s</comment>', $menuId));

    }
    
    /**
     * @see Command
     */
    protected function interact(InputInterface $input, OutputInterface $output)
    {
        if (!$this->getHelperSet()->has('question')) {
            $this->legacyInteract($input, $output);

            return;
        }

        $questions = array();

        if (!$input->getArgument('sira')) {
            $question = new Question('sira degeri giriniz:');
            $question->setValidator(function($sira) {
                if (empty($sira)) {
                    throw new \Exception('sira bos olamaz');
                }

                return $sira;
            });
            $questions['sira'] = $question;
        }

        if (!$input->getArgument('parentid')) {
            $question = new Question('parentid degeri giriniz:');
            $questions['parentid'] = $question;
        }

        if (!$input->getArgument('menuId')) {
            $question = new Question('menuId degeri giriniz:');
            $question->setValidator(function($menuId) {
                if (empty($menuId)) {
                    throw new \Exception('menuId bos olamaz');
                }

                return $menuId;
            });
            $questions['menuId'] = $question;
        }
        
        if (!$input->getArgument('title')) {
            $question = new Question('title degeri giriniz:');
            /*$question->setValidator(function($title) {
                if (empty($title)) {
                    throw new \Exception('title bos olamaz');
                }

                return $title;
            });*/
            $questions['title'] = $question;
        }
        
        if (!$input->getArgument('url')) {
            $question = new Question('url degeri giriniz:');
            $question->setValidator(function($url) {
                if (empty($url)) {
                    throw new \Exception('url bos olamaz');
                }

                return $url;
            });
            $questions['url'] = $question;
        }
        
        if (!$input->getArgument('width')) {
            $question = new Question('width degeri giriniz:');
            $question->setValidator(function($width) {
                if (empty($width)) {
                    throw new \Exception('width bos olamaz');
                }

                return $width;
            });
            $questions['width'] = $question;
        }
        
        if (!$input->getArgument('height')) {
            $question = new Question('height degeri giriniz:');
            $question->setValidator(function($height) {
                if (empty($height)) {
                    throw new \Exception('height bos olamaz');
                }

                return $height;
            });
            $questions['height'] = $question;
        }
        
        if (!$input->getArgument('modulename')) {
            $question = new Question('modulename degeri giriniz:');
            $questions['modulename'] = $question;
        }
        
        if (!$input->getArgument('iconcls')) {
            $question = new Question('iconcls degeri giriniz:');
            $questions['iconcls'] = $question;
        }
        
        if (!$input->getArgument('multiplewindow')) {
            $question = new Question('multiplewindow degeri giriniz:');
            $questions['multiplewindow'] = $question;
        }
        
        if (!$input->getArgument('maximizable')) {
            $question = new Question('maximizable degeri giriniz:');
            $questions['maximizable'] = $question;
        }
        
        if (!$input->getArgument('resizable')) {
            $question = new Question('resizable degeri giriniz:');
            $questions['resizable'] = $question;
        }
        
        if (!$input->getArgument('mobiledevice')) {
            $question = new Question('mobiledevice degeri giriniz:');
            $questions['mobiledevice'] = $question;
        }

        foreach ($questions as $name => $question) {
            $answer = $this->getHelper('question')->ask($input, $output, $question);
            $input->setArgument($name, $answer);
        }
    }

}
