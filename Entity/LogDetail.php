<?php

namespace Kaitek\Bundle\FrameworkBundle\Entity;

use Kaitek\Bundle\FrameworkBundle\Model\LogDetail as BaseLogDetail;
use Doctrine\ORM\Mapping as ORM;

/**
 * LogDetail
 *
 * @ORM\Table(name="_log_detail")
 * @ORM\Entity(repositoryClass="Kaitek\Bundle\FrameworkBundle\Repository\LogDetailRepository")
 */
class LogDetail extends BaseLogDetail
{
    /**
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * @ORM\ManyToOne(targetEntity="User", inversedBy="log_details")
     */
    protected $user;
}

