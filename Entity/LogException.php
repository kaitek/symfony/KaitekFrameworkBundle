<?php

namespace Kaitek\Bundle\FrameworkBundle\Entity;

use Kaitek\Bundle\FrameworkBundle\Model\LogException as BaseLogException;
use Doctrine\ORM\Mapping as ORM;

/**
 * LogException
 *
 * @ORM\Table(name="_log_exception")
 * @ORM\Entity(repositoryClass="Kaitek\Bundle\FrameworkBundle\Repository\LogExceptionRepository")
 */
class LogException extends BaseLogException
{

    /**
     * @var int
     *
     * @ORM\Column(name="code", type="integer")
     */
    protected $code;

    /**
     * @var string
     *
     * @ORM\Column(name="file", type="string", length=500)
     */
    protected $file;

    /**
     * @var int
     *
     * @ORM\Column(name="line", type="integer")
     */
    protected $line;

    /**
     * @var string
     *
     * @ORM\Column(name="message", type="text")
     */
    protected $message;

    /**
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * @ORM\ManyToOne(targetEntity="User", inversedBy="log_exceptions")
     */
    protected $user;
}
