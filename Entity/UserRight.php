<?php

namespace Kaitek\Bundle\FrameworkBundle\Entity;

use Kaitek\Bundle\FrameworkBundle\Model\UserRight as BaseUserRight;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

/**
 * UserRight
 *
 * @ORM\Table(name="_user_right"
 *  ,indexes={
 *      @ORM\Index(name="idx_user_right_user_id", columns={"user_id"})
 * })
 * @ORM\Entity(repositoryClass="Kaitek\Bundle\FrameworkBundle\Repository\UserRightRepository")
 */
class UserRight extends BaseUserRight
{

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="user_rights")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * @Assert\NotNull()
     */
    protected $userId;

    /**
     * @ORM\ManyToOne(targetEntity="Menu", inversedBy="user_rights")
     * @ORM\JoinColumn(name="menu_id", referencedColumnName="id")
     * @Assert\NotNull()
     */
    protected $menuId;

    /**
     * @var bool
     *
     * @ORM\Column(name="gRight", type="boolean")
     * @Assert\NotNull()
     * @Assert\Type(
     *      type="bool",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $gRight = false;

    /**
     * @var bool
     *
     * @ORM\Column(name="aRight", type="boolean")
     * @Assert\NotNull()
     * @Assert\Type(
     *      type="bool",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $aRight = false;

    /**
     * @var bool
     *
     * @ORM\Column(name="uRight", type="boolean")
     * @Assert\NotNull()
     * @Assert\Type(
     *      type="bool",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $uRight = false;

    /**
     * @var bool
     *
     * @ORM\Column(name="dRight", type="boolean")
     * @Assert\NotNull()
     * @Assert\Type(
     *      type="bool",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $dRight = false;

    /**
     * @var bool
     *
     * @ORM\Column(name="uOwnRight", type="boolean")
     * @Assert\NotNull()
     * @Assert\Type(
     *      type="bool",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $uOwnRight = false;

    /**
     * @var bool
     *
     * @ORM\Column(name="dOwnRight", type="boolean")
     * @Assert\NotNull()
     * @Assert\Type(
     *      type="bool",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $dOwnRight = false;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     *
     * @return UserRight
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return int
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set menuId
     *
     * @param integer $menuId
     *
     * @return UserRight
     */
    public function setMenuId($menuId)
    {
        $this->menuId = $menuId;

        return $this;
    }

    /**
     * Get menuId
     *
     * @return int
     */
    public function getMenuId()
    {
        return $this->menuId;
    }

    /**
     * Set gRight
     *
     * @param boolean $gRight
     *
     * @return UserRight
     */
    public function setGRight($gRight)
    {
        $this->gRight = $gRight;

        return $this;
    }

    /**
     * Get gRight
     *
     * @return bool
     */
    public function getGRight()
    {
        return $this->gRight;
    }

    /**
     * Set aRight
     *
     * @param boolean $aRight
     *
     * @return UserRight
     */
    public function setARight($aRight)
    {
        $this->aRight = $aRight;

        return $this;
    }

    /**
     * Get aRight
     *
     * @return boolean
     */
    public function getARight()
    {
        return $this->aRight;
    }

    /**
     * Set uRight
     *
     * @param boolean $uRight
     *
     * @return UserRight
     */
    public function setURight($uRight)
    {
        $this->uRight = $uRight;

        return $this;
    }

    /**
     * Get uRight
     *
     * @return boolean
     */
    public function getURight()
    {
        return $this->uRight;
    }

    /**
     * Set dRight
     *
     * @param boolean $dRight
     *
     * @return UserRight
     */
    public function setDRight($dRight)
    {
        $this->dRight = $dRight;

        return $this;
    }

    /**
     * Get dRight
     *
     * @return boolean
     */
    public function getDRight()
    {
        return $this->dRight;
    }

    /**
     * Set uOwnRight
     *
     * @param boolean $uOwnRight
     *
     * @return UserRight
     */
    public function setUOwnRight($uOwnRight)
    {
        $this->uOwnRight = $uOwnRight;

        return $this;
    }

    /**
     * Get uOwnRight
     *
     * @return boolean
     */
    public function getUOwnRight()
    {
        return $this->uOwnRight;
    }

    /**
     * Set dOwnRight
     *
     * @param boolean $dOwnRight
     *
     * @return UserRight
     */
    public function setDOwnRight($dOwnRight)
    {
        $this->dOwnRight = $dOwnRight;

        return $this;
    }

    /**
     * Get dOwnRight
     *
     * @return boolean
     */
    public function getDOwnRight()
    {
        return $this->dOwnRight;
    }

    /**
     * Set cdate
     *
     * @param \DateTime $cdate
     *
     * @return UserRight
     */
    public function setCdate($cdate)
    {
        $this->cdate = $cdate;

        return $this;
    }

    /**
     * Set ddate
     *
     * @param \DateTime $ddate
     *
     * @return UserRight
     */
    public function setDdate($ddate)
    {
        $this->ddate = $ddate;

        return $this;
    }

    function getUserroleId() {
        return $this->userroleId;
    }

    function setUserroleId($userroleId) {
        $this->userroleId = $userroleId;
    }



}
