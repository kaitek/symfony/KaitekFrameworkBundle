<?php

namespace Kaitek\Bundle\FrameworkBundle\Entity;

use Kaitek\Bundle\FrameworkBundle\Model\CustomRight as BaseCustomRight;
use Doctrine\ORM\Mapping as ORM;

/**
 * CustomRight
 *
 * @ORM\Table(name="_custom_right")
 * @ORM\Entity(repositoryClass="Kaitek\Bundle\FrameworkBundle\Repository\CustomRightRepository")
 */
class CustomRight extends BaseCustomRight
{
    /**
     * @var string
     *
     * @ORM\Column(name="entity", type="string", length=255)
     */
    protected $entity;

    /**
     * @var string
     *
     * @ORM\Column(name="fieldName", type="string", length=255)
     */
    protected $fieldName;

    /**
     * @var string
     *
     * @ORM\Column(name="fieldValue", type="string", length=255)
     */
    protected $fieldValue;

    /**
     * @var Role
     *
     * @ORM\ManyToOne(targetEntity="Role", inversedBy="custom_rights")
     * @ORM\JoinColumn(name="role_id", referencedColumnName="id")
     */
    protected $role;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User", inversedBy="custom_rights")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;
}

