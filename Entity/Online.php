<?php

namespace Kaitek\Bundle\FrameworkBundle\Entity;

use Kaitek\Bundle\FrameworkBundle\Model\Online as BaseOnline;
use Kaitek\Bundle\FrameworkBundle\Model\User;
use Doctrine\ORM\Mapping as ORM;

/**
 * Online
 *
 * @ORM\Table(name="_online")
 * @ORM\Entity(repositoryClass="Kaitek\Bundle\FrameworkBundle\Repository\OnlineRepository")
 */
class Online extends BaseOnline
{
    
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;
    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="onlines")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;

    /**
     * @var string
     *
     * @ORM\Column(name="sessionId", type="string", length=255)
     */
    protected $sessionId;

    /**
     * @var string
     *
     * @ORM\Column(name="ip", type="string", length=20)
     */
    protected $ip;

    /**
     * @var string
     *
     * @ORM\Column(name="host", type="string", length=255)
     */
    protected $host;

    /**
     * @var int
     *
     * @ORM\Column(name="active", type="smallint")
     */
    protected $active;

    /**
     * @var string
     *
     * @ORM\Column(name="outIp", type="string", length=20)
     */
    protected $outIp;

    /**
     * @var string
     *
     * @ORM\Column(name="userAgent", type="string", length=300)
     */
    protected $userAgent;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="lastUpdate", type="datetime")
     */
    protected $lastUpdate;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function getUser(): ?User
    {
        return $this->user;
    }

    /**
     * {@inheritdoc}
     */
    public function setUser(User $user): BaseOnline
    {
        $this->user = $user;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setSessionId($sessionId)
    {
        $this->sessionId = $sessionId;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getSessionId()
    {
        return $this->sessionId;
    }

    /**
     * {@inheritdoc}
     */
    public function setIp($ip)
    {
        $this->ip = $ip;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * {@inheritdoc}
     */
    public function setHost($host)
    {
        $this->host = $host;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getHost()
    {
        return $this->host;
    }

    /**
     * {@inheritdoc}
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * {@inheritdoc}
     */
    public function setOutIp($outIp)
    {
        $this->outIp = $outIp;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getOutIp()
    {
        return $this->outIp;
    }

    /**
     * {@inheritdoc}
     */
    public function setUserAgent($userAgent)
    {
        $this->userAgent = $userAgent;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getUserAgent()
    {
        return $this->userAgent;
    }

    /**
     * {@inheritdoc}
     */
    public function setLastUpdate($lastUpdate)
    {
        $this->lastUpdate = $lastUpdate;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getLastUpdate()
    {
        return $this->lastUpdate;
    }
}
