<?php

namespace Kaitek\Bundle\FrameworkBundle\Entity;

use Kaitek\Bundle\FrameworkBundle\Model\Role as BaseRole;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Role
 *
 * @ORM\Table(name="_role")
 * @ORM\Entity(repositoryClass="Kaitek\Bundle\FrameworkBundle\Repository\RoleRepository")
 */
class Role extends BaseRole
{
    /**
     * @var \Doctrine\Common\Collections\Collection|CustomRight[]
     *
     * @ORM\OneToMany(targetEntity="CustomRight", mappedBy="role")
     */
    private $custom_rights;

    /**
     * @var \Doctrine\Common\Collections\Collection|User[]
     *
     * @ORM\ManyToMany(targetEntity="User", mappedBy="user_roles")
     */
    protected $users;

    /**
     * @var \Doctrine\Common\Collections\Collection|RoleRight[]
     *
     * @ORM\OneToMany(targetEntity="RoleRight", mappedBy="role")
     */
    private $role_rights;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100)
     * @Assert\NotBlank()
     * @Assert\Length(max = 100)
     */
    protected $name;

    public function __construct()
    {
        $this->user_roles = new ArrayCollection();
        $this->role_rights = new ArrayCollection();
        $this->custom_rights = new ArrayCollection();
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Role
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}
