<?php

namespace Kaitek\Bundle\FrameworkBundle\Entity;

//use FOS\UserBundle\Util\CanonicalizerInterface;
//use FOS\UserBundle\Model\User as BaseUser;
use Kaitek\Bundle\FrameworkBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * User
 *
 * @ORM\Table(name="_user")
 * @ORM\Entity(repositoryClass="Kaitek\Bundle\FrameworkBundle\Repository\UserRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class User extends BaseUser
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="avatar", type="string", nullable=true)
     */
    protected $avatar;

    /**
     * @var \Doctrine\Common\Collections\Collection|CustomRight[]
     * @ORM\OneToMany(targetEntity="CustomRight", mappedBy="user")
     */
    private $custom_rights;

    /**
     * @var string
     *
     * @ORM\Column(name="fullname", type="string", nullable=true, length=100, options={"default":"İsim Girilmemiş"})
     * @Assert\Length(max = 100)
     */
    protected $fullname = 'İsim Girilmemiş';

    /**
     * @var \Doctrine\Common\Collections\Collection|LogDetail[]
     * @ORM\OneToMany(targetEntity="LogDetail", mappedBy="user")
     */
    protected $log_details;

    /**
     * @var \Doctrine\Common\Collections\Collection|LogException[]
     * @ORM\OneToMany(targetEntity="LogException", mappedBy="user")
     */
    protected $log_exceptions;

    /**
     * @var \Doctrine\Common\Collections\Collection|Online[]
     * @ORM\OneToMany(targetEntity="Online", mappedBy="user")
     */
    protected $onlines;

    /**
     * @var \Doctrine\Common\Collections\Collection|Role[]
     *
     * @ORM\ManyToMany(targetEntity="Role", inversedBy="users")
     * @ORM\JoinTable(
     *  name="_user_role",
     *  joinColumns={
     *      @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     *  },
     *  inverseJoinColumns={
     *      @ORM\JoinColumn(name="role_id", referencedColumnName="id")
     *  }
     * )
     */
    protected $user_roles;

    /**
     * @var \Doctrine\Common\Collections\Collection|UserRight[]
     * @ORM\OneToMany(targetEntity="UserRight", mappedBy="userId")
     */
    protected $user_rights;
}
