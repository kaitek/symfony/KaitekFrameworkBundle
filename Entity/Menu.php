<?php

namespace Kaitek\Bundle\FrameworkBundle\Entity;

use Kaitek\Bundle\FrameworkBundle\Model\Menu as BaseMenu;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Menu
 *
 * @ORM\Table(name="_menu")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Entity(repositoryClass="Kaitek\Bundle\FrameworkBundle\Repository\MenuRepository")
 */
class Menu extends BaseMenu
{
    /**
     * @ORM\OneToMany(targetEntity="UserRight", mappedBy="menuId")
     */
    private $user_rights;

    /**
     * @ORM\OneToMany(targetEntity="RoleRight", mappedBy="menuId")
     */
    private $role_rights;

    /**
     * @var int
     *
     * @ORM\Column(name="sira", type="smallint", options={"default" = 0}, nullable=true)
     * @Assert\Type(
     *      type="integer",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $sira = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="parentid", type="integer", options={"default" = 0}, nullable=true)
     * @Assert\Type(
     *      type="integer",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $parentid = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="menuId", type="string", length=100)
     * @Assert\NotBlank()
     */
    protected $menuId;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=100, options={"default" = ""}, nullable=true)
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 100)
     */
    protected $title;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=100, options={"default" = ""}, nullable=true)
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 100)
     */
    protected $url;

    /**
     * @var int
     *
     * @ORM\Column(name="width", type="smallint", options={"default" = null}, nullable=true)
     * @Assert\Type(
     *      type="integer",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $width;

    /**
     * @var int
     *
     * @ORM\Column(name="height", type="smallint", options={"default" = null}, nullable=true)
     * @Assert\Type(
     *      type="integer",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $height;

    /**
     * @var string
     *
     * @ORM\Column(name="modulename", type="string", length=50, options={"default" = ""}, nullable=true)
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50)
     */
    protected $modulename;

    /**
     * @var string
     *
     * @ORM\Column(name="cls", type="string", length=150, options={"default" = ""}, nullable=true)
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 150)
     */
    protected $cls;

    /**
     * @var string
     *
     * @ORM\Column(name="iconcls", type="string", length=150, options={"default" = ""}, nullable=true)
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 150)
     */
    protected $iconcls;

    /**
     * @var string
     *
     * @ORM\Column(name="script", type="text", options={"default" = ""}, nullable=true)
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $script;

    /**
     * @var bool
     *
     * @ORM\Column(name="multiplewindow", type="boolean", nullable=true)
     */
    protected $multiplewindow;

    /**
     * @var bool
     *
     * @ORM\Column(name="maximizable", type="boolean", options={"default" = false}, nullable=true)
     */
    protected $maximizable;

    /**
     * @var bool
     *
     * @ORM\Column(name="resizable", type="boolean", options={"default" = false}, nullable=true)
     */
    protected $resizable;

    /**
     * @var bool
     *
     * @ORM\Column(name="mobiledevice", type="boolean", options={"default" = false}, nullable=true)
     */
    protected $mobiledevice;

    public function __construct()
    {
        parent::__construct();
        // your own logic
        $this->user_rights = new ArrayCollection();
        $this->role_rights = new ArrayCollection();
    }

    /**
     * Set sira
     *
     * @param integer $sira
     *
     * @return Menu
     */
    public function setSira($sira)
    {
        $this->sira = $sira;

        return $this;
    }

    /**
     * Get sira
     *
     * @return int
     */
    public function getSira()
    {
        return $this->sira;
    }

    /**
     * Set parentid
     *
     * @param integer $parentid
     *
     * @return Menu
     */
    public function setParentId($parentid)
    {
        $this->parentid = $parentid;
        return $this;
    }

    /**
     * Get parentid
     *
     * @return int
     */
    public function getParentId()
    {
        return $this->parentid;
    }

    /**
     * Set menuId
     *
     * @param string $menuId
     *
     * @return Menu
     */
    public function setMenuId($menuId)
    {
        $this->menuId = $menuId;

        return $this;
    }

    /**
     * Get menuId
     *
     * @return string
     */
    public function getMenuId()
    {
        return $this->menuId;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Menu
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set url
     *
     * @param string $url
     *
     * @return Menu
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set width
     *
     * @param integer $width
     *
     * @return Menu
     */
    public function setWidth($width)
    {
        $this->width = $width;

        return $this;
    }

    /**
     * Get width
     *
     * @return int
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * Set height
     *
     * @param integer $height
     *
     * @return Menu
     */
    public function setHeight($height)
    {
        $this->height = $height;

        return $this;
    }

    /**
     * Get height
     *
     * @return int
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * Set optparams
     *
     * @param array $optparams
     *
     * @return Menu
     */
    public function setOptparams($optparams)
    {
        $this->optparams = $optparams;

        return $this;
    }

    /**
     * Get optparams
     *
     * @return array
     */
    public function getOptparams()
    {
        return $this->optparams;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return Menu
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set multiplewindow
     *
     * @param boolean $multiplewindow
     *
     * @return Menu
     */
    public function setMultipleWindow($multiplewindow)
    {
        $this->multiplewindow = $multiplewindow;

        return $this;
    }

    /**
     * Get multiplewindow
     *
     * @return bool
     */
    public function getMultipleWindow()
    {
        return $this->multiplewindow;
    }

    /**
     * Set maximizable
     *
     * @param boolean $maximizable
     *
     * @return Menu
     */
    public function setMaximizable($maximizable)
    {
        $this->maximizable = $maximizable;

        return $this;
    }

    /**
     * Get maximizable
     *
     * @return bool
     */
    public function getMaximizable()
    {
        return $this->maximizable;
    }

    /**
     * Set resizable
     *
     * @param boolean $resizable
     *
     * @return Menu
     */
    public function setResizable($resizable)
    {
        $this->resizable = $resizable;

        return $this;
    }

    /**
     * Get resizable
     *
     * @return bool
     */
    public function getResizable()
    {
        return $this->resizable;
    }

    /**
     * Set mobiledevice
     *
     * @param boolean $mobiledevice
     *
     * @return Menu
     */
    public function setMobileDevice($mobiledevice)
    {
        $this->mobiledevice = $mobiledevice;

        return $this;
    }

    /**
     * Get mobiledevice
     *
     * @return bool
     */
    public function getMobileDevice()
    {
        return $this->mobiledevice;
    }

    function getModuleName() {
        return $this->modulename;
    }

    function setModuleName($modulename) {
        $this->modulename = $modulename;
    }

    function getCls() {
        return $this->cls;
    }

    function setCls($cls) {
        $this->cls = $cls;
    }

    function getIconCls() {
        return $this->iconcls;
    }

    function setIconCls($iconcls) {
        $this->iconcls = $iconcls;
    }

    function getScript() {
        return $this->script;
    }

    function setScript($script) {
        $this->script = $script;
    }


}
