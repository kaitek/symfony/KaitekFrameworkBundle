<?php
/**
 * Created by PhpStorm.
 * User: halil
 * Date: 1/23/17
 * Time: 12:09 PM
 */

namespace Kaitek\Bundle\FrameworkBundle\ORM\Mapping;

use Doctrine\ORM\Mapping\Annotation;

/**
 * @Annotation
 * @Target({"PROPERTY","ANNOTATION"})
 */
class FieldData implements Annotation
{
    public $label;
}