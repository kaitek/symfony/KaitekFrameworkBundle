<?php

namespace Kaitek\Bundle\FrameworkBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Kaitek\Bundle\FrameworkBundle\Model\RoleRightLoaderInterface;
/**
 * RoleRightRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class RoleRightRepository extends EntityRepository
{
    public function loadRoleRightByRoleId($roleId) {
        $roleRight = $this->createQueryBuilder('r')
            ->where('IDENTITY(r.role) = :roleId')
            ->setParameter('roleId', $roleId)
            ->getQuery()
            ->getArrayResult();
        return $roleRight;
    }
}
