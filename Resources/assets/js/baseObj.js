var mj = require('myjui');

baseObj = function(config) {
    baseObj.superclass.constructor.call(this, config);
};
baseObj.prototype = {
    gridLoaded: true,
    recordsperpage: 25,
    renderDefaulUI: true,
    audit: false,
    windowButtons: {
        add: true,
        delete: true,
        edit: true,
        exit: false,
        formSubmit: true,
        formCancel: true
    },
    init: function() {
        var t = this;
        if (t.scope && t.scope.win) {
            t.scope.win.obj = t;
            t.win = t.scope.win;
            t.winTitle = t.scope.win.title;
            t.waitMask = t.win.mask;
        }

        var routeName = t.modulename + '-showall';
        var route = Routing.getRoute(routeName);
        var data = {
            'pg': 1,
            'lm': t.recordsperpage
        };
        var url = Routing.generate(routeName, data);
        t.stores = {};
        t.stores.main = new mj.store({
            url: url,
            method: route.requirements._method,
            data: t.data
        });
        t.stores.main.on('beforeload', t._onMainStoreBeforeLoad, t);
        t.stores.main.on('load', t._onMainStoreLoad, t);

        if (t.renderDefaulUI) {
            t.tabPanels = {};
            t.tabPanels.main = new mj.tab({
                renderTo: t.scope,
                activeTab: t.gridLoaded ? 1 : 0,
                border: false,
                hideHeader: true,
                items: [{
                    closable: false
                }, {
                    closable: false
                }]
            });
            t.tabPanels.main.on('tabchange', t._onParentTabChange, t);

            t.tabs = {};
            t.tabs.main = {
                form: t.tabPanels.main.tabs[0],
                grid: t.tabPanels.main.tabs[1]
            };
            t.on('activate-tab-form', t._onActivateTabForm);
            t.on('activate-tab-grid', t._onActivateTabGrid);

            if (typeof t.formConfig != "undefined") {
                t.stores.form = new mj.store();
                t.stores.form.on('load', t._onFormStoreLoad, t);

                t.forms = {};
                t.forms.main = new mj.form(mj.apply({
                    renderTo: t.tabs.main.form.getBody(),
                    modulename: t.modulename,
                    store: t.stores.form
                }, t.formConfig));
            }

            if (typeof t.gridConfig !== "undefined") {
                if( mj.getIndex(t.gridConfig.cm, 'dataIndex', 'version') === -1 ) {
                    //{header: '', width: 5, hide: true}
                    var lastCol = t.gridConfig.cm[t.gridConfig.cm.length-1];
                    if(lastCol.hide && lastCol.header === ''){
                        t.gridConfig.cm.pop();
                    }
                    t.gridConfig.cm.push({header: mj.lng.glb.revision, width: 25, dataIndex: 'version', hide: true});
                    t.gridConfig.cm.push({header: '', width: 5, hide: true});
                }


                t.grids = {};
                t.grids.main = new mj.grid(mj.apply({
                    win: t.win,
                    renderTo: t.tabs.main.grid.getBody(),
                    store: t.stores.main,
                    entity: t.modulename,
                    pbar: new mj.pager({
                        pos: 'bottom',
                        beforePageText: false,
                        limit: 10
                    }),
                    fitToParent: true
                }, t.gridConfig));
                t.on('reset', t._onReset, t);

                t.grids.main.on('rowclick', t._onMainGridRowClick, t);

                t.grids.main.on('rowdblclick', t.editSelectedRecord, t);
            }

            t.initDefaultButtons();

            if(t.win){
                t.win.body.perfectScrollbar('update');
            }
        }
        baseObj.superclass.init.call(this);
    },
    _onActivateTabForm: function() {
    },
    _onActivateTabGrid: function() {
    },
    _onButtonAddClick: function() {
        this.newRecord();
    },
    _onButtonauditSwitchClick: function() {
        var t = this;
        if(t._auditShowDeleted){
            t._auditShowDeleted = false;
            t.buttons.auditSwitch.setIconCls('fa-trash');
            t.buttons.auditSwitch.setAlt(mj.lng.glb.auditShowDeleted);
        } else {
            t._auditShowDeleted = true;
            t.buttons.auditSwitch.setIconCls('fa-heart-o');
            t.buttons.auditSwitch.setAlt(mj.lng.glb.auditHideDeleted);
        }
    },
    _onButtonDeleteClick: function() {
        this.deleteRecord();
    },
    _onButtonEditClick: function() {
        this.editSelectedRecord();
    },
    _onButtonExitClick: function() {
        this.exit();
    },
    _onButtonFormCancelClick: function() {
        this.reset();
        this.activateTabGrid();
    },
    _onButtonFormSubmitClick: function() {
        this.save();
    },
    _onFormStoreLoad: function() {
        var t = this;
        //-- Timeout vermeden activateTabForm yapıldığında t.stores.form için load listener'larının bir kısmı çalışmıyor.
        // 24.04.2017'de $.eachR içinde yapılan değişiklik sonrası setTimeout kapatıldı. Sorun devam ederse tekrar açılacak.
        //setTimeout(function() {
            t.activateTabForm();
        //}, 10);
    },
    _onMainGridRowClick: function(grid, rowIndex) {
        var t = this;
        if (t.buttons.edit) {
            t.buttons.edit.enable();
        }
        if (t.buttons.delete) {
            t.buttons.delete.enable();
        }
    },
    _onMainStoreBeforeLoad: function() {
        this.stores.main.params.menuId = this.id;
    },
    _onMainStoreLoad: function() {
        var t = this;
        t.trigger('mainstoreload', t);
        setTimeout(function() {
            if (t.buttons.edit) {
                t.buttons.edit.disable();
            }
            if (t.buttons.delete) {
                t.buttons.delete.disable();
            }
            //if(t.extras && typeof t.extras['documentId'] == 'undefined' && typeof t.extras['recordId'] != 'undefined'){
            //    t.grids.main.selectRow(t.grids.main, 0, false);
            //    t.editSelectedRecord();
            //}
        }, 10);
        return true;
    },
    _onParentTabChange: function(tab, activeItem){
        var t = this;
        if(activeItem == t.tabs.main.grid) {
            if (t.buttons.edit)
                t.buttons.add.show();
            if (t.buttons.delete)
                t.buttons.delete.show();
            if (t.buttons.edit)
                t.buttons.edit.show();
            if (t.buttons.exit)
                t.buttons.exit.show();
            if (t.buttons.pagerButtons)
                t.buttons.pagerButtons.show();
        } else {
            if (t.buttons.edit)
                t.buttons.add.hide();
            if (t.buttons.delete)
                t.buttons.delete.hide();
            if (t.buttons.edit)
                t.buttons.edit.hide();
            if (t.buttons.exit)
                t.buttons.exit.hide();
            if (t.buttons.pagerButtons)
                t.buttons.pagerButtons.hide();
        }
        if(activeItem == t.tabs.main.form){
            if (t.buttons.formCancel)
                t.buttons.formCancel.show();
            if (t.buttons.formSubmit)
                t.buttons.formSubmit.show();
        } else {
            if (t.buttons.formCancel)
                t.buttons.formCancel.hide();
            if (t.buttons.formSubmit)
                t.buttons.formSubmit.hide();
        }
        setTimeout(function(){
            t.win.body.perfectScrollbar('update');
            // Bazı client'larda scrollbar update gerçekleşmemesi halinde alttaki süre arttırılabilir.
        }, 50);
    },
    _onReset: function(_forms, _data) {
        var t = this;
        if (typeof _data != "undefined") {
            t.stores.main.preloaded = true;
            t.stores.main.data = _data;
        }
        t.stores.main.load();
    },
    activateTabForm: function() {
        this.tabs.main.form.activate();
        this.trigger('activate-tab-form');
    },
    activateTabGrid: function() {
        this.tabs.main.grid.activate();
        this.trigger('activate-tab-grid');
    },
    deleteRecord: function() {
        var t = this,
            _f = this.forms;
        var selectedRecord = t.getSelectedRecord();
        if (selectedRecord !== false) {
            mj.message({
                title: mj.lng.glb.winTitleOnay,
                text: mj.lng.glb.delQuestion,
                type: "warning",
                showCancelButton: true,
                confirmButtonText: mj.lng.glb.del
            }, function(isConfirm) {
                if (isConfirm) {
                    t.win.mask.show();
                    var id = selectedRecord.id;
                    var version = selectedRecord.version;
                    var routeName = t.modulename + '-del';
                    var route = Routing.getRoute(routeName);
                    var pagerParams = (t.grids && t.grids.main && t.grids.main.pbar) ? {
                        'pg': t.grids.main.pbar.current,
                        'lm': t.grids.main.pbar.limit
                    } : {};
                    var url = Routing.generate(routeName, id ? mj.apply(pagerParams, {
                        'id': id,
                        'v': version
                    }) : pagerParams);
                    if (route && route.requirements && route.requirements._method) {
                        var data = {'menuId': t.id};
                        $.ajax({
                            url: url,
                            method: route.requirements._method,
                            dataType: 'json',
                            data: JSON.stringify(data),
                            success: function(data, textStatus, jqXHR) {
                                t.reset(_f, data);
                            },
                            error: function(jqXHR, textStatus, errorThrown) {
                                mj.message({
                                    title: mj.lng.glb.error,
                                    text: jqXHR.responseJSON.message,
                                    type: "error",
                                    html: true
                                });
                            },
                            complete: function(jqXHR, textStatus) {
                                t.win.mask.hide();
                            }
                        });
                    }
                }
            });
        }
    },
    editData: function(data) {
        this.forms.main.store.preloaded = true;
        this.forms.main.store.data = [data];
        this.forms.main.store.load();
    },
    editSelectedRecord: function() {
        var selectedRecord = this.getSelectedRecord();
        if (selectedRecord !== false)
            this.editData(selectedRecord);
    },
    exit: function() {
        this.win.close();
    },
    getSelectedRecord: function() {
        return (this.grids && this.grids.main && this.grids.main.selectedRow) ? this.grids.main.selectedRow.data : false;
    },
    initDefaultButtons: function() {
        var t = this;
        t.buttons = {};
        if (t.audit)
            t.buttons.auditSwitch = t.win.addButton({
                alt: mj.lng.glb.auditShowDeleted,
                iconcls: 'fa-trash',
                cls: 'btn-sm left',
                scope: t,
                handler: t._onButtonauditSwitchClick
            });
        if (t.windowButtons.exit)
            t.buttons.exit = t.win.addButton({
                alt: mj.lng.glb.exit,
                iconcls: 'fa-sign-out',
                scope: t,
                handler: t._onButtonExitClick
            });
        if (t.windowButtons.add)
            t.buttons.add = t.win.addButton({
                alt: mj.lng.glb.add,
                cls: 'btn-sm left',
                iconcls: 'fa-plus-square-o',
                scope: t,
                handler: t._onButtonAddClick
            });
        if (t.windowButtons.delete)
            t.buttons.delete = t.win.addButton({
                alt: mj.lng.glb.del,
                cls: 'btn-sm left',
                iconcls: 'fa-minus-square-o',
                disabled: true,
                scope: t,
                handler: t._onButtonDeleteClick
            });
        if (t.windowButtons.edit)
            t.buttons.edit = t.win.addButton({
                alt: mj.lng.glb.edit,
                cls: 'btn-sm left',
                iconcls: 'fa-pencil',
                disabled: true,
                scope: t,
                handler: t._onButtonEditClick
            });
        if (t.windowButtons.formSubmit)
            t.buttons.formSubmit = t.win.addButton({
                title: mj.lng.glb.save,
                cls: 'btn-sm left',
                iconcls: 'fa-floppy-o',
                invisible: true,
                scope: t,
                handler: t._onButtonFormSubmitClick
            });
        if (t.windowButtons.formCancel)
            t.buttons.formCancel = t.win.addButton({
                title: mj.lng.glb.cancel,
                cls: 'btn-sm left',
                iconcls: 'fa-ban',
                invisible: true,
                scope: t,
                handler: t._onButtonFormCancelClick
            });

        if (typeof t.gridConfig != "undefined")
            t.buttons.pagerButtons = $('.btn-pager', t.win.buttonCnt);
    },
    newRecord: function() {
        this.resetForms();
        this.activateTabForm();
    },
    reset: function(_forms, _data) {
        this.resetForms(_forms, _data);
        this.trigger('reset', _forms, _data);
    },
    resetForms: function(_forms, _data) {
        var t = this;
        if (!_forms) {
            _forms = t.forms;
        }
        for (var f in _forms) {
            if (typeof t.forms[f] != 'undefined' && typeof t.forms[f] != 'function' && typeof t.forms[f].clear == 'function') {
                t.forms[f].clear();
            }
        }
        if (t.win && typeof t.win.setTitle == 'function') {
            t.win.setTitle(t.winTitle);
        }
    },
    save: function() {
        var t = this,
            _f = this.forms,
            f = this.forms.main;
        t.trigger('beforesave');
        if (f.modulename && typeof Routing !== 'undefined') {
            if (f.validate()) {
                var id = f.getValue('id').id;
                var versionField = f.getField('version');
                var version = typeof versionField != "undefined" ? f.getValue('version').version : 0;
                var mode = id ? 'update' : 'add';
                var routeName = f.modulename + '-' + mode;
                var route = Routing.getRoute(routeName);
                var pagerParams = (t.grids && t.grids.main && t.grids.main.pbar) ? {
                    'pg': t.grids.main.pbar.current,
                    'lm': t.grids.main.pbar.limit
                } : {};
                var url = Routing.generate(routeName, id ? mj.apply(pagerParams, {
                    'id': id,
                    'v': version
                }) : pagerParams);
                var data = f.getValue();
                data.menuId = t.id;
                if (route && route.requirements && route.requirements._method) {
                    t.win.mask.show();
                    $.ajax({
                        url: url,
                        method: route.requirements._method,
                        data: JSON.stringify(data),
                        contentType: 'application/json',
                        dataType: 'json',
                        success: function(data, textStatus, jqXHR) {
                            t.reset(_f, data);
                            t.activateTabGrid();
                        },
                        error: function(jqXHR, textStatus, errorThrown) {
                            mj.message({
                                title: mj.lng.glb.error,
                                text: jqXHR.responseJSON.message,
                                type: "error",
                                html: true
                            });
                        },
                        complete: function(jqXHR, textStatus) {
                            t.win.mask.hide();
                        }
                    });
                }
            }
        }
    }
};
mj.extend(baseObj, mj.component);
