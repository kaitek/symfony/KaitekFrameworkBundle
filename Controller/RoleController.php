<?php
/*
 * Role tanımları sayfasının controller kodu
 * role,roleright,customright(role kısmı) tek sayfada olacak
 * gelen değer post-put metodları içinde parse edilerek kayıt işlemleri yapılacak
 *
 */

namespace Kaitek\Bundle\FrameworkBundle\Controller;

use Kaitek\Bundle\FrameworkBundle\Entity\Role;
use Kaitek\Bundle\FrameworkBundle\Entity\RoleRight;
use Kaitek\Bundle\FrameworkBundle\Entity\CustomRight;

use Kaitek\Bundle\FrameworkBundle\Controller\BaseController as ControllerBase;
use Kaitek\Bundle\FrameworkBundle\Controller\BasePagingControllerInterface;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class RoleController extends ControllerBase implements BasePagingControllerInterface
{
    public const ENTITY = 'KaitekFrameworkBundle:Role';

    public function __construct()
    {
        parent::__construct();
    }

    public function getNewEntity()
    {
        return new Role();
    }

    /**
     * @Route(path="/Role/{pg}/{lm}/{id}/{v}", requirements={"pg": "\d+","lm": "\d+", "id": "\d+","v": "\d+"}, name="Role-del", options={"expose"=true}, methods={"DELETE"}), methods={"DELETE"}
     */
    public function deleteAction(Request $request, $_locale, $pg, $lm, $id, $v)
    {
        $entity = $this->getDoctrine()
            ->getRepository('KaitekFrameworkBundle:Role')
            ->find($id);
        return $this->recordDelete($request, $entity, $id, $v, $_locale, $pg, $lm);
    }

    public function getAllRecords($scope, $request, $page=1, $limit=25)
    {
        $records = parent::getAllRecords($scope, $request, $page, $limit);
        $repository=$this->getDoctrine()->getRepository('KaitekFrameworkBundle:Menu');
        $tr = $this->get('translator');
        foreach($records["Role"]["records"] as $key=>$record) {
            $records["Role"]["records"][$key]["rights"] = $repository->loadUserMenuTreeWithRoleRights($record["id"], $tr, 'text', 'children');
        }
        return $records;
    }

    public function getQBQuery()
    {
        $queries=array();
        $em = $this->getDoctrine()->getManager();
        $qb = $em->createQueryBuilder();
        $qb=$qb->select('r')
                ->from('KaitekFrameworkBundle:Role', 'r')
                ->orderBy('r.name', 'ASC');
        $queries["Role"]=array("qb"=>$qb,"getAll"=>true);
        return $queries;
    }

    /**
     * @Route(path="/Role/{pg}/{lm}", requirements={"pg": "\d+","lm": "\d+"}, name="Role-add", options={"expose"=true}, methods={"POST"})
     */
    public function postAction(Request $request, $_locale, $pg, $lm)
    {
        return $this->recordAdd($request, $_locale, $pg, $lm);
    }

    /**
     * @Route(path="/Role/{pg}/{lm}/{id}/{v}", requirements={"pg": "\d+","lm": "\d+", "id": "\d+","v": "\d+"}, name="Role-update", options={"expose"=true}, methods={"PUT"})
     */
    public function putAction(Request $request, $_locale, $pg, $lm, $id, $v)
    {
        if(!isset($this->_requestData)) {
            $this->_requestData = json_decode($request->getContent());
        }

        $user = $this->getUser();
        $userId = $user->getId();
        if(is_array($this->_requestData)) {
            try {
                $em = $this->getDoctrine()->getManager();
                $em->getConnection()->beginTransaction();
                $validator = $this->get('validator');
                foreach($this->_requestData as $d) {
                    if($d->role_right_id && $d->version && !$d->aRight && !$d->uRight && !$d->dRight && !$d->gRight && !$d->uOwnRight && !$d->dOwnRight) {
                        //delete
                        $entity = $this->getDoctrine()
                            ->getRepository('KaitekFrameworkBundle:RoleRight')
                            ->find($d->role_right_id);
                        $cbd = $this->checkBeforeDelete($request, $d->role_right_id, $entity, $d->version);
                        if ($cbd === true) {
                            $entity->setDeleteuserId($userId);
                            $em->persist($entity);
                            $em->flush();
                            $em->remove($entity);
                            $em->flush();
                        } else {
                            $em->getConnection()->rollback();
                            return $cbd;
                        }
                    } elseif ($d->aRight || $d->uRight || $d->dRight || $d->gRight || $d->uOwnRight || $d->dOwnRight) {
                        if($d->role_right_id && $d->version) {
                            //update
                            $entity = $this->getDoctrine()
                                ->getRepository('KaitekFrameworkBundle:RoleRight')
                                ->find($d->role_right_id);
                            $cbu = $this->checkBeforeUpdate($request, $d->role_right_id, $entity, $d->version);
                            if ($cbu === true) {
                                $entity->setUpdateuserId($userId);
                            } else {
                                $em->getConnection()->rollback();
                                return $cbu;
                            }
                        } else {
                            //insert
                            $cba=$this->checkBeforeAdd($request);
                            if($cba===true) {
                                $role = $this->getDoctrine()
                                    ->getRepository('KaitekFrameworkBundle:Role')
                                    ->find($id);

                                $menu = $this->getDoctrine()
                                    ->getRepository('KaitekFrameworkBundle:Menu')
                                    ->find($d->menu_id);

                                $entity = new RoleRight();
                                $entity->setCreateuserId($userId);
                                $entity->setRole($role);
                                $entity->setMenuId($menu);
                                $errors = $this->getValidateMessage($validator->validate($entity));
                                if ($errors!==false) {
                                    $em->getConnection()->rollback();
                                    return $errors;
                                }
                            } else {
                                $em->getConnection()->rollback();
                                return $cba;
                            }
                        }
                        $entity->setGRight($d->gRight);
                        $entity->setARight($d->aRight);
                        $entity->setURight($d->uRight);
                        $entity->setDRight($d->dRight);
                        $entity->setUOwnRight($d->uOwnRight);
                        $entity->setDOwnRight($d->dOwnRight);
                        $em->persist($entity);
                        $em->flush();
                    }
                }
                $em->getConnection()->commit();
            } catch (Exception $e) {
                // Rollback the failed transaction attempt
                $em->getConnection()->rollback();
                //throw $e;
                return $this->msgError($e->getMessage());
            }
            return $this->showAllAction($request, $_locale, $pg, $lm);
        } else {
            /* @var $entity Role */
            $entity = $this->getDoctrine()
                ->getRepository('KaitekFrameworkBundle:Role')
                ->find($id);
            return $this->recordEdit($request, $entity, $id, $v, $_locale, $pg, $lm);
        }
    }

    /**
     * @Route(path="/Role", name="Role-module", options={"expose"=true}, methods={"POST"})
     */
    public function renderBackendModule(Request $request, $_locale)
    {
        $cbg = $this->checkBeforeGet($request);
        //$cbg=true;
        if ($cbg === true) {
            $data = $this->getBackendData($request, $_locale, self::ENTITY);

            return $this->render('@KaitekFramework/Backend/Role.html.twig', $data);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/Role/{id}", requirements={"id": "\d+"}, name="Role-show", options={"expose"=true}, methods={"GET"})
     */
    public function showAction(Request $request, $_locale, $id)
    {
        $cbg=$this->checkBeforeGet($request);
        if($cbg===true) {
            $records=$this->getRecordById($this, $request, "role", $id);
            return new JsonResponse($records);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/Role/all/{pg}/{lm}", requirements={"pg": "\d+","lm": "\d+"}, name="Role-showall", options={"expose"=true}, methods={"GET"})
     */
    public function showAllAction(Request $request, $_locale, $pg, $lm)
    {
        $cbg=$this->checkBeforeGet($request);
        if($cbg===true) {
            $records=$this->getAllRecords($this, $request, $pg, $lm);
            return new JsonResponse($records);
        } else {
            return $cbg;
        }
    }
}
