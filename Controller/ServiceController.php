<?php

namespace Kaitek\Bundle\FrameworkBundle\Controller;

use Kaitek\Bundle\FrameworkBundle\Controller\BaseController as ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class ServiceController extends ControllerBase //implements BaseControllerInterface
{
    /**
     * @var Container $container
     */
    protected $container;
    /**
     * Constructor
     * 
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        $this->container = $container;
    }
}
