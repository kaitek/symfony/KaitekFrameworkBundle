<?php

namespace Kaitek\Bundle\FrameworkBundle\Controller;

use Kaitek\Bundle\FrameworkBundle\Entity\Menu;

use Kaitek\Bundle\FrameworkBundle\Controller\BaseController as ControllerBase;
use Kaitek\Bundle\FrameworkBundle\Controller\BaseControllerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class MenuController extends ControllerBase implements BaseControllerInterface
{
    public const ENTITY = 'KaitekFrameworkBundle:Menu';

    public function __construct()
    {
        parent::__construct();
    }

    public function getNewEntity()
    {
        return new Menu();
    }

    private function deleteChildren($parentid, $em, $dUserId)
    {
        $entities = $em->createQueryBuilder()->select('m')
            ->from(self::ENTITY, 'm')
            ->where('m.parentid = :parentid')
            ->setParameters(array('parentid' => $parentid))
            ->getQuery()
            ->getResult();
        foreach($entities as  $en) {
            $en->setDeleteuserId($dUserId);
            $em->persist($en);
            $em->flush();
            $this->deleteChildren($en->getId(), $em, $dUserId);
            $em->remove($en);
            $em->flush();
        }
    }
    /**
     * @Route(path="/Menu/{id}/{v}", requirements={"id": "\d+","v": "\d+"}, name="Menu-del", options={"expose"=true}, methods={"DELETE"})
     */
    public function deleteAction(Request $request, $_locale, $id, $v)
    {
        $entity = $this->getDoctrine()
            ->getRepository(self::ENTITY)
            ->find($id);
        $cbd=$this->checkBeforeDelete($request, $id, $entity, $v);
        //$cbd=true;
        if($cbd===true) {
            $em = $this->getDoctrine()->getManager();
            $user = $this->getUser();
            $dUserId = $user->getId();
            $em->getConnection()->beginTransaction();
            try {
                $entity->setDeleteuserId($dUserId);
                $em->persist($entity);
                $em->flush();

                $entities = $em->createQueryBuilder()->select('m')
                    ->from(self::ENTITY, 'm')
                    ->where('m.parentid = :parentid')
                    ->andWhere('m.sira > :sira')
                    ->setParameters(array('parentid' => $entity->getParentId(), 'sira' => $entity->getSira()))
                    ->getQuery()
                    ->getResult();
                foreach($entities as  $en) {
                    $en->setSira($en->getSira()-1);
                    $em->persist($en);
                    $em->flush();
                }

                $entities = $em->createQueryBuilder()->select('r')
                    ->from('KaitekFrameworkBundle:RoleRight', 'r')
                    ->where('r.menuId = :menuId')
                    ->setParameters(array('menuId' => $entity->getId()))
                    ->getQuery()
                    ->getResult();
                foreach($entities as  $en) {
                    $en->setDeleteuserId($dUserId);
                    $em->persist($en);
                    $em->flush();
                    $em->remove($en);
                    $em->flush();
                }

                $entities = $em->createQueryBuilder()->select('r')
                    ->from('KaitekFrameworkBundle:UserRight', 'r')
                    ->where('r.menuId = :menuId')
                    ->setParameters(array('menuId' => $entity->getId()))
                    ->getQuery()
                    ->getResult();
                foreach($entities as  $en) {
                    $en->setDeleteuserId($dUserId);
                    $em->persist($en);
                    $em->flush();
                    $em->remove($en);
                    $em->flush();
                }

                $this->deleteChildren($id, $em, $dUserId);
                $em->remove($entity);
                $em->flush();
                $em->getConnection()->commit();
            } catch (Exception $e) {
                // Rollback the failed transaction attempt
                $em->getConnection()->rollback();
                //throw $e;
                return $this->msgError($e->getMessage());
            }
            return $this->showAllAction($request, $_locale, 1, 1000);
        } else {
            return $cbd;
        }
    }

    public function getQBQuery()
    {
        $queries=array();
        $em = $this->getDoctrine()->getManager();
        $qb = $em->createQueryBuilder();
        $qb=$qb->select('m.id,m.sira,m.parentid,m.menuId,m.title,m.url
                    ,m.width,m.height,m.modulename,m.iconcls,m.script
                    ,m.multiplewindow,m.maximizable,m.resizable,m.mobiledevice,m.version')
                ->from(self::ENTITY, 'm')
                ->where('m.deleteuserId is null')
                ->orderBy('m.sira', 'ASC');
        $queries["Menu"]=array("qb"=>$qb,"getAll"=>true);
        return $queries;
    }

    /**
     * @Route(path="/Menu/", name="Menu-add", options={"expose"=true}, methods={"POST"})
     */
    public function postAction(Request $request, $_locale)
    {
        if(!isset($this->_requestData)) {
            $this->_requestData = json_decode($request->getContent());
        }
        $sira = $this->getDoctrine()->getManager()->createQueryBuilder()
            ->select('max(m.sira)')
            ->from(self::ENTITY, 'm')
            ->where('m.parentid = :parentid')
            ->setParameters(array('parentid' => $this->getVarIfEmpty('parentid', 0)))
            ->getQuery()
            ->getSingleScalarResult();
        if($sira === null) {
            $sira = 0;
        } else {
            $sira = $sira+1;
        }
        $this->_requestData->sira = (int)$sira;
        return $this->recordAdd($request, $_locale, 1, 1000);
    }

    /**
     * @Route(path="/Menu/{id}/{v}", requirements={"id": "\d+","v": "\d+"}, name="Menu-update", options={"expose"=true}, methods={"PUT"})
     */
    public function putAction(Request $request, $_locale, $id, $v)
    {
        /* @var $entity Menu */
        $entity = $this->getDoctrine()
            ->getRepository(self::ENTITY)
            ->find($id);

        return $this->recordEdit($request, $entity, $id, $v, $_locale, 1, 1000);
    }

    /**
     * @Route(path="/Menu", name="Menu-module", options={"expose"=true}, methods={"POST"})
     */
    public function renderBackendModule(Request $request, $_locale)
    {
        $cbg=$this->checkBeforeGet($request);
        //$cbg=true;
        if($cbg===true) {
            //$_rpp=$this->container->getParameter('kaitek_framework.recordsperpage');
            $_rpp = 1000;
            $records=$this->getAllRecords($this, $request, 1, $_rpp);
            $repository=$this->getDoctrine()->getRepository(self::ENTITY);
            $tr = $this->get('translator');
            $menus = $repository->loadUserMenuTree($records["Menu"]["records"], $tr, 'text', 'children');
            $records["Menu"]["records"]=$menus;
            $records["Menu"]["totalProperty"]=count($menus);
            //return new JsonResponse($records);
            $data = array(
                "modulename" => $request->request->get('modulename')
                , "data" => $records
                , "audit" => 0
                , "extras" => array()
            );
            return $this->render('@KaitekFramework/Backend/Menu.html.twig', $data);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/Menu/{id}", requirements={"id": "\d+"}, name="Menu-show", options={"expose"=true}, methods={"GET"})
     */
    public function showAction(Request $request, $_locale, $id)
    {
        $cbg=$this->checkBeforeGet($request);
        //$cbg=true;
        if($cbg===true) {
            $records=$this->getRecordById($this, $request, "menu", $id);
            return new JsonResponse($records);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/Menu/all/{page}/{limit}", defaults={"pg": 1, "lm": 1000}, requirements={"pg": "\d+","lm": "\d+"}, name="Menu-showall", options={"expose"=true}, methods={"GET"})
     */
    public function showAllAction(Request $request, $_locale, $pg, $lm)
    {
        $cbg=$this->checkBeforeGet($request);
        //$cbg=true;
        if($cbg===true) {
            //$_rpp=$this->container->getParameter('kaitek_framework.recordsperpage');
            $records=$this->getAllRecords($this, $request, $pg, $lm);
            $repository=$this->getDoctrine()->getRepository(self::ENTITY);
            $tr = $this->get('translator');
            $menus = $repository->loadUserMenuTree($records["Menu"]["records"], $tr, 'text', 'children');
            $records["Menu"]["records"]=$menus;
            $records["Menu"]["totalProperty"]=count($menus);
            return new JsonResponse($records);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/Menu/{id}/{v}", requirements={"id": "\d+","v": "\d+"}, name="Menu-order", options={"expose"=true}, methods={"POST"})
     */
    public function saveTreeOrderAction(Request $request, $_locale, $id, $v)
    {
        /* @var $entity Menu */

        if($id == null) {
            return $this->msgError($this->get('translator')->trans('err.main.not_found', array(), 'KaitekFrameworkBundle'));
        } else {
            $entity = $this->getDoctrine()
                ->getRepository(self::ENTITY)
                ->find($id);
            $cbu=$this->checkBeforeUpdate($request, $id, $entity, $v);
            if($cbu===true) {
                if(!isset($this->_requestData)) {
                    $this->_requestData = json_decode($request->getContent());
                }

                $oldParentId = $this->getVar('oldParentId');
                $oldOrder = $this->getVar('oldOrder');
                $newParentId = $this->getVar('newParentId');
                $newOrder = $this->getVar('newOrder');

                $em = $this->getDoctrine()->getManager();
                $entities = $em->createQueryBuilder()->select('m')
                    ->from(self::ENTITY, 'm')
                    ->where('m.parentid = :parentid')
                    ->andWhere('m.sira > :sira')
                    ->setParameters(array('parentid' => $oldParentId, 'sira' => $oldOrder))
                    ->getQuery()
                    ->getResult();
                foreach($entities as  $en) {
                    $en->setSira($en->getSira()-1);
                    $em->persist($en);
                }
                $entities = $em->createQueryBuilder()->select('m')
                    ->from(self::ENTITY, 'm')
                    ->where('m.parentid = :parentid')
                    ->andWhere('m.sira >= :sira')
                    ->setParameters(array('parentid' => $newParentId, 'sira' => $newOrder))
                    ->getQuery()
                    ->getResult();
                foreach($entities as  $en) {
                    $en->setSira($en->getSira()+1);
                    $em->persist($en);
                }

                $entity->setSira($newOrder);
                $entity->setParentId($newParentId);
                $em->persist($entity);

                $em->getConnection()->beginTransaction();
                try {
                    $em->flush();
                    $em->getConnection()->commit();
                } catch (Exception $e) {
                    // Rollback the failed transaction attempt
                    $em->getConnection()->rollback();
                    //throw $e;
                    return $this->msgError($e->getMessage());
                }
                return $this->showAllAction($request, $_locale, 1, 1000);
            } else {
                return $cbu;
            }
        }
    }

}
