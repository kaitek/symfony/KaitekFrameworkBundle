<?php

/*
 * Copyright (c) 2015, oguzhan
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

namespace Kaitek\Bundle\FrameworkBundle\Controller;

use Doctrine\DBAL\LockMode;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\ORM\QueryBuilder;
use Kaitek\Bundle\FrameworkBundle\Model\User;
use Kaitek\Bundle\FrameworkBundle\ORM\Mapping\FieldData;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\User\UserInterface;
#use Psr\Container\ContainerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
#use Symfony\Component\Translation\Translator;
use SimpleThings\EntityAudit\AuditConfiguration;

abstract class BaseController extends AbstractController
{
    /**
     * @var string
     */
    public const QUERY_TYPE_SQL = 'SQL';
    /**
     * @var string
     */
    public const QUERY_TYPE_DQL = 'DQL';
    /**
     * @var string
     */
    public const QUERY_TYPE_DQL_QB = 'DQL_QB';

    /**
     * @var string
     */
    protected $_queryType=self::QUERY_TYPE_DQL_QB;

    /**
     * @var \stdClass
     */
    protected $_requestData;

    /**
     * @var AnnotationReader
     */
    protected $annotationReader;

    /**
     * @var ContainerInterface
     */
    protected $_container;
    /**
     * @var RequestStack
     */
    protected $requestStack;

    protected $isDAVRequest;

    public function __construct(...$params)
    {
        $this->annotationReader = new AnnotationReader();

        $number_of_arguments = func_num_args();
        if ($number_of_arguments == 2) {//servis üzerinden gelmesi durumu
            //    // __construct(RequestStack $requestStack = null, ContainerInterface $container = null)
            $this->_setRequestStack($params[0]);
            $this->_setContainer($params[1]);
        }
    }

    protected function checkAuthentication(Request $request, $_locale)
    {
        try {
            if ($_locale=='') {
                $_locale=($this->_container==null ? $this->container : $this->_container)->getParameter('locale');
                $url = $request->get('_route');
                $response = new RedirectResponse(($this->_container==null ? $this->container : $this->_container)->get('router')->generate($url, array('_locale' => $_locale)));
                return $response;
            }
        } catch (\Exception $ex) {
        }

        $session = $request->getSession();
        $session->set('_locale', $_locale);
        $loginId='';
        $lastUsername='';
        if (class_exists('\Symfony\Component\Security\Core\Security')) {
            $authErrorKey = Security::AUTHENTICATION_ERROR;
            $lastUsernameKey = Security::LAST_USERNAME;
        } else {
            // BC for SF < 2.6
            $authErrorKey = SecurityContextInterface::AUTHENTICATION_ERROR;
            $lastUsernameKey = SecurityContextInterface::LAST_USERNAME;
        }
        $menus=array();
        $roles=array();
        //if($this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY', null)) {
            $user = $this->getUser();
            // last username entered by the user
            //$lastUsername = (null === $session) ? '' : (null===$session->get($lastUsernameKey)) ? (null === $user)?'': $user->getUsername() : $session->get($lastUsernameKey);
            $loginId = ((null === $session) ? '' : ((null === $user) ? '' : $user->getUsername()));
            if ($user != null) {
                $fullname = $user->getFullname();
                $fullname = $fullname != "" && $fullname != null ? $fullname : $loginId;
            }
            $lastUsername = ((null === $session) ? '' : ((null === $user) ? '' : $fullname));
            $roles=(null === $user) ? '' : $user->getRoles();
            /*
            //1-controller üzerinden
            $menus=$this->getMenu();
            dump($menus);
            //2-controller üzerinden service ile
            $sc=$this->container->get('kaiframework.servicecontroller');
            $menus=$sc->getMenu();
            dump($menus);
            */
            //3-repository üzerinden
            $repository=$this->getDoctrine()->getRepository('KaitekFrameworkBundle:Menu');
            $em = $this->getDoctrine()->getManager();
            $af = ($this->_container==null ? $this->container : $this->_container)->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
            $ru = ($this->_container==null ? $this->container : $this->_container)->get('security.authorization_checker')->isGranted('ROLE_USER');
            $ra = ($this->_container==null ? $this->container : $this->_container)->get('security.authorization_checker')->isGranted('ROLE_ADMIN');
            $tr = ($this->_container==null ? $this->container : $this->_container)->get('translator');
            //dump($tr);
            $userId = $user->getId();
            //$menus = $repository->loadUserMenu($em,$af,$ru,$ra,$tr,$userId);
            //dump($menus);
            //$customRight = $em->getRepository('KaitekFrameworkBundle:CustomRight')->loadCustomRightByUserInfo($userId,'Consultant');
            //dump($customRight);
            $menus = $repository->loadUserMenuQB($em, $af, $ru, $ra, $tr, $userId);
            //dump($menus);
            //$m1=$repository->loadUserRightMenuQB($em,$af,$ru,$ra,$tr,$userId,2);
            //dump($m1);
        }

        // get the error if any (works with forward and redirect -- see below)
        if ($request->attributes->has($authErrorKey)) {
            $error = $request->attributes->get($authErrorKey);
        } elseif (null !== $session && $session->has($authErrorKey)) {
            $error = $session->get($authErrorKey);
            $session->remove($authErrorKey);
        } else {
            $error = null;
        }

        if (!$error instanceof AuthenticationException) {
            $error = null; // The value does not come from the security component.
        }

        //if ($this->has('security.csrf.token_manager')) {
        $csrfToken = ($this->_container==null ? $this->container : $this->_container)->get('security.csrf.token_manager')->getToken('authenticate')->getValue();
        //} else {
        //    // BC for SF < 2.4
        //    $csrfToken = $this->has('form.csrf_provider')
        //        ? $this->get('form.csrf_provider')->generateCsrfToken('authenticate')
        //        : null;
        //}

        $module = $request->get('editModule');
        $menuItem = false;
        if ($module != "") {
            foreach ($menus as $menuI) {
                if ($menuI['modulename'] == $module) {
                    $menuItem = $menuI;
                    break;
                } elseif (isset($menuI['items']) && count($menuI['items'])>0) {
                    foreach ($menuI['items'] as $menuJ) {
                        if ($menuJ['modulename'] == $module) {
                            $menuItem = $menuJ;
                            break;
                        }
                    }
                }
            }
            if ($this->isGranted('IS_AUTHENTICATED_FULLY', null)) {
                if ($menuItem == false) {
                    return $this->msgError(
                        ($this->_container==null ? $this->container : $this->_container)->get('translator')->trans('err.main.process_authorize', array(), 'KaitekFrameworkBundle'),
                        401
                    );
                }
            }
        }

        $data=array(
            'last_username' => $lastUsername,
            'loginId' => $loginId,
            'role' => $roles,
            'error' => $error,
            'csrf_token' => $csrfToken,
            'menu'=>$menus,
            'editModule' => $menuItem,
            'module' => $module,
            'id' => $request->get('editRecordId'),
            'documentId' => $request->get('documentId'),
            'docTypeEntityId' => $request->get('docTypeEntityId'),
            'focusField' => $request->get('focusField')
        );
        return $data;
    }

    public function _setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
        $this->_container = $container;
    }

    public function _setRequestStack(RequestStack $requestStack = null)
    {
        $this->requestStack = $requestStack;
    }

    public function beforeFilter(Request $request)
    {
        // $this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'Unable to access this page!')
        //if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
        if ($request->attributes->get('_isDAV') === true||$request->attributes->get('_isDCSService') === true) {
            return true;
        }
        if (!$this->isGranted('IS_AUTHENTICATED_FULLY', null)) {
            //throw $this->createAccessDeniedException();
            if ($request->isXmlHttpRequest()!=true) {
                return $this->redirectToRoute('fos_user_security_logout');
            } else {
                return $this->msgError(($this->_container==null ? $this->container : $this->_container)->get('translator')->trans('security.login.not_logged_in', array(), 'KaitekFrameworkBundle'), 403);
            }
        }
        //if (!$this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
        if (!$this->isGranted('ROLE_USER', null)) {
            //throw $this->createAccessDeniedException();
            if ($request->isXmlHttpRequest()!=true) {
                return $this->redirectToRoute('fos_user_security_logout');
            } else {
                return $this->msgError(($this->_container==null ? $this->container : $this->_container)->get('translator')->trans('security.login.not_logged_in', array(), 'KaitekFrameworkBundle'), 403);
            }
        }

        //if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
        if (!$this->isGranted('ROLE_ADMIN', null)) {
            return true;
        } else {
            $user = $this->getUser();
            $sessionId=$request->getSession()->getId();
            $em = $this->getDoctrine()->getManager();
            $_ms=($this->_container==null ? $this->container : $this->_container)->getParameter('kaitek_framework.multiplesessions');
            $onlineUser = $em->getRepository('KaitekFrameworkBundle:Online')->loadOnlineByUserInfo($user, $sessionId, $_ms);
            if (null === $onlineUser/*$onlineUser instanceof OnlineInterface*/) {
                return false;
            } else {
                return true;
            }
        }
    }

    public function canonicalize($string)
    {
        return null === $string ? null : mb_convert_case($string, MB_CASE_LOWER, mb_detect_encoding($string));
    }

    public function checkBeforeAdd(Request $request)
    {
        $cp=$this->controlParams($request);
        if ($cp!==true) {
            return $cp;
        }
        if (/*$request->attributes->get('_isDAV') === true||*/$request->attributes->get('_isDCSService') === true) {
            return true;
        }
        return $this->checkRightAdd($request);
    }

    public function checkBeforeDelete(Request $request, $id, $entity, $v)
    {
        $cp=$this->controlParams($request);
        if ($cp!==true) {
            return $cp;
        }
        $user = $this->getUser();
        if (!$entity) {
            $message=($this->_container==null ? $this->container : $this->_container)->get('translator')->trans('err.main.not_found', array(), 'KaitekFrameworkBundle');
            return $this->msgError($message);
        }

        //if ($entity->getCreateuserId() < 1  && !($this->_container==null?$this->container:$this->_container)->get('security.authorization_checker')->isGranted('ROLE_ADMIN')){
        //    $message=($this->_container==null?$this->container:$this->_container)->get('translator')->trans('err.main.system_record_cant_deleted', array(), 'KaitekFrameworkBundle');
        //    return $this->msgError($message);
        //}

        if ($v > 0) {
            try {
                $em = $this->getDoctrine()->getManager();
                $em->lock($entity, LockMode::OPTIMISTIC, $v);
            } catch (OptimisticLockException $e) {
                $message=($this->_container==null ? $this->container : $this->_container)->get('translator')->trans('err.main.record_change_delete', array(), 'KaitekFrameworkBundle');
                return $this->msgError($message);
            }
        }
        return $this->checkRightDelete($request, $entity, $user);
    }

    public function checkBeforeGet(Request $request)
    {
        $cp=$this->controlParams($request);
        if ($cp!==true) {
            return $cp;
        }
        return $this->checkRightGet($request);
    }

    public function checkBeforeUpdate(Request $request, $id, $entity, $v)
    {
        $cp=$this->controlParams($request);
        if ($cp!==true) {
            return $cp;
        }
        if (/*$request->attributes->get('_isDAV') === true||*/$request->attributes->get('_isDCSService') === true) {
            return true;
        }
        $user = $this->getUser();
        $userId = $user->getId();
        if (!$entity) {
            $message=($this->_container==null ? $this->container : $this->_container)->get('translator')->trans('err.main.not_found', array(), 'KaitekFrameworkBundle');
            return $this->msgError($message);
        }
        //try {
        //    if ($entity->getCreateuserId() < 1  && !($this->_container==null?$this->container:$this->_container)->get('security.authorization_checker')->isGranted('ROLE_ADMIN')){
        //        $message=($this->_container==null?$this->container:$this->_container)->get('translator')->trans('err.main.system_record_cant_modified', array(), 'KaitekFrameworkBundle');
        //        return $this->msgError($message);
        //    }
        //} catch (\Throwable $th) {
        //throw $th;
        //}
        if ($v > 0) {
            try {
                $em = $this->getDoctrine()->getManager();
                $em->lock($entity, LockMode::OPTIMISTIC, $v);
            } catch (OptimisticLockException $e) {
                $message=($this->_container==null ? $this->container : $this->_container)->get('translator')->trans('err.main.record_change_update', array(), 'KaitekFrameworkBundle');
                return $this->msgError($message);
            }
        }
        return $this->checkRightUpdate($request, $entity, $userId);
    }

    /**
     * Returns the user have right to access.
     *
     * @param Request $request Request object
     *
     * @return boolean
     */
    public function checkRight(Request $request)
    {
        if (!($this->_container==null ? $this->container : $this->_container)->get('security.authorization_checker')->isGranted('ROLE_USER')) {
            //throw $this->createAccessDeniedException();
            if ($request->isXmlHttpRequest()!=true) {
                return $this->redirectToRoute('fos_user_security_logout');
            } else {
                return $this->msgError(($this->_container==null ? $this->container : $this->_container)->get('translator')->trans('security.login.not_logged_in', array(), 'KaitekFrameworkBundle'), 403);
            }
        }
        $robj=new \stdClass();
        if (($this->_container==null ? $this->container : $this->_container)->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            $robj->isAdmin=true;
            $robj->isUser=false;
            $robj->getRight=true;
            $robj->addRight=true;
            $robj->updRight=true;
            $robj->delRight=true;
            $robj->uOwnRight=true;
            $robj->dOwnRight=true;
        } else {
            $user = $this->getUser();
            $userId = $user->getId();
            $menuId = $request->request->get('menuId');
            if (!$menuId) {
                $menuId = (int) $request->query->get('menuId');
            }
            if (!$menuId) {
                $menuId = $this->getVar('menuId');
            }
            #$sessionId=$request->getSession()->getId();
            $em = $this->getDoctrine()->getManager();
            $af = ($this->_container==null ? $this->container : $this->_container)->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
            $ru = ($this->_container==null ? $this->container : $this->_container)->get('security.authorization_checker')->isGranted('ROLE_USER');
            $ra = ($this->_container==null ? $this->container : $this->_container)->get('security.authorization_checker')->isGranted('ROLE_ADMIN');
            $tr = ($this->_container==null ? $this->container : $this->_container)->get('translator');
            $userRight = $em->getRepository('KaitekFrameworkBundle:Menu')->loadUserRightMenuQB($em, $af, $ru, $ra, $tr, $userId, $menuId);
            if (null === $userRight/*$userRight instanceof UserRightInterface*/) {
                $robj->isAdmin=false;
                $robj->isUser=true;
                $robj->getRight=false;
                $robj->addRight=false;
                $robj->updRight=false;
                $robj->delRight=false;
                $robj->uOwnRight=false;
                $robj->dOwnRight=false;
            } else {
                $robj->isAdmin=false;
                $robj->isUser=true;
                $robj->getRight=$userRight['gRight'];
                $robj->addRight=$userRight['aRight'];
                $robj->updRight=$userRight['uRight'];
                $robj->delRight=$userRight['dRight'];
                $robj->uOwnRight=$userRight['uOwnRight'];
                $robj->dOwnRight=$userRight['dOwnRight'];
            }
        }
        return $robj;
    }

    /**
     * Returns the user custom right.
     *
     * @param int $userId
     * @param string $table
     *
     * @return string
     */
    public function checkCustomRight($userId, $entity, $alias='')
    {
        if (!($this->_container==null ? $this->container : $this->_container)->get('security.authorization_checker')->isGranted('ROLE_USER')) {
            //throw $this->createAccessDeniedException();
            if ($request->isXmlHttpRequest()!=true) {
                return $this->redirectToRoute('fos_user_security_logout');
            } else {
                return $this->msgError(($this->_container==null ? $this->container : $this->_container)->get('translator')->trans('security.login.not_logged_in', array(), 'KaitekFrameworkBundle'), 403);
            }
        }
        $rf=false;
        if (($this->_container==null ? $this->container : $this->_container)->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            return $rf;
        }
        $em = $this->getDoctrine()->getManager();
        $customRight = $em->getRepository('KaitekFrameworkBundle:CustomRight')->loadCustomRightByUserInfo($userId, $entity);
        if (null === $customRight) {
            $rf=false;
        } else {
            if ("array" === getType($customRight)) {
                $customRights=array("where"=>array(),"params"=>array());
                if ($this->getQueryType()==self::QUERY_TYPE_SQL) {
                    //$customRights=array("where"=>"","params"=>array());
                    //$wstr="";
                    $i=0;
                    foreach ($customRight as $item) {
                        //$wstr.=($wstr==""?" ":" or ").strtolower($entity).".".$item->getFieldName()."=:cr_".$item->getFieldName().$i;
                        $customRights["where"][]=strtolower($entity).".".$item->getFieldName()."=:cr_".strtolower($entity)."_".$item->getFieldName().$i;
                        //$wobj.=$wobj==""?" ".$entity.".".$item->getFieldName()."='".$item->getFieldValue()."' ":" or ".$item->getFieldName()."='".$item->getFieldValue()."' ";
                        //$wobj.=$wobj==""?" ".$item->getMethod()."='".$item->getPath()."' ":" or ".$item->getMethod()."='".$item->getPath()."' ";
                        $param=new \stdClass();
                        $param->name="cr_".strtolower($entity)."_".$item->getFieldName().$i;
                        $param->value=$item->getFieldValue();
                        $customRights["params"][]=$param;
                        $i++;
                    }
                    //$robj=$wobj==""?"":" and (".$wobj.") ";
                    if (("array" == getType($customRights["where"])&&count($customRights["where"])==0)||"array" !== getType($customRights["where"])/*$wstr==""*/) {
                        $rf=false;
                    } else {
                        //$customRights["where"]=" and (".$wstr.") ";
                        return $customRights;
                    }
                }
                if ($this->getQueryType()==self::QUERY_TYPE_DQL_QB) {
                    //$customRights=array("where"=>array(),"params"=>array());
                    $i=0;
                    foreach ($customRight as $item) {
                        /*$param=new \stdClass();
                        $param->entity=$entity;
                        $param->name=$item->getFieldName();
                        $param->value=":cr_".$item->getFieldName().$i;
                        $customRights["where"][]=$param;*/
                        $customRights["where"][]=($alias=='' ? $entity : $alias).".".$item->getFieldName()."=:cr_".($alias=='' ? $entity : $alias)."_".$item->getFieldName().$i;
                        $param=new \stdClass();
                        $param->name="cr_".($alias=='' ? $entity : $alias)."_".$item->getFieldName().$i;
                        $param->value=$item->getFieldValue();
                        $customRights["params"][]=$param;
                        $i++;
                    }
                    if (("array" == getType($customRights["where"])&&count($customRights["where"])==0)||"array" !== getType($customRights["where"])) {
                        $rf=false;
                    } else {
                        return $customRights;
                    }
                }
            } else {
                $rf=false;
            }
        }
        return $rf;
    }

    /**
     * Returns the user have right to add record.
     *
     * @param Request $request Request object
     *
     * @return boolean|JsonResponse
     */
    public function checkRightAdd(Request $request)
    {
        $robj=$this->checkRight($request);
        if ($robj->getRight==true&&$robj->addRight==true) {
            return true;
        } else {
            $mtitle = ($this->_container==null ? $this->container : $this->_container)->get('translator')->trans('err.main.message_title', array(), 'KaitekFrameworkBundle');
            $message = ($this->_container==null ? $this->container : $this->_container)->get('translator')->trans('err.main.process_authorize', array(), 'KaitekFrameworkBundle');
            $content = array(
                        'title' => $mtitle,
                        'success' => false,
                        'message' => $message,
                        'file' => ''
            );
            return new JsonResponse($content, 401);
        }
    }

    /**
     * Returns the user have right to delete record.
     *
     * @param Request $request Request object
     * @param BaseInterface $record record object
     * @param User $user User object
     *
     * @return boolean|JsonResponse
     */
    public function checkRightDelete(Request $request, $record, $user)
    {
        $robj=$this->checkRight($request);
        $err=false;
        if ($robj->getRight==true) {
            if ($robj->delRight==true) {
                return true;
            } else {
                if ($robj->dOwnRight==true) {
                    if ($user->getId() == $record->getCuserId()) {
                        return true;
                    } else {
                        $err=true;
                    }
                } else {
                    $err=true;
                }
            }
        } else {
            $err=true;
        }
        if ($err==true) {
            $mtitle = ($this->_container==null ? $this->container : $this->_container)->get('translator')->trans('err.main.message_title', array(), 'KaitekFrameworkBundle');
            $message = ($this->_container==null ? $this->container : $this->_container)->get('translator')->trans('err.main.process_authorize', array(), 'KaitekFrameworkBundle');
            $content = array(
                        'title' => $mtitle,
                        'success' => false,
                        'message' => $message,
                        'file' => ''
            );
            return new JsonResponse($content, 401);
        }
    }

    /**
     * Returns the user have right to list record(s).
     *
     * @param Request $request Request object
     *
     * @return boolean|JsonResponse
     */
    public function checkRightGet(Request $request)
    {
        $robj=$this->checkRight($request);
        if ($robj->getRight==true) {
            return true;
        } else {
            $mtitle = ($this->_container==null ? $this->container : $this->_container)->get('translator')->trans('err.main.message_title', array(), 'KaitekFrameworkBundle');
            $message = ($this->_container==null ? $this->container : $this->_container)->get('translator')->trans('err.main.process_authorize', array(), 'KaitekFrameworkBundle');
            $content = array(
                        'title' => $mtitle,
                        'success' => false,
                        'message' => $message,
                        'file' => ''
            );
            return new JsonResponse($content, 401);
        }
    }

    /**
     * Returns the user have right to update record.
     *
     * @param Request $request Request object
     * @param BaseInterface $record record object
     * @param User $user User object
     *
     * @return boolean|JsonResponse
     */
    public function checkRightUpdate(Request $request, $record, $user)
    {
        $robj=$this->checkRight($request);
        $err=false;
        if ($robj->getRight==true) {
            if ($robj->updRight==true) {
                return true;
            } else {
                if ($robj->uOwnRight==true) {
                    if ($user->getId() == $record->getCuserId()) {
                        return true;
                    } else {
                        $err=true;
                    }
                } else {
                    $err=true;
                }
            }
        } else {
            $err=true;
        }
        if ($err==true) {
            $mtitle = ($this->_container==null ? $this->container : $this->_container)->get('translator')->trans('err.main.message_title', array(), 'KaitekFrameworkBundle');
            $message = ($this->_container==null ? $this->container : $this->_container)->get('translator')->trans('err.main.process_authorize', array(), 'KaitekFrameworkBundle');
            $content = array(
                        'title' => $mtitle,
                        'success' => false,
                        'message' => $message,
                        'file' => ''
            );
            return new JsonResponse($content, 401);
        }
    }

    public function clearLookup($data)
    {
        /*formdan gelecek olan combo değerine göre gerekmesi halinde düzenlenecek*/
        if ($data) {
            foreach ($data as $cKey=>$cVal) {
                /*if(substr($cKey, 0, 3) == 'fi_'){
                    $nKey = substr($cKey, 3);
                    array_push($this->fileInputs, $nKey);
                    $data->$nKey = $cVal;
                    unset($data->$cKey);
                }*/
                if (strpos($cKey, "_lookup_")) {
                    unset($data->$cKey);
                }
            }
        }
        return $data;
    }

    /**
     * Check the parameters.
     *
     * @param Request $request Request object
     *
     * @return boolean|Response|redirect
     */
    public function controlParams(Request $request)
    {
        $rf=true;

        $this->isDAVRequest = $request->attributes->get('_isDAV') === true;

        $bf=$this->beforeFilter($request);
        if ($bf!==true) {
            if ($request->isXmlHttpRequest()!=true) {
                return $this->redirectToRoute('fos_user_security_logout');
            } else {
                if ($bf===false) {
                    return $this->msgError(($this->_container==null ? $this->container : $this->_container)->get('translator')->trans('err.request.beforefilter', array(), 'KaitekFrameworkBundle'), 401);
                } else {
                    return $bf;
                }
            }
        }

        if (!isset($this->_requestData)) {
            $this->_requestData = json_decode($request->getContent());
        }

        if ($this->isDAVRequest) {
            return $rf;
        }
        if ($request->isXmlHttpRequest()!=true) {//X-Requested-With: XMLHttpRequest
            return new Response(($this->_container==null ? $this->container : $this->_container)->get('translator')->trans('err.request.isajaxfalse', array(), 'KaitekFrameworkBundle'), 500);
        }
        return $rf;
    }

    public function delete_files($target)
    {
        try {
            if (is_dir($target)) {
                $files = glob($target . '*', GLOB_MARK); //GLOB_MARK adds a slash to directories returned

                foreach ($files as $file) {
                    $this->delete_files($file);
                }
                if (is_dir($target)) {
                    rmdir($target);
                }
            } elseif (is_file($target)) {
                unlink($target);
            }
        } catch (\Exception $e) {
        }
    }

    public function gen_uuid()
    {
        $uuid = array(
         'time_low'  => 0,
         'time_mid'  => 0,
         'time_hi'  => 0,
         'clock_seq_hi' => 0,
         'clock_seq_low' => 0,
         'node'   => array()
        );

        $uuid['time_low'] = mt_rand(0, 0xffff) + (mt_rand(0, 0xffff) << 16);
        $uuid['time_mid'] = mt_rand(0, 0xffff);
        $uuid['time_hi'] = (4 << 12) | (mt_rand(0, 0x1000));
        $uuid['clock_seq_hi'] = (1 << 7) | (mt_rand(0, 128));
        $uuid['clock_seq_low'] = mt_rand(0, 255);

        for ($i = 0; $i < 6; $i++) {
            $uuid['node'][$i] = mt_rand(0, 255);
        }

        $uuid = sprintf(
            '%08x-%04x-%04x-%02x%02x-%02x%02x%02x%02x%02x%02x',
            $uuid['time_low'],
            $uuid['time_mid'],
            $uuid['time_hi'],
            $uuid['clock_seq_hi'],
            $uuid['clock_seq_low'],
            $uuid['node'][0],
            $uuid['node'][1],
            $uuid['node'][2],
            $uuid['node'][3],
            $uuid['node'][4],
            $uuid['node'][5]
        );

        return $uuid;
    }

    public function getQueryType()
    {
        return $this->_queryType;
    }

    public function getAllRecords($scope, $request, $page=1, $limit=25)
    {
        if ($scope->getQueryType()==self::QUERY_TYPE_SQL) {
            //return $this->getAllRecordsSQL($scope,$request,$page,$limit);
            $records=array();
            $queries=$this->getSqlStr($request);
            foreach ($queries as $key=>$row) {
                if ($row["getAll"]==true) {
                    $records[$key]=$this->getAllRecordsSQL($scope, $request, $row["sql"], $page, $limit);
                }
            }
            return $records;
        }
        if ($scope->getQueryType()==self::QUERY_TYPE_DQL_QB) {
            //return $this->getAllRecordsDQLQB($scope,$request,$page,$limit);
            $records=array();
            $queries=$this->getQBQuery();
            foreach ($queries as $key=>$row) {
                if ($row["getAll"]==true) {
                    $records[$key]=$this->getAllRecordsDQLQB($scope, $request, $row["qb"], $page, $limit);
                }
            }
            //return $this->getRecordByIdDQLQB($scope,$request,$qb,$id);
            return $records;
        }
        return $this->msgError($scope->getQueryType()." Not implemented yet.", 400, 'array');
    }

    public function getRecordByFieldName($scope, $request, $alias, $id)
    {
        if ($scope->getQueryType()==self::QUERY_TYPE_SQL) {
            $records=array();
            $queries=$this->getQBQuery();
            foreach ($queries as $key=>$row) {
                $records[$key]=$this->getRecordByIdSQL($scope, $request, $key, $row["sql"], $id);
            }
            //return $this->getRecordByIdSQL($scope,$request,$alias,$id);
            return $records;
        }
        if ($scope->getQueryType()==self::QUERY_TYPE_DQL_QB) {
            $records=array();
            $queries=$this->getQBQuery();
            foreach ($queries as $key=>$row) {
                $records[$key]=$this->getRecordByIdDQLQB($scope, $request, $row["qb"], $id);
            }
            //return $this->getRecordByIdDQLQB($scope,$request,$qb,$id);
            return $records;
        }
        return $this->msgError($scope->getQueryType()." Not implemented yet.", 400, 'array');
    }

    public function getRecordById($scope, $request, $alias, $id)
    {
        if ($scope->getQueryType()==self::QUERY_TYPE_SQL) {
            $records=array();
            $queries=$scope->getSqlStr($request);
            foreach ($queries as $key=>$row) {
                $records[$key]=$this->getRecordByIdSQL($scope, $request, $key, $row["sql"], $id);
            }
            //return $this->getRecordByIdSQL($scope,$request,$alias,$id);
            return $records;
        }
        if ($scope->getQueryType()==self::QUERY_TYPE_DQL_QB) {
            $records=array();
            $queries=$this->getQBQuery();
            foreach ($queries as $key=>$row) {
                $records[$key]=$this->getRecordByIdDQLQB($scope, $request, $row["qb"], $id);
            }
            //return $this->getRecordByIdDQLQB($scope,$request,$qb,$id);
            return $records;
        }
        return $this->msgError($scope->getQueryType()." Not implemented yet.", 400, 'array');
    }

    public function getRecordByIdDQLQB($scope, $request, $qb, $val)
    {
        $user = $this->getUser();
        $userId = $user->getId();
        //$qb=$this->getQBQuery();
        $rootAlias=$qb->getRootAliases()[0];
        $dqlparts=$qb->getDqlParts();
        $dqlfrom=$dqlparts['from'][0];
        $dqljoins=$dqlparts['join'];
        $entities=array();
        $tmpbundle='';
        $tmpentity='';
        $tmpalias=$dqlfrom->getAlias();
        $dqlfrom=$dqlfrom->getFrom();
        if (strrpos($dqlfrom, ":")!==false) {
            $tmparr=explode(':', $dqlfrom);
            $tmpbundle=$tmparr[0];
            $tmpentity=$tmparr[1];
        } else {
            $tmpentity=$dqlfrom;
        }
        $entities[]=array('bundle'=>$tmpbundle,'entity'=>$tmpentity,'alias'=>$tmpalias);
        foreach ($dqljoins as $dqljoin) {
            foreach ($dqljoin as $itemjoin) {
                $tmpbundle='';
                $tmpentity='';
                $tmpalias=$itemjoin->getAlias();
                $itemjoin=$itemjoin->getJoin();
                if (strrpos($itemjoin, ":")!==false) {
                    $tmparr=explode(':', $itemjoin);
                    $tmpbundle=$tmparr[0];
                    $tmpentity=$tmparr[1];
                } else {
                    $tmpentity=$itemjoin;
                }
                $entities[]=array('bundle'=>$tmpbundle,'entity'=>$tmpentity,'alias'=>$tmpalias);
            }
        }
        $ccr=false;
        if ("array" === getType($entities)&&count($entities)>0) {
            foreach ($entities as $item) {
                $cr=$this->checkCustomRight($userId, $item['entity'], $item['alias']);
                if ($ccr===false) {
                    $ccr=$cr;
                } elseif ("array" === getType($cr)) {
                    $ccr['where']=array_merge($ccr['where'], $cr['where']);
                    $ccr['params']=array_merge($ccr['params'], $cr['params']);
                }
            }
        }
        $filters=$this->parseFilter($request);
        $arr=$this->prepareParams($ccr, $filters);
        if ("array" === getType($arr['ccr'])&&count($arr['ccr'])>0) {
            $tmpwhere=" ".implode(' or ', $arr['ccr'])." ";
            $qb->andWhere($tmpwhere);
        }
        if ("array" === getType($arr['filter'])&&count($arr['filter'])>0) {
            $tmpwhere=" ".implode(' and ', $arr['filter'])." ";
            $qb->andWhere($tmpwhere);
        }
        if (getType($val)=="array") {
            foreach ($val as $key=>$item) {
                $_key=str_replace('.', '_', key($item));
                $qb->andWhere(key($item)."=:".$_key);
            }
        } else {
            $qb->andWhere($rootAlias.".id=:id");
        }
        $qb=$qb->getQuery();
        if ("array" === getType($arr['params'])&&count($arr["params"])>0) {
            foreach ($arr["params"] as $param) {
                $qb->setParameter($param->name, $param->value);
            }
        }
        if (getType($val)=="array") {
            foreach ($val as $key=>$item) {
                $_key=str_replace('.', '_', key($item));
                $qb->setParameter($_key, $item[key($item)]);
            }
        } else {
            $qb=$qb->setParameter("id", $val);
        }
        $records=$qb->getArrayResult();
        return array("totalProperty"=>count($records),"records"=>$records);
    }

    public function getRecordByIdSQL($scope, $request, $alias, $sql, $id)
    {
        $user = $this->getUser();
        $userId = $user->getId();
        //$sql=$scope->getSqlStr($request);
        $ccr=false;
        $tables=$this->sql_query_get_tables($sql);
        if ("array" === getType($tables)&&count($tables)>0) {
            foreach ($tables as $table) {
                $cr=$this->checkCustomRight($userId, $table);
                if ($ccr===false) {
                    $ccr=$cr;
                } elseif ("array" === getType($cr)) {
                    $ccr['where']=array_merge($ccr['where'], $cr['where']);
                    $ccr['params']=array_merge($ccr['params'], $cr['params']);
                }
            }
        }
        $filters=$this->parseFilter($request);
        $arr=$this->prepareParams($ccr, $filters);
        $tmpwhere=" and ".$alias.".id = :id ";
        if ("array" === getType($arr['ccr'])&&count($arr['ccr'])>0) {
            $tmpwhere.=" and (".implode(' or ', $arr['ccr']).") ";
        }
        if ("array" === getType($arr['filter'])&&count($arr['filter'])>0) {
            $tmpwhere.=" and (".implode(' and ', $arr['filter']).") ";
        }
        $sql=str_replace('@@where@@', $tmpwhere, $sql);
        $em = $this->getDoctrine()->getManager();
        $conn = $em->getConnection();
        $stmt = $conn->prepare($sql);
        $stmt->bindValue('id', $id);
        if ("array" === getType($arr['params'])&&count($arr["params"])>0) {
            foreach ($arr["params"] as $param) {
                if (is_int($param->value)) {
                    $typeparam = \PDO::PARAM_INT;
                } elseif (is_bool($param->value)) {
                    $typeparam = \PDO::PARAM_BOOL;
                } elseif (is_null($param->value)) {
                    $typeparam = \PDO::PARAM_NULL;
                } elseif (is_string($param->value)) {
                    $typeparam = \PDO::PARAM_STR;
                } else {
                    $typeparam = false;
                }
                if ($typeparam) {
                    $stmt->bindValue($param->name, $param->value, $typeparam);
                } else {
                    $stmt->bindValue($param->name, $param->value);
                }
            }
        }
        $stmt->execute();
        $records = $stmt->fetchAll();
        return array("totalProperty"=>count($records),"records"=>$records);
    }

    private function getAllRecordsDQLQB($scope, $request, $qb, $page=1, $limit=25)
    {
        $user = $this->getUser();
        $userId = $user->getId();
        //$queries=$this->getQBQuery();
        //$f=false;
        //$retkey='';
        //$qb=false;
        //foreach($queries as $key=>$row){
        //    if($f==false){
        //        $f=true;
        //        $retkey=$key;
        //        $qb=$row;
        //    }
        //}
        $rootAlias=$qb->getRootAliases()[0];
        $dqlparts=$qb->getDqlParts();
        $dqlfrom=$dqlparts['from'][0];
        $dqljoins=$dqlparts['join'];
        $entities=array();
        $tmpbundle='';
        $tmpentity='';
        $tmpalias=$dqlfrom->getAlias();
        $dqlfrom=$dqlfrom->getFrom();
        if (strrpos($dqlfrom, ":")!==false) {
            $tmparr=explode(':', $dqlfrom);
            $tmpbundle=$tmparr[0];
            $tmpentity=$tmparr[1];
        } else {
            $tmpentity=$dqlfrom;
        }
        $entities[]=array('bundle'=>$tmpbundle,'entity'=>$tmpentity,'alias'=>$tmpalias);
        foreach ($dqljoins as $dqljoin) {
            foreach ($dqljoin as $itemjoin) {
                $tmpbundle='';
                $tmpentity='';
                $tmpalias=$itemjoin->getAlias();
                $itemjoin=$itemjoin->getJoin();
                if (strrpos($itemjoin, ":")!==false) {
                    $tmparr=explode(':', $itemjoin);
                    $tmpbundle=$tmparr[0];
                    $tmpentity=$tmparr[1];
                } else {
                    $tmpentity=$itemjoin;
                }
                $entities[]=array('bundle'=>$tmpbundle,'entity'=>$tmpentity,'alias'=>$tmpalias);
            }
        }
        $ccr=false;
        if ("array" === getType($entities)&&count($entities)>0) {
            foreach ($entities as $item) {
                $cr=$this->checkCustomRight($userId, $item['entity'], $item['alias']);
                if ($ccr===false) {
                    $ccr=$cr;
                } elseif ("array" === getType($cr)) {
                    $ccr['where']=array_merge($ccr['where'], $cr['where']);
                    $ccr['params']=array_merge($ccr['params'], $cr['params']);
                }
            }
        }
        $filters=$this->parseFilter($request);
        $offset = ($page-1)*$limit;
        $arr=$this->prepareParams($ccr, $filters);
        if ("array" === getType($arr['ccr'])&&count($arr['ccr'])>0) {
            $tmpwhere=" ".implode(' or ', $arr['ccr'])." ";
            $qb->andWhere($tmpwhere);
        }
        if ("array" === getType($arr['filter'])&&count($arr['filter'])>0) {
            $tmpwhere=" ".implode(' and ', $arr['filter'])." ";
            $qb->andWhere($tmpwhere);
        }
        /** @var QueryBuilder $qb */
        $qb=$qb->getQuery();

        $params = $qb->getParameters();
        for ($i=0; $i<$params->count(); $i++) {
            $param = $params->get($i);
            $p = new \stdClass();
            $p->name = $param->getName();
            $p->value = $param->getValue();
            $arr["params"][] = $p;
        }
        $count = $this->getCountDql($qb->getDql(), $arr, $rootAlias);
        if ("array" === getType($arr['params'])&&count($arr["params"])>0) {
            foreach ($arr["params"] as $param) {
                $qb->setParameter($param->name, $param->value);
            }
        }
        $qb=$qb->setFirstResult($offset)
            ->setMaxResults($limit);
        $records=$qb->getArrayResult();
        //if($retkey==''){
        return array("totalProperty"=>$count,"records"=>$records);
        //}else{
        //    return array($retkey=>array("totalProperty"=>$count,"records"=>$records));
        //}
    }

    public function getAllRecordsSQL($scope, $request, $sql, $page=1, $limit=25)
    {
        $user = $this->getUser();
        $userId = $user->getId();
        //$sql=$scope->getSqlStr($request);
        $ccr=false;
        $tables=$this->sql_query_get_tables($sql);
        if ("array" === getType($tables)&&count($tables)>0) {
            foreach ($tables as $table) {
                $cr=$this->checkCustomRight($userId, $table);
                if ($ccr===false) {
                    $ccr=$cr;
                } elseif ("array" === getType($cr)) {
                    $ccr['where']=array_merge($ccr['where'], $cr['where']);
                    $ccr['params']=array_merge($ccr['params'], $cr['params']);
                }
            }
        }
        $filters=$this->parseFilter($request);
        $offset = ($page-1)*$limit;
        $arr=$this->prepareParams($ccr, $filters);
        //dump($arr);
        //dump($sql);
        if ($limit==0&&$filters==false) {
            return array("totalProperty"=>0,"records"=>[]);
        } elseif ($limit==0&&$filters!==false) {
            $limit=($this->_container==null ? $this->container : $this->_container)->getParameter('kaitek_framework.recordsperpage');
        } elseif ($limit==-1) {
            $limit=($this->_container==null ? $this->container : $this->_container)->getParameter('kaitek_framework.recordsperpage');
        }
        $tmpwhere="";
        if ("array" === getType($arr['ccr'])&&count($arr['ccr'])>0) {
            $tmpwhere.=" and (".implode(' or ', $arr['ccr']).") ";
        }
        if ("array" === getType($arr['filter'])&&count($arr['filter'])>0) {
            $tmpwhere.=" and (".implode(' and ', $arr['filter']).") ";
        }
        $sql=str_replace('@@where@@', $tmpwhere, $sql);
        //dump($sql);
        $lsql=$sql." OFFSET :ofs LIMIT :lim";
        $em = $this->getDoctrine()->getManager();
        //dump($lsql);
        $conn = $em->getConnection();
        $stmt = $conn->prepare($lsql);
        if ("array" === getType($arr['params'])&&count($arr["params"])>0) {
            foreach ($arr["params"] as $param) {
                if (is_int($param->value)) {
                    $typeparam = \PDO::PARAM_INT;
                } elseif (is_bool($param->value)) {
                    $typeparam = \PDO::PARAM_BOOL;
                } elseif (is_null($param->value)) {
                    $typeparam = \PDO::PARAM_NULL;
                } elseif (is_string($param->value)) {
                    $typeparam = \PDO::PARAM_STR;
                } else {
                    $typeparam = false;
                }
                if ($typeparam) {
                    $stmt->bindValue($param->name, $param->value, $typeparam);
                } else {
                    $stmt->bindValue($param->name, $param->value);
                }
            }
        }
        $stmt->bindValue("ofs", $offset);
        $stmt->bindValue("lim", $limit);
        $stmt->execute();
        $records = $stmt->fetchAll();
        $count=$this->getCountSql($sql, $arr);
        //if($count>0&&count($records)==0){
        //    echo($lsql);
        //}
        return array("totalProperty"=>$count,"records"=>$records);
    }

    protected function getBackendData(Request $request, $_locale, $entity, $lim=-1)
    {
        //$_rpp = $lim==-1?$this->getParameter('kaitek_framework.recordsperpage'):$lim;
        $_rpp=25;
        if ($lim==-1) {
            $_rpp=($this->_container==null ? $this->container : $this->_container)->getParameter('kaitek_framework.recordsperpage');
        } else {
            $_rpp = $lim;
        }
        $records = $this->getAllRecords($this, $request, 1, $_rpp);
        /**
         * BaseController'dan türeyen sınıflarda, burada öngörülmeyen değerleri göndermek için
         * data['extras'] kullanılabilir. module.html.twig üzerinden baseObj içine aktarılır.
         */
        $data = array(
            'modulename' => $request->request->get('modulename'),
            'audit' => 0, //$this instanceof BaseAuditControllerInterface,
            'data' => $records,
            'extras' => array()
        );

        //try {
        //    $calendarHelper = $this->container->get('calendar');
        //    if($calendarHelper) {
        //        $calendarHelper->prepareCalendarModuleParameters($request, $_locale, $entity, $data);
        //    }
        //} catch (\Exception $e) {
        //
        //}
        //
        //try {
        //    $documentationHelper = $this->container->get('documentation');
        //    if($documentationHelper) {
        //        $documentationHelper->prepareDocumentationModuleParameters($request, $_locale, $entity, $data);
        //    }
        //} catch (\Exception $e) {
        //
        //}

        return $data;
    }

    protected function getBackendDataById(Request $request, $_locale, $entity, $alias, $id)
    {
        //$_rpp = $this->getParameter('kaitek_framework.recordsperpage');
        $records = $this->getRecordById($this, $request, $alias, $id);
        $data = array(
            'modulename' => $request->request->get('modulename'),
            'audit' => 0, //$this instanceof BaseAuditControllerInterface,
            'data' => $records,
            'extras' => array()
        );

        //try {
        //    $calendarHelper = $this->container->get('calendar');
        //    if($calendarHelper) {
        //        $calendarHelper->prepareCalendarModuleParameters($request, $_locale, $entity, $data);
        //    }
        //} catch (\Exception $e) {
        //
        //}
        //
        //try {
        //    $documentationHelper = $this->container->get('documentation');
        //    if($documentationHelper) {
        //        $documentationHelper->prepareDocumentationModuleParameters($request, $_locale, $entity, $data);
        //    }
        //} catch (\Exception $e) {
        //
        //}

        return $data;
    }

    /**
     * @Route(path="/BaseController/{pg}/{lm}/{table}/{fieldId}/{fieldDisplay}/{val}", requirements={"pg": "\d+","lm": "\d+"}, name="BaseController-getComboValues", options={"expose"=true}, methods={"GET"})
     */
    public function getComboValues(Request $request, $_locale, $pg, $lm, $table, $fieldId='code', $fieldDisplay='code', $val='', $where='')
    {
        $sql = "SELECT $fieldId as id,$fieldDisplay as code "
        . " FROM $table "
        . " WHERE 1=1 $where @@where@@ "
        . " @@group@@  "
        . " ORDER BY $fieldDisplay "
        . " OFFSET :ofs LIMIT :lim";
        $params=[];
        $tmpwhere="";
        //combo typeahead türkçe karakter sorun olduğundan ut8decode kaldırıldı
        $val=(urldecode($val));
        if ($val!=='') {
            $val=implode(".", explode("|_|", $val));
            $arr_concat=explode('concat', $fieldDisplay);
            if (count($arr_concat)>1) {
                $tmpwhere.=" and ( ";
                $str_concat=str_replace(",,", ",", str_replace(array('(',')',"'-'",'"-"'), "", $arr_concat[1]));
                $arr_concat_new=explode(',', $str_concat);
                $counter=0;
                foreach ($arr_concat_new as $item) {
                    $tmpwhere.=($counter===0 ? '' : ' or ')." $table.$item like :".$item.$counter." ";
                    $params[]=json_decode('{"name":"'.$item.$counter.'","value":"%'.$val.'%"}');
                    $counter++;
                }
                $tmpwhere.=" ) ";
                $sql=str_replace('@@group@@', "GROUP BY ".$str_concat, $sql);
            } else {
                $tmpwhere.=" and $table.$fieldDisplay like :".$fieldDisplay." ";
                $params[]=json_decode('{"name":"'.$fieldDisplay.'","value":"%'.$val.'%"}');
            }
        }
        $sql=str_replace('@@group@@', "", $sql);
        $params[]=json_decode('{"name":"ofs","value":"'.($pg-1)*$lm.'"}');
        $params[]=json_decode('{"name":"lim","value":"'.$lm.'"}');
        $sql=str_replace('@@where@@', $tmpwhere, $sql);
        $em = $this->getDoctrine()->getManager();
        $conn = $em->getConnection();
        $stmt = $conn->prepare($sql);
        try {
            foreach ($params as $param) {
                if (is_int($param->value)) {
                    $typeparam = \PDO::PARAM_INT;
                } elseif (is_bool($param->value)) {
                    $typeparam = \PDO::PARAM_BOOL;
                } elseif (is_null($param->value)) {
                    $typeparam = \PDO::PARAM_NULL;
                } elseif (is_string($param->value)) {
                    $typeparam = \PDO::PARAM_STR;
                } else {
                    $typeparam = false;
                }
                if ($typeparam) {
                    $stmt->bindValue($param->name, $param->value, $typeparam);
                } else {
                    $stmt->bindValue($param->name, $param->value);
                }
            }
            $stmt->execute();
            $records = $stmt->fetchAll();
            $count=$this->getCountSql($sql, array('params'=>array_slice($params, 0, count($params)-2)));
        } catch (\Exception $e) {
            echo($e);
        }
        return new JsonResponse(array("totalProperty"=>$count,"records"=>$records));
    }

    public function getCountDql($dql, $arr, $alias)
    {
        $count=0;
        $dqlarr=explode(" FROM ", $dql);
        $dqlarrselect=explode("SELECT ", $dqlarr[0]);
        $dqlarrselect[1]="count(".$alias.".id)";
        $dql="SELECT ".$dqlarrselect[1]." FROM ".$dqlarr[1];
        $dqlarr=explode(" ORDER ", $dql);
        $dql=$dqlarr[0];
        $qc = $this->getDoctrine()->getManager()
            ->createQuery($dql);
        if (/*isset($arr["params"]) &&*/ $arr["params"]!==false) {
            foreach ($arr["params"] as $param) {
                $qc->setParameter($param->name, $param->value);
            }
        }
        $count=$qc->getSingleScalarResult();
        return $count;
    }

    private function getCountSql($sql, $arr)
    {
        $count=0;
        $sqlarr=explode(" FROM ", $sql);
        $sqlarrselect=explode("SELECT ", $sqlarr[0]);
        $sqlarrselect[1]="COUNT(*) count";
        $sql="SELECT ".$sqlarrselect[1]." FROM ".$sqlarr[1];
        $sqlarr=explode(" ORDER ", $sql);
        $sql=$sqlarr[0];
        $em = $this->getDoctrine()->getManager();
        $conn = $em->getConnection();
        $stmt = $conn->prepare($sql);
        if (/*isset($arr["params"]) &&*/ $arr["params"]!==false) {
            foreach ($arr["params"] as $param) {
                if (is_int($param->value)) {
                    $typeparam = \PDO::PARAM_INT;
                } elseif (is_bool($param->value)) {
                    $typeparam = \PDO::PARAM_BOOL;
                } elseif (is_null($param->value)) {
                    $typeparam = \PDO::PARAM_NULL;
                } elseif (is_string($param->value)) {
                    $typeparam = \PDO::PARAM_STR;
                } else {
                    $typeparam = false;
                }
                if ($typeparam) {
                    $stmt->bindValue($param->name, $param->value, $typeparam);
                } else {
                    $stmt->bindValue($param->name, $param->value);
                }
            }
        }
        $stmt->execute();
        $arr = $stmt->fetchAll();
        if (count($arr)>1) {
            $count=count($arr);
        } else {
            if (count($arr)===1) {
                $count = $arr[0]["count"];
            }
        }
        //$count = $stmt->fetchColumn(0);
        return $count;
    }

    public function getDataUsers()
    {
        //$_sql="SELECT id,username,fullname,avatar FROM _user WHERE expired='f' and locked='f' and roles not like '%SUPER_ADMIN%'";
        $_sql="SELECT id,username,fullname,avatar FROM _user WHERE roles not like '%SUPER_ADMIN%'";
        $em = $this->getDoctrine()->getManager();
        $conn = $em->getConnection();
        $stmt = $conn->prepare($_sql);
        $stmt->execute();
        $records = $stmt->fetchAll();
        $arr=array('params'=>false);
        $count=$this->getCountSql($_sql, $arr);
        return array("totalProperty"=>$count,"records"=>$records);
    }

    public function getEntityAudit($entityName, $id, $rev)
    {
        $obj=$this->getEntityAuditSQL($entityName);
        $em = $this->getDoctrine()->getManager();
        $conn = $em->getConnection();
        $arr=array();
        foreach ($obj as $row) {
            $stmt = $conn->prepare($row["query"]);
            $stmt->bindValue("id", $id);
            $stmt->bindValue("rev", $rev);
            $stmt->execute();
            $records = $stmt->fetchAll();
            $arr[]=array("entity"=>$row["entity"],"records"=>$records);
        }
        return $arr;
    }

    private function getEntityAuditDiff($oldData, $newData)
    {
        $diff = array();

        $keys = array_keys($oldData + $newData);
        foreach ($keys as $field) {
            $old = array_key_exists($field, $oldData) ? $oldData[$field] : null;
            $new = array_key_exists($field, $newData) ? $newData[$field] : null;

            if ($old == $new) {
                $row = array('old' => '', 'new' => '', 'same' => $old);
            } else {
                $row = array('old' => $old, 'new' => $new, 'same' => '');
            }

            $diff[$field] = $row;
        }

        return $diff;
    }

    public function getEntityAuditDifference($entityName, $id, $oldRev, $newRev)
    {
        $oldValues = $this->getEntityAudit($entityName, $id, $oldRev);
        $newValues = $this->getEntityAudit($entityName, $id, $newRev);
        $arr=array();
        foreach ($oldValues as $row) {
            $key = array_search($row["entity"], array_column($newValues, 'entity'));
            $arr[]=$this->getEntityAuditDiff($row["records"], $newValues[$key]["records"]);
        }
        return $arr;
    }

    private function getEntityAuditSQL($entityName)
    {
        $arr=array("master"=>array(),"detail"=>array());
        $em = $this->getDoctrine()->getManager();
        $ac = new AuditConfiguration();
        $cm=$em->getClassMetadata($entityName);
        $tnmaster=$ac->getTableName($cm);
        $arrsql=array("select"=>array(),"from"=>$tnmaster,"join"=>array(),"order"=>array());
        foreach ($cm->fieldNames as $key=>$val) {
            $arrsql["select"][]=$tnmaster.'.'.$key." ".str_replace('_audit', '', $tnmaster).'_'.$key."";
        }
        $arrsql["order"][]=$tnmaster.'.rev desc';
        foreach ($cm->associationMappings as $asmap) {
            if ($asmap['mappedBy']!==null) {
                //detay tablo kayıtlar, ayrı dataset içinde gösterilecek
                $cm1=$em->getClassMetadata($asmap['targetEntity']);
                $tnmaster1=$ac->getTableName($cm1);
                $arrsql1=array("select"=>array(),"from"=>$tnmaster1,"join"=>array(),"order"=>array());
                foreach ($cm1->fieldNames as $key=>$val) {
                    $arrsql1["select"][]=$tnmaster1.'.'.$key." ".str_replace('_audit', '', $tnmaster1).'_'.$key."" ;
                }
                $arrsql1["order"][]=$tnmaster1.'.rev desc';
                foreach ($cm1->associationMappings as $asmap1) {
                    if ($asmap1['mappedBy']===null&&$asmap['mappedBy']!=$asmap1['fieldName']) {
                        $cd=$em->getClassMetadata($asmap1['targetEntity']);
                        $tn=$ac->getTableName($cd);
                        foreach ($cd->fieldNames as $key=>$val) {
                            if (strpos($key, 'name')!==false/*||$key=="id"*/) {
                                $arrsql1["select"][]=$tn.'.'.$key." ".str_replace('_audit', '', $tn).'_'.$key."";
                            }
                        }
                        $str="";
                        foreach ($asmap1['joinColumns'] as $row=>$item) {
                            $str.=" and ".$tn.".".$item['referencedColumnName']."=".$tnmaster1.".".$item['name'];
                            $arrsql1["select"][]=$tnmaster1.'.'.$item['name']." ".str_replace('_audit', '', $tnmaster1).'_'.$item['name']."";
                        }
                        $arrsql1["join"][]="left join $tn on $tn.rev<=$tnmaster1.rev ".$str;
                        $arrsql1["order"][]=$tn.'.rev desc';
                    }
                }
                $sql=" select ". implode(',', $arrsql1['select'])." "
                    . " from ".$arrsql1['from']." ".implode(' ', $arrsql1['join'])." where $tnmaster1.id=:id and $tnmaster1.rev=:rev "
                    . " order by ".implode(',', $arrsql1['order'])." "
                    . " offset 0 limit 1 ";
                $arr["detail"][]=array("entity"=>$arrsql1['from'],"query"=>$sql);
            } else {
                //join tablo kayıtları, join sorgu oluşturulacak
                $cd=$em->getClassMetadata($asmap['targetEntity']);
                $tn=$ac->getTableName($cd);
                foreach ($cd->fieldNames as $key=>$val) {
                    if (strpos($key, 'name')!==false/*||$key=="id"*/) {
                        $arrsql["select"][]=$tn.'.'.$key." ".str_replace('_audit', '', $tn).'_'.$key."";
                    }
                }
                $str="";
                foreach ($asmap['joinColumns'] as $row=>$item) {
                    $str.=" and ".$tn.".".$item['referencedColumnName']."=".$tnmaster.".".$item['name'];
                    $arrsql["select"][]=$tnmaster.'.'.$item['name']." ".str_replace('_audit', '', $tnmaster).'_'.$item['name']."";
                }
                $arrsql["join"][]="left join $tn on $tn.rev<=$tnmaster.rev ".$str;
                $arrsql["order"][]=$tn.'.rev desc';
            }
        }
        $sql=" select ". implode(',', $arrsql['select'])." "
                . " from ".$arrsql['from']." ".implode(' ', $arrsql['join'])." where $tnmaster.id=:id and $tnmaster.rev=:rev "
                . " order by ".implode(',', $arrsql['order'])." "
                . " offset 0 limit 1 ";
        $arr["master"][]=array("entity"=>$arrsql['from'],"query"=>$sql);
        return array_merge($arr["master"], $arr["detail"]);
    }

    public function getValidateMessage($errors)
    {
        if (count($errors) > 0) {
            $errorsString="";
            foreach ($errors as $error) {
                $em = $this->getDoctrine()->getManager();
                $cd=$em->getClassMetadata(get_class($error->getRoot()));

                $reflectionProperty = new \ReflectionProperty($cd->name, $error->getPropertyPath());
                /** @var FieldData $propertyAnnotation */
                $propertyAnnotation = $this->annotationReader->getPropertyAnnotation($reflectionProperty, 'Kaitek\Bundle\FrameworkBundle\ORM\Mapping\FieldData');
                /** @var Column $propertyAnnotationORM */
                $propertyAnnotationORM = $this->annotationReader->getPropertyAnnotation($reflectionProperty, 'Doctrine\ORM\Mapping\Column');

                /** @var Translator $translator */
                $translator = ($this->_container==null ? $this->container : $this->_container)->get('translator');
                if (true) {
                    if ($propertyAnnotation && isset($propertyAnnotation->label)) {
                        $fieldLabelTranslationId = explode('.', $propertyAnnotation->label);
                    } else {
                        $fieldLabelTranslationId = array($cd->name, $error->getPropertyPath());
                    }
                    if (count($fieldLabelTranslationId) > 1) {
                        $p1 = is_object($propertyAnnotation) ? $propertyAnnotation : new \stdClass();
                        if (!isset($propertyAnnotation->label)) {
                            $name = explode('\\', $cd->getName());
                            $name = $name[count($name) - 1];
                            $p1->label = $name . '.' . $error->getPropertyPath();
                        }
                        $p2 = is_object($propertyAnnotationORM) ? $propertyAnnotationORM : new \stdClass();
                        $params = array_merge(get_object_vars($p1), get_object_vars($p2));
                        $formattedParams = array();
                        foreach ($params as $key => $param) {
                            if (is_array($param)) {
                                /**
                                 * TODO: Mevcut kod, parametreleri formattedParams altına alırken 2 alt kademeye kadar destekliyor. Daha fazlasına inmek gerekirse bu kısım recursive bir fonksiyona dönüştürülmeli.
                                 */
                                foreach ($param as $subKey => $value) {
                                    $formattedParams['{{ ' . $key . '.' . $subKey . ' }}'] = $translator->trans($value);
                                }
                            } else {
                                $formattedParams['{{ ' . $key . ' }}'] = $translator->trans($param, [], substr($param, 0, strpos($param, '.')));
                            }
                        }
                        $errorsString .= $formattedParams['{{ label }}'] ."-".$translator->trans($error->getMessage(), $formattedParams, substr($param, 0, strpos($param, '.')));
                    } else {
                        $errorsString .= "(" . $message . ") " . $error->getMessage() . "<br>";
                    }
                } else {
                    $errorsString .= "(" . $error->getPropertyPath() . ") " . $error->getMessage() . "<br>";
                }
            }
            return $this->msgError($errorsString);
        }
        return false;
    }

    /**
     * Returns request params.
     *
     * @param string $paramName
     *
     * @return mixed
     */
    public function getVar($paramName="")
    {
        return $this->getVarIfEmpty($paramName, null);
    }

    /**
     * Returns request params.
     *
     * @param string $paramName
     * @param mixed $defaultValue
     *
     * @return mixed
     */
    public function getVarIfEmpty($paramName="", $defaultValue=null)
    {
        $val = null;
        if (is_array($this->_requestData)) {
            return isset($this->_requestData[$paramName]) && $this->_requestData[$paramName] !== "" ? $this->_requestData[$paramName] : $defaultValue;
        }
        return isset($this->_requestData->$paramName) && $this->_requestData->$paramName !== "" ? $this->_requestData->$paramName : $defaultValue;
    }

    /**
     * Get a user from the Security Token Storage.
     *
     * @return mixed
     *
     * @throws \LogicException If SecurityBundle is not available
     *
     * @see TokenInterface::getUser()
     */
    public function getUser(): ?UserInterface
    {
        if ($this->isDAVRequest) {
            //TODO: DAV USER BİLGİSİNİ SYMFONY USER BİLGİSİNE DÖNÜŞTÜR.
        }
        return parent::getUser();
    }

    public function is_true($val, $return_null=false)
    {
        $val=is_string($val) ? ((trim($val)=='T'||trim($val)=='True'||trim($val)=='TRUE'||trim($val)=='true'||trim($val)=='t') ? true : false) : $val;
        $boolval = (is_string($val) ? filter_var(trim($val), FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE) : (bool) $val);
        return ($boolval===null && !$return_null ? false : $boolval);
    }

    /**
     * Returns the error message.
     *
     * @param string $message
     *
     * @return JsonResponse
     */
    public function msgError($message="", $errCode=400, $rt='json')
    {
        $mtitle = ($this->_container==null ? $this->container : $this->_container)->get('translator')->trans('err.main.message_title', array(), 'KaitekFrameworkBundle');
        $message = $message=="" ? ($this->_container==null ? $this->container : $this->_container)->get('translator')->trans('err.main.error_message', array(), 'KaitekFrameworkBundle') : $message;
        $content = array(
                    'title' => $mtitle,
                    'success' => false,
                    'message' => $message,
                    'file' => ''
        );
        if ($rt=='json') {
            return new JsonResponse($content, $errCode);
        } else {
            return $content;
        }
    }

    /**
     * Returns the success message.
     *
     * @param string $message
     *
     * @return JsonResponse
     */
    public function msgSuccess($message="")
    {
        $mtitle = ($this->_container==null ? $this->container : $this->_container)->get('translator')->trans('err.main.success_message_title', array(), 'KaitekFrameworkBundle');
        $message = $message=="" ? ($this->_container==null ? $this->container : $this->_container)->get('translator')->trans('err.main.success_message', array(), 'KaitekFrameworkBundle') : $message;
        $content = array(
                    'title' => $mtitle,
                    'success' => true,
                    'message' => $message,
                    'file' => ''
        );
        return new JsonResponse($content, 200);
    }

    /**
     * Parse filter.
     *
     * @param Request $request Request object
     *
     * @return array
     */
    public function parseFilter(Request $request)
    {
        /*
         * operator variable can be one of the following
         * =
         * >
         * <
         * >=
         * <=
         * .%
         * %.
         * %.%
         * (.)
         * between
         */
        $rf=false;
        $filters=false;
        try {
            $paramsquery = $request->query->all();
            if (count($paramsquery)>0) {
                if (isset($paramsquery["filters"])) {
                    if (gettype($paramsquery["filters"])==='array') {
                        $filter=$paramsquery["filters"];
                    } else {
                        $filter=json_decode($paramsquery["filters"]);
                    }
                } else {
                    $filter=false;
                }
                if ($filter!==false) {
                    $filters=array("where"=>array(),"params"=>array());
                    $i=100;
                    foreach ($filter as $filterItem) {
                        if (gettype($paramsquery["filters"])==='array') {
                            $alias=$filterItem['alias'];
                            $fieldname=implode('', explode('_lookup_', $filterItem['fieldname']));
                            $operator=$filterItem['operator'];
                            $value=$filterItem['value'];
                        } else {
                            $alias=$filterItem->alias;
                            $fieldname=$filterItem->fieldname;
                            $operator=$filterItem->operator;
                            $value=$filterItem->value;
                        }

                        $param=new \stdClass();
                        $tmp=" ";
                        switch ($operator) {
                            case ".%":
                            case "%.":
                            case "%.%":
                                $tmp.="lower(".$alias.".".$fieldname.") like lower(:".$fieldname.$i.") ";
                                break;
                            case "(.)":
                                $tmp.="".$alias.".".$fieldname." in (:".$fieldname.$i.") ";
                                break;
                            case "between":
                                $tmp.="".$alias.".".$fieldname." between :".$fieldname."1".$i." and :".$fieldname."2".$i." ";
                                break;
                            default:
                                $tmp.="".$alias.".".$fieldname." ".$operator." :".$fieldname.$i." ";
                                break;
                        }
                        $filters["where"][]=$tmp;
                        switch ($operator) {
                            case ".%":
                                $value= $value."%";
                                break;
                            case "%.":
                                $value= "%".$value;
                                break;
                            case "%.%":
                                $value="%".$value."%";
                                break;
                        }
                        if ($operator!="between") {
                            $param->name=$fieldname.$i;
                            $_val=($value==='Hayır' ? false : ($value==='Evet' ? true : $value));
                            $param->value=$_val;
                        } else {
                            $values=  explode("|", $value);
                            $param->name=$fieldname."1".$i;
                            $param->value=$values[0];
                            $filters["params"][]=$param;
                            $param=new \stdClass();
                            $param->name=$fieldname."2".$i;
                            $param->value=$values[1];
                        }
                        $filters["params"][]=$param;
                        $i++;
                    }
                    return $filters;
                }
            }
        } catch (Exception $e) {
            throw $e;
        }
        return $rf;
    }

    public function prepareParams($ccr, $filters)
    {
        $arr=array("ccr"=>"","filter"=>"","params"=>false);
        if ($ccr!==false) {
            $arr["ccr"]=$ccr["where"];
            if ($arr["params"]==false) {
                $arr["params"]=array();
            }
            $arr['params']=array_merge($arr['params'], $ccr['params']);
        }
        if ($filters!==false) {
            $arr["filter"]=$filters["where"];
            if ($arr["params"]==false) {
                $arr["params"]=array();
            }
            $arr['params']=array_merge($arr['params'], $filters['params']);
        }
        return $arr;
    }

    /**
     * Renders a view.
     *
     * @param string   $view       The view name
     * @param array    $parameters An array of parameters to pass to the view
     * @param Response $response   A response instance
     *
     * @return Response A Response instance
     */
    public function render(string $view, array $parameters = array(), Response $response = null): Response
    {
        $response = parent::render($view, $parameters, $response);
        $content = $response->getContent();

        $response->setContent($content);
        return $response;
    }

    public function recordAdd(Request $request, $_locale, $page=1, $limit=25)
    {
        $cba=$this->checkBeforeAdd($request);
        if ($cba===true) {
            $entity = $this->getNewEntity();
            $this->setEntityWithRequest($entity, $request);

            $validator = ($this->_container==null ? $this->container : $this->_container)->get('validator');
            $errors = $this->getValidateMessage($validator->validate($entity));
            if ($errors!==false) {
                return $errors;
            }

            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();
            $conn->beginTransaction();
            try {
                $em->persist($entity);
                $em->flush();
                $conn->commit();
            } catch (\Exception $e) {
                // Rollback the failed transaction attempt
                $conn->rollback();
                //throw $e;
                return $this->msgError($e->getMessage());
            }
            if (method_exists($this, 'showAllAction') && $request->attributes->get('_isDCSService') !== true) {
                return $this->showAllAction($request, $_locale, $page, $limit);
            } else {
                return $this->msgSuccess();
            }
        } else {
            return $cba;
        }
    }

    public function recordDelete(Request $request, &$entity, $id, $v, $_locale, $page=1, $limit=25)
    {
        $cbd = $this->checkBeforeDelete($request, $id, $entity, $v);
        //$cbd=true;
        if ($cbd === true) {
            $user = $this->getUser();
            $userId = $user->getId();
            $entity->setDeleteuserId($userId);
            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();
            $conn->beginTransaction();
            try {
                $em->persist($entity);
                $em->flush();
                $em->remove($entity);
                $em->flush();
                $conn->commit();
            } catch (\Exception $e) {
                // Rollback the failed transaction attempt
                $conn->rollback();

                // Foreign key violation mesajı mı?
                $msg = $e->getMessage();
                $classMeta = $em->getClassMetadata(get_class($entity));
                $className = $classMeta->getName();
                $tableName = $classMeta->getTableName();
                $customizedMessage = false;
                $foreignKeyViolation = strpos($msg, 'violates foreign key constraint') > -1;
                if ($foreignKeyViolation) {
                    preg_match_all('/".*?"/', $msg, $matchGroups);
                    foreach ($matchGroups as $matchGroup) {
                        if (is_array($matchGroup)) {
                            foreach ($matchGroup as $match) {
                                $match = trim($match, '"');
                                if (substr($match, 0, 3) != 'fk_' && $match != $tableName) {
                                    $referencedTableName = $match;
                                    $referencedEntityClassMeta = $this->getClassMetaFromTableName($em, $referencedTableName);
                                    foreach ($referencedEntityClassMeta->associationMappings as $associationMapping) {
                                        $referencedEntityName = substr($referencedEntityClassMeta->name, strlen($referencedEntityClassMeta->namespace)+1);
                                        $referencedEntityColumnName = $associationMapping['fieldName'];
                                        $referencedEntityTitle = ($this->_container==null ? $this->container : $this->_container)->get('translator')->trans($referencedEntityName.'.title', array(), $referencedEntityName);
                                        if (!$referencedEntityTitle) {
                                            $referencedEntityTitle = $referencedEntityName;
                                        }
                                        $referencedEntityColumnTitle = ($this->_container==null ? $this->container : $this->_container)->get('translator')->trans($referencedEntityName.'.'.$referencedEntityColumnName, array(), $referencedEntityName);
                                        if (!$referencedEntityColumnTitle) {
                                            $referencedEntityColumnTitle = $referencedEntityColumnName;
                                        }
                                        if ($associationMapping['targetEntity'] == $className) {
                                            $customizedMessage = ($this->_container==null ? $this->container : $this->_container)->get('translator')->trans('err.main.foreign_key_constraint_exception_on_delete', array('%entity%'=>$referencedEntityTitle, '%column%'=>$referencedEntityColumnTitle), 'KaitekFrameworkBundle');
                                        }
                                    }
                                    break;
                                }
                            }
                        }
                    }
                }

                if ($customizedMessage) {
                    return $this->msgError($customizedMessage);
                } else {
                    return $this->msgError($e->getMessage());
                }
            }
            if (method_exists($this, 'showAllAction') && $request->attributes->get('_isDCSService') !== true) {
                return $this->showAllAction($request, $_locale, $page, $limit);
            } else {
                return $this->msgSuccess();
            }
        } else {
            return $cbd;
        }
    }

    public function recordEdit(Request $request, &$entity, $id, $v, $_locale, $page=1, $limit=25)
    {
        $cbu=$this->checkBeforeUpdate($request, $id, $entity, $v);
        //$cbu=true;
        if ($cbu===true) {
            $this->setEntityWithRequest($entity, $request);
            $validator = ($this->_container==null ? $this->container : $this->_container)->get('validator');
            $errors = $this->getValidateMessage($validator->validate($entity));
            if ($errors!==false) {
                return $errors;
            }
            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();
            $conn->beginTransaction();
            try {
                $em->persist($entity);
                $em->flush();
                $conn->commit();
            } catch (\Exception $e) {
                // Rollback the failed transaction attempt
                $conn->rollback();
                //throw $e;
                return $this->msgError($e->getMessage());
            }
            if (method_exists($this, 'showAllAction') && $request->attributes->get('_isDCSService') !== true) {
                return $this->showAllAction($request, $_locale, $page, $limit);
            } else {
                return $this->msgSuccess();
            }
        } else {
            return $cbu;
        }
    }

    protected function setEntityWithRequest(&$entity, Request $request)
    {
        if (!isset($this->_requestData)) {
            $content = $request->getContent();
            $this->_requestData = json_decode($request->getContent());
            if ($this->_requestData == null) {
                $this->_requestData = $request->request->all();
            }
        }
        $this->_requestData = $this->clearLookup($this->_requestData);
        if (/*$request->attributes->get('_isDAV') === true||*/$request->attributes->get('_isDCSService') === true) {
            $userId=0;
        } else {
            $user = $this->getUser();
            $userId = $user->getId();
        }
        if ($request->getMethod() === Request::METHOD_POST) {
            $entity->setCreateuserId($userId);
        }

        if (is_callable(array($entity, "setUpdateuserId")) && $request->getMethod() === Request::METHOD_PUT/*||$request->getMethod() === Request::METHOD_POST*/) {
            $entity->setUpdateuserId($userId);
        }

        $entityClass = get_class($entity);
        foreach ($this->_requestData as $key=>$val) {
            $defaultValue = null;
            try {
                $reflectionProperty = new \ReflectionProperty($entityClass, $key);
                $propertyAnnotation = $this->annotationReader->getPropertyAnnotation(
                    $reflectionProperty,
                    'Doctrine\ORM\Mapping\Column'
                );
                if (isset($propertyAnnotation->options['default'])) {
                    $defaultValue = $propertyAnnotation->options['default'];
                }
                $setterFn = "set".strtoupper(substr($key, 0, 1)).substr($key, 1);

                $manyToOne = $this->annotationReader->getPropertyAnnotation(
                    $reflectionProperty,
                    'Doctrine\ORM\Mapping\ManyToOne'
                );
                if (isset($manyToOne) && isset($manyToOne->targetEntity)) {
                    $joinColumn = $this->annotationReader->getPropertyAnnotation(
                        $reflectionProperty,
                        'Doctrine\ORM\Mapping\JoinColumn'
                    );
                    if (isset($joinColumn) && isset($joinColumn->referencedColumnName)) {
                        $foreignRefVal = $this->getVarIfEmpty($key, $defaultValue);
                        $foreignReference = $this->getDoctrine()
                            ->getRepository($manyToOne->targetEntity)
                            ->findBy(array($joinColumn->referencedColumnName => $foreignRefVal));
                        if (is_array($foreignReference) && count($foreignReference) > 0) {
                            $foreignReference = $foreignReference[0];
                            $entity->$setterFn($foreignReference);
                        } else {
                            // kontrol gerekli mi?
                        }
                    }
                } else {
                    if (isset($propertyAnnotation->type) && ($propertyAnnotation->type == 'datetime' || $propertyAnnotation->type == 'date')) {
                        $val = $this->getVarIfEmpty($key, $defaultValue);
                        $dVal = new \DateTime($val);
                        if ($val!==null) {
                            $entity->$setterFn(new \DateTime($this->getVarIfEmpty($key, $defaultValue)));
                        } else {
                            $entity->$setterFn(null);
                        }
                    } else {
                        $entity->$setterFn($this->getVarIfEmpty($key, $defaultValue));
                    }
                }
            } catch (\Exception $e) {
                //echo($e->getMessage());
            }
        }

        return $entity;
    }

    public function sql_query_get_tables($statement)
    {
        preg_match_all("/(from|into|update|join) [\\'\\´]?([a-zA-Z0-9_-]+)[\\'\\´]?/i", $statement, $matches);
        if (!empty($matches)) {
            return array_unique($matches[2]);
        } else {
            return array();
        }
    }

    public function setQueryType($_queryType)
    {
        $this->_queryType = $_queryType;
    }

    /**
     * @param \Doctrine\ORM\EntityManager $em Entity manager
     * @param string $table Table name
     * @return ClassMetadata Entity class metadata, null if not found
     */
    protected function getClassMetaFromTableName($em, $table)
    {
        // Go through all the classes
        $classNames = $em->getConfiguration()->getMetadataDriverImpl()->getAllClassNames();
        foreach ($classNames as $className) {
            $classMetaData = $em->getClassMetadata($className);
            if ($table == $classMetaData->getTableName()) {
                return $classMetaData;
            }
        }
        return null;
    }

    /**
     * @param \Doctrine\ORM\EntityManager $em Entity manager
     * @param string $table Table name
     * @return string Entity class name, null if not found
     */
    protected function getClassNameFromTableName($em, $table)
    {
        $classMetaData = $this->getClassMetaFromTableName($em, $table);
        if ($classMetaData) {
            return $classMetaData->getName();
        }
        return null;
    }

    /**
     * @param \Doctrine\ORM\EntityManager $em Entity manager
     * @param string $className
     * @param string $column
     * @return string Field name, null if not found
     */
    protected function getFieldNameFromColumnName($em, $className, $column)
    {
        $classMetaData = $em->getClassMetadata($className);
        if ($classMetaData) {
            return $classMetaData->getFieldForColumn($column);
        }
        return null;
    }

    public function replace_tr($text)
    {
        $text = trim($text);
        $search = array('Ç','ç','Ğ','ğ','ı','İ','Ö','ö','Ş','ş','Ü','ü',' ');
        $replace = array('c','c','g','g','i','i','o','o','s','s','u','u','_');
        $new_text = str_replace($search, $replace, $text);
        return $new_text;
    }
}
