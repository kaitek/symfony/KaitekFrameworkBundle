<?php
/*
 * User tanımları sayfasının controller kodu
 * user,userright,userrole,customright(user kısmı) tek sayfada olacak
 * gelen değer post-put metodları içinde parse edilerek kayıt işlemleri yapılacak
 *
 */

namespace Kaitek\Bundle\FrameworkBundle\Controller;

use Kaitek\Bundle\FrameworkBundle\Entity\User;
use Kaitek\Bundle\FrameworkBundle\Entity\UserRight;
use Kaitek\Bundle\FrameworkBundle\Entity\CustomRight;

use Kaitek\Bundle\FrameworkBundle\Controller\BaseController as ControllerBase;
use Kaitek\Bundle\FrameworkBundle\Controller\BasePagingControllerInterface;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class UserController extends ControllerBase implements BasePagingControllerInterface
{
    public const ENTITY = 'KaitekFrameworkBundle:User';

    public function __construct()
    {
        parent::__construct();
    }

    public function getNewEntity()
    {
        return new User();
    }

    public function recordAdd(Request $request, $_locale, $page=1, $limit=25)
    {
        $cba=$this->checkBeforeAdd($request);
        if($cba===true) {
            if(!isset($this->_requestData)) {
                $this->_requestData = json_decode($request->getContent());
            }
            $username   = $this->getVar('username');
            $email      = $this->getVar('email');
            $password   = $this->getVar('password');
            $inactive   = false;
            $superadmin = false;

            //$manipulator = $this->container->get('fos_user.util.user_manipulator');
            //$newUser = $manipulator->create($username, $password, $email, !$inactive, $superadmin);
            $userManager = ($this->_container==null ? $this->container : $this->_container)->get('fos_user.user_manager');
            $newUser = $userManager->createUser();
            $newUser->setUsername($username);
            $newUser->setEmail($email);
            $newUser->setPlainPassword($password);
            $newUser->setEnabled(true);
            $newUser->setSuperAdmin((bool) $superadmin);
            $userManager->updateUser($newUser);

            $user = $this->getUser();
            $userId = $user->getId();

            $entity = $this->getDoctrine()
                ->getRepository(self::ENTITY)
                ->find($newUser->getId());

            $entity->setFullname($this->getVar('fullname'));
            $entity->setCreateuserId($userId);

            $em = $this->getDoctrine()->getManager();
            $em->getConnection()->beginTransaction();
            try {
                $em->flush();
                $em->getConnection()->commit();
            } catch (Exception $e) {
                // Rollback the failed transaction attempt
                $em->getConnection()->rollback();
                //throw $e;
                return $this->msgError($e->getMessage());
            }

            if(method_exists($this, 'showAllAction')) {
                return $this->showAllAction($request, $_locale, $page, $limit);
            } else {
                return $this->msgSuccess();
            }
        } else {
            return $cba;
        }
    }

    /**
     * @Route(path="/User/{pg}/{lm}/{id}/{v}", requirements={"pg": "\d+","lm": "\d+", "id": "\d+","v": "\d+"}, name="User-del", options={"expose"=true}, methods={"DELETE"})
     */
    public function deleteAction(Request $request, $_locale, $pg, $lm, $id, $v)
    {
        $entity = $this->getDoctrine()
            ->getRepository(self::ENTITY)
            ->find($id);

        return $this->recordDelete($request, $entity, $id, $v, $_locale, $pg, $lm);
    }

    public function getAllRecords($scope, $request, $page=1, $limit=25)
    {
        $records = parent::getAllRecords($scope, $request, $page, $limit);
        $repository=$this->getDoctrine()->getRepository('KaitekFrameworkBundle:Menu');
        $tr = $this->get('translator');
        $em = $this->getDoctrine()->getManager();
        foreach($records["User"]["records"] as $key=>$record) {
            $records["User"]["records"][$key]["rights"] = $repository->loadUserMenuTreeWithUserRights($record["id"], $tr, 'text', 'children');

            //$qb = $em->createQueryBuilder();
            //$queryrole = $qb->select('ur.id,r.id roleId,r.version')
            //        ->from(self::ENTITY,'u')
            //        ->join('u.roles', 'r')
            //        ->where('ur.deleteuserId is null and r.deleteuserId is null and u.id=:userId')
            //        ->setParameter('userId', $record["id"])
            //        ->getQuery();
            //$records["User"]["records"][$key]["roles"] = $queryrole->getArrayResult();
        }
        foreach($records["Role"]["records"] as $key=>$record) {
            $records["Role"]["records"][$key]["rights"] = $repository->loadRoleRights($record["id"]);
        }
        return $records;
    }

    public function getQBQuery()
    {
        $queries=array();
        $em = $this->getDoctrine()->getManager();
        $qb = $em->createQueryBuilder();
        $qb=$qb->select('u, r')
                ->from(self::ENTITY, 'u')
                ->leftJoin('u.user_roles', 'r')
                ->where($qb->expr()->notLike('u.roles', $qb->expr()->literal('%ROLE_SUPER_ADMIN%')))
                ->orderBy('u.username', 'ASC');
        $queries["User"]=array("qb"=>$qb,"getAll"=>true);

        $qb = $em->createQueryBuilder();
        $qb=$qb->select('cr.id,cr.entity,cr.fieldName,cr.fieldValue,cr.version')
                ->from(self::ENTITY, 'u')
                ->join('KaitekFrameworkBundle:CustomRight', 'cr', 'WITH', 'IDENTITY(cr.user)=u.id')
                ->where('cr.deleteuserId is null')
                ->orderBy('cr.entity', 'ASC')
                ->addOrderBy('cr.fieldName', 'ASC')
                ->addOrderBy('cr.fieldValue', 'ASC');
        $queries["CustomRight"]=array("qb"=>$qb,"getAll"=>true);

        $qb = $em->createQueryBuilder();
        $qb=$qb->select('r.id,r.name')
                ->from('KaitekFrameworkBundle:Role', 'r')
                ->where('r.deleteuserId is null');
        $queries["Role"]=array("qb"=>$qb,"getAll"=>true);

        return $queries;
    }

    /**
     * @Route(path="/User/{pg}/{lm}", requirements={"pg": "\d+","lm": "\d+"}, name="User-add", options={"expose"=true}, methods={"POST"})
     */
    public function postAction(Request $request, $_locale, $pg, $lm)
    {
        return $this->recordAdd($request, $_locale, $pg, $lm);
    }

    /**
     * @Route(path="/User/{pg}/{lm}/{id}/{v}", requirements={"pg": "\d+","lm": "\d+", "id": "\d+","v": "\d+"}, name="User-update", options={"expose"=true}, methods={"PUT"})
     */
    public function putAction(Request $request, $_locale, $pg, $lm, $id, $v)
    {
        if(!isset($this->_requestData)) {
            $this->_requestData = json_decode($request->getContent());
        }

        /** @var \Kaitek\Bundle\FrameworkBundle\Model\User $user */
        $user = $this->getUser();
        $userId = $user->getId();

        $selectedUser = $this->getDoctrine()
            ->getRepository(self::ENTITY)
            ->find($id);
        $selectedUserId = $selectedUser->getId();

        $cbu = $this->checkBeforeUpdate($request, $selectedUserId, $selectedUser, $selectedUser->getVersion());
        if ($cbu === true) {
            if (isset($this->_requestData->userRights)) {
                try {
                    $rolesToInsert = $this->_requestData->rolesToInsert;
                    $rolesToDelete = $this->_requestData->rolesToDelete;
                    $userRights = $this->_requestData->userRights;

                    $em = $this->getDoctrine()->getManager();
                    $em->getConnection()->beginTransaction();
                    $validator = $this->get('validator');

                    foreach ($rolesToInsert as $roleId) {
                        $role = $this->getDoctrine()
                            ->getRepository('KaitekFrameworkBundle:Role')
                            ->find($roleId);

                        $selectedUser->addUserRole($role);
                        $selectedUser->setUpdateuserId($userId);

                        $em->persist($selectedUser);
                        $em->flush();
                    }

                    foreach ($rolesToDelete as $roleToDelete) {
                        $role = $this->getDoctrine()
                            ->getRepository('KaitekFrameworkBundle:Role')
                            ->find($roleToDelete->id);
                        $selectedUser->removeUserRole($role);
                        $selectedUser->setUpdateuserId($userId);
                        $em->persist($selectedUser);
                        $em->flush();
                    }

                    foreach ($userRights as $d) {
                        if ($d->user_right_id && $d->version && !$d->aRight && !$d->uRight && !$d->dRight && !$d->gRight && !$d->uOwnRight && !$d->dOwnRight) {
                            //delete
                            $entity = $this->getDoctrine()
                                ->getRepository('KaitekFrameworkBundle:UserRight')
                                ->find($d->user_right_id);
                            $entity->setDeleteuserId($userId);
                            $em->persist($entity);
                            $em->flush();
                            $em->remove($entity);
                            $em->flush();
                        } elseif ($d->aRight || $d->uRight || $d->dRight || $d->gRight || $d->uOwnRight || $d->dOwnRight) {
                            if ($d->user_right_id && $d->version) {
                                //update
                                $entity = $this->getDoctrine()
                                    ->getRepository('KaitekFrameworkBundle:UserRight')
                                    ->find($d->user_right_id);
                                $entity->setUpdateuserId($userId);
                            } else {
                                //insert
                                $menu = $this->getDoctrine()
                                    ->getRepository('KaitekFrameworkBundle:Menu')
                                    ->find($d->menu_id);

                                $entity = new UserRight();
                                $entity->setCreateuserId($userId);
                                $entity->setUserId($selectedUser);
                                $entity->setMenuId($menu);
                                $errors = $this->getValidateMessage($validator->validate($entity));
                                if ($errors !== false) {
                                    $em->getConnection()->rollback();
                                    return $errors;
                                }
                            }
                            $entity->setGRight($d->gRight);
                            $entity->setARight($d->aRight);
                            $entity->setURight($d->uRight);
                            $entity->setDRight($d->dRight);
                            $entity->setUOwnRight($d->uOwnRight);
                            $entity->setDOwnRight($d->dOwnRight);
                            $em->persist($entity);
                            $em->flush();
                        }
                    }
                    $em->getConnection()->commit();
                } catch (Exception $e) {
                    // Rollback the failed transaction attempt
                    $em->getConnection()->rollback();
                    //throw $e;
                    return $this->msgError($e->getMessage());
                }
                return $this->showAllAction($request, $_locale, $pg, $lm);
            } else {
                /* @var $selectedUser User */
                //$selectedUser = $this->getDoctrine()
                //    ->getRepository(self::ENTITY)
                //    ->find($id);

                //setEntityWithRequest metoduna şifre alanını göndermemek için şifre güncellenerek request içinden siliniyor.
                if (!isset($this->_requestData)) {
                    $this->_requestData = json_decode($request->getContent());
                }
                $newPassword = $this->_requestData->password;
                unset($this->_requestData->password);
                if (isset($newPassword) && $newPassword != "") {
                    //$manipulator = $this->container->get('fos_user.util.user_manipulator');
                    //$manipulator->changePassword($selectedUser->getUsername(), $newPassword);
                    $userManager = ($this->_container==null ? $this->container : $this->_container)->get('fos_user.user_manager');
                    $user = $userManager->findUserByUsername($selectedUser->getUsername());
                    $user->setPlainPassword($newPassword);
                    $userManager->updateUser($user);
                }
                $selectedUser->setUsername($this->_requestData->username);
                $selectedUser->setFullname($this->_requestData->fullname);
                $selectedUser->setEmail($this->_requestData->email);
                $validator = $this->get('validator');
                $errors = $this->getValidateMessage($validator->validate($selectedUser));
                if ($errors!==false) {
                    return $errors;
                }
                /** @var EntityManager $em */
                $em = $this->getDoctrine()->getManager();
                $em->getConnection()->beginTransaction();
                try {
                    $em->persist($selectedUser);
                    $em->flush();
                    $em->getConnection()->commit();
                } catch (\Exception $e) {
                    // Rollback the failed transaction attempt
                    $em->getConnection()->rollback();
                    //throw $e;
                    return $this->msgError($e->getMessage());
                }
                if(method_exists($this, 'showAllAction') && $request->attributes->get('_isDCSService') !== true) {
                    return $this->showAllAction($request, $_locale, $pg, $lm);
                } else {
                    return $this->msgSuccess();
                }
                //return $this->recordEdit($request, $selectedUser, $id, $v, $_locale, $pg, $lm);
            }
        } else {
            return $cbu;
        }
    }

    /**
     * @Route(path="/User", name="User-module", options={"expose"=true}, methods={"POST"})
     */
    public function renderBackendModule(Request $request, $_locale)
    {
        $cbg = $this->checkBeforeGet($request);
        //$cbg=true;
        if ($cbg === true) {
            $data = $this->getBackendData($request, $_locale, self::ENTITY);

            return $this->render('@KaitekFramework/Backend/User.html.twig', $data);
        } else {
            return $cbg;
        }
    }
    /*
    public function renderBackendModule(Request $request, $_locale) {
        $cbg=$this->checkBeforeGet($request);
        //$cbg=true;
        if($cbg===true){
            $_rpp=$this->container->getParameter('kaitek_framework.recordsperpage');
            $records=$this->getAllRecords($this,$request,1,$_rpp);
            $data=array(
                "modulename" => $request->request->get('modulename')
                , "data" => $records
            );
            return $this->render('@KaitekFramework/Backend/User.html.twig', $data);
        }else {
            return $cbg;
        }
    }
*/
    /**
     * @Route(path="/User/{id}", requirements={"id": "\d+"}, name="User-show", options={"expose"=true}, methods={"GET"})
     */
    public function showAction(Request $request, $_locale, $id)
    {
        $cbg=$this->checkBeforeGet($request);
        //$cbg=true;
        if($cbg===true) {
            $records=$this->getRecordById($this, $request, "user", $id);
            return new JsonResponse($records);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/User/all/{pg}/{lm}", requirements={"pg": "\d+","lm": "\d+"}, name="User-showall", options={"expose"=true}, methods={"GET"})
     */
    public function showAllAction(Request $request, $_locale, $pg, $lm)
    {
        $cbg=$this->checkBeforeGet($request);
        //$cbg=true;
        if($cbg===true) {
            $records=$this->getAllRecords($this, $request, $pg, $lm);
            return new JsonResponse($records);
        } else {
            return $cbg;
        }
    }
}
