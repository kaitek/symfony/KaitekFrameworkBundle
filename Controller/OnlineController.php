<?php

namespace Kaitek\Bundle\FrameworkBundle\Controller;

use Kaitek\Bundle\FrameworkBundle\Entity\Online;

use Kaitek\Bundle\FrameworkBundle\Controller\BaseController as ControllerBase;
use Kaitek\Bundle\FrameworkBundle\Controller\BaseControllerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class OnlineController extends ControllerBase implements BaseControllerInterface
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getNewEntity()
    {
        return new Online();
    }

    /**
     * Silme işlemi yok
     *
     * @Route(path="/Online/{id}/{v}", requirements={"id": "\d+","v": "\d+"}, name="Online-del", options={"expose"=true}, methods={"DELETE"})
     */
    public function deleteAction(Request $request, $_locale, $id, $v)
    {
        return $this->msgSuccess();
        /*$entity = $this->getDoctrine()
            ->getRepository('KaitekFrameworkBundle:Menu')
            ->find($id);
        $cbd=$this->checkBeforeDelete($request,$id,$entity,$v);
        //$cbd=true;
        if($cbd===true){
            $user = $this->getUser();
            $userId = $user->getId();
            $entity->setDeleteuserId($userId);
            $em = $this->getDoctrine()->getManager();
            $em->getConnection()->beginTransaction();
            try {
                $em->persist($entity);
                $em->flush();
                $em->remove($entity);
                $em->flush();
                $em->getConnection()->commit();
            } catch (Exception $e) {
                // Rollback the failed transaction attempt
                $em->getConnection()->rollback();
                //throw $e;
                return $this->msgError($e->getMessage());
            }
            return $this->msgSuccess();
        }else {
            return $cbd;
        }*/
    }

    public function getQBQuery()
    {
        $queries=array();
        $em = $this->getDoctrine()->getManager();
        $qb = $em->createQueryBuilder();
        $qb=$qb->select('o.id,o.ip,o.host,o.outIp,o.userAgent,o.lastUpdate')
                ->from('KaitekFrameworkBundle:Online', 'o')
                ->join('KaitekFrameworkBundle:User', 'u', 'WITH', 'u.id=o.userId')
                ->where('o.active=1')
                ->orderBy('o.lastUpdate', 'DESC');
        $queries["Online"]=array("qb"=>$qb,"getAll"=>true);
        return $queries;
    }

    /**
     * Yeni kayıt ekleme yok
     *
     * @Route(path="/Online/", name="Online-add", options={"expose"=true}, methods={"POST"})
     */
    public function postAction(Request $request, $_locale)
    {
        return $this->msgSuccess();
        /*$cba=$this->checkBeforeAdd($request);
        //$cba=true;
        if($cba===true){
            $user = $this->getUser();
            $userId = $user->getId();
            $entity=new Menu();
            $entity->setMenuId;("deneme kategorisi");
            $entity->setCreateuserId($userId);
            $validator = $this->get('validator');
            $errors = $this->getValidateMessage($validator->validate($entity));
            if ($errors!==false){
                return $errors;
            }
            $em = $this->getDoctrine()->getManager();
            $em->getConnection()->beginTransaction();
            try {
                $em->persist($entity);
                $em->flush();
                $em->getConnection()->commit();
            } catch (Exception $e) {
                // Rollback the failed transaction attempt
                $em->getConnection()->rollback();
                //throw $e;
                return $this->msgError($e->getMessage());
            }
            return $this->msgSuccess();
        } else {
            return $cba;
        }*/
    }

    /**
     * kişinin oturumunun kapatılmasında kullanılıyor
     *
     * @Route(path="/Online/{id}/{v}", requirements={"id": "\d+","v": "\d+"}, name="Online-update", options={"expose"=true}, methods={"PUT"})
     */
    public function putAction(Request $request, $_locale, $id, $v)
    {
        $entity = $this->getDoctrine()
            ->getRepository('KaitekFrameworkBundle:Online')
            ->find($id);
        $cbu=$this->checkBeforeUpdate($request, $id, $entity, $v);
        //$cbu=true;
        if($cbu===true) {
            $user = $this->getUser();
            $userId = $user->getId();
            $entity->setActive(0);
            $validator = $this->get('validator');
            $errors = $this->getValidateMessage($validator->validate($entity));
            if ($errors!==false) {
                return $errors;
            }
            $em = $this->getDoctrine()->getManager();
            $em->getConnection()->beginTransaction();
            try {
                $em->flush();
                $em->getConnection()->commit();
            } catch (Exception $e) {
                // Rollback the failed transaction attempt
                $em->getConnection()->rollback();
                //throw $e;
                return $this->msgError($e->getMessage());
            }
            return $this->msgSuccess();
        } else {
            return $cbu;
        }
    }

    /**
     * @Route(path="/Online", name="Online-module", options={"expose"=true}, methods={"POST"})
     */
    public function renderBackendModule(Request $request, $_locale)
    {
        $cbg=$this->checkBeforeGet($request);
        //$cbg=true;
        if($cbg===true) {
            $_rpp=($this->_container==null ? $this->container : $this->_container)->getParameter('kaitek_framework.recordsperpage');
            $records=$this->getAllRecords($this, $request, 1, $_rpp);
            $data=array(
                "modulename" => $request->request->get('modulename')
                ,"records" => $records
            );
            return $this->render('@KaitekFramework/Backend/Online.html.twig', $data);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/Online/{id}", requirements={"id": "\d+"}, name="Online-show", options={"expose"=true}, methods={"GET"})
     */
    public function showAction(Request $request, $_locale, $id)
    {
        $cbg=$this->checkBeforeGet($request);
        //$cbg=true;
        if($cbg===true) {
            $records=$this->getRecordById($this, $request, "menu", $id);
            return new JsonResponse($records);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/Online/all/{page}/{limit}", defaults={"pg": 1, "lm": 25}, requirements={"pg": "\d+","lm": "\d+"}, name="Online-showall", options={"expose"=true}, methods={"GET"})
     */
    public function showAllAction(Request $request, $_locale, $pg, $lm)
    {
        $cbg=$this->checkBeforeGet($request);
        //$cbg=true;
        if($cbg===true) {
            $records=$this->getAllRecords($this, $request, $pg, $lm);
            return new JsonResponse($records);
        } else {
            return $cbg;
        }
    }
}
