<?php

namespace Kaitek\Bundle\FrameworkBundle\Controller;

use Doctrine\ORM\Query\ResultSetMapping;
use Kaitek\Bundle\FrameworkBundle\Controller\BaseController as ControllerBase;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\Security\Http\ParameterBagUtils;

class DefaultController extends ControllerBase
{
    /**
     * @Route(path="/", name="kaitek-framework-home-get", methods={"GET"})
     */
    public function indexAction(Request $request, $_locale)
    {
        $parameters = $this->checkAuthentication($request, $_locale);
        //if($parameters instanceof RedirectResponse){
        //    return $parameters;
        //}
        $response = null;
        if($parameters instanceof Response){
            $response = $parameters;
            $parameters = [];
        }
        if (isset($parameters['menu'])) {
            $_kt = ($this->_container==null?$this->container:$this->_container)->getParameter('kaitek_framework.kullanimtipi');
            $_twig = $_kt == 'intranet' ? 'Default/application-idx.html.twig' : 'Default/web.html.twig';
            if ($request->isXmlHttpRequest()!=true) {
                return $this->render($_twig, $parameters, $response);
            } else {
                return $this->msgError($this->get('translator')->trans('err.request.beforefilter', array(), 'KaitekFrameworkBundle'), 401);
            }
        }else{
            return $response;
        }
    }

    
}
