<?php

namespace Kaitek\Bundle\FrameworkBundle\Model;

use Kaitek\Bundle\FrameworkBundle\Model\Base as ModelBase;
/**
 * CustomRight
 */
abstract class CustomRight extends ModelBase implements BaseInterface,CustomRightInterface
{
    /**
     * @var string
     */
    protected $entity;

    /**
     * @var string
     */
    protected $fieldName;

    /**
     * @var string
     */
    protected $fieldValue;

    /**
     * @var Role
     */
    protected $role;

    /**
     * @var User
     */
    protected $user;

    /**
     * {@inheritdoc}
     */
    public function getEntity()
    {
        return $this->entity;
    }

    /**
     * {@inheritdoc}
     */
    public function setEntity($entity):CustomRight
    {
        $this->entity = $entity;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getFieldName()
    {
        return $this->fieldName;
    }

    /**
     * {@inheritdoc}
     */
    public function setFieldName($fieldName):CustomRight
    {
        $this->fieldName = $fieldName;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getFieldValue()
    {
        return $this->fieldValue;
    }

    /**
     * {@inheritdoc}
     */
    public function setFieldValue($fieldValue):CustomRight
    {
        $this->fieldValue = $fieldValue;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getRole()
    {
        return $this->role;
    }
    
    /**
     * {@inheritdoc}
     */
    public function setRole($role):CustomRight
    {
        $this->role = $role;
    
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getUser():User
    {
        return $this->user;
    }

    /**
     * {@inheritdoc}
     */
    public function setUser(User $user):CustomRight
    {
        $this->user = $user;

        return $this;
    }
}
