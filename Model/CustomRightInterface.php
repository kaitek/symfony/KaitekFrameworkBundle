<?php
namespace Kaitek\Bundle\FrameworkBundle\Model;

/**
 * CustomRightInterface
 */
interface CustomRightInterface
{
    /**
     * Get entity
     *
     * @return string
     */
    public function getEntity();

    /**
     * Set entity
     *
     * @param string $entity
     *
     * @return CustomRight
     */
    public function setEntity($entity): CustomRight;

    /**
     * Get fieldName
     *
     * @return string
     */
    public function getFieldName();

    /**
     * Set fieldName
     *
     * @param string $fieldName
     *
     * @return CustomRight
     */
    public function setFieldName($fieldName): CustomRight;

    /**
     * Get fieldValue
     *
     * @return string
     */
    public function getFieldValue();

    /**
     * Set fieldValue
     *
     * @param string $fieldValue
     *
     * @return CustomRight
     */
    public function setFieldValue($fieldValue): CustomRight;

    /**
     * Get role
     *
     * @return integer
     */
    public function getRole();

    /**
     * Set role
     *
     * @param integer $role
     *
     * @return CustomRight
     */
    public function setRole($role): CustomRight;

    /**
     * Get user
     *
     * @return User
     */
    public function getUser(): User;

    /**
     * Set user
     *
     * @param User $user
     *
     * @return CustomRight
     */
    public function setUser(User $user): CustomRight;


}
