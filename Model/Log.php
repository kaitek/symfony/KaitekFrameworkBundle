<?php

namespace Kaitek\Bundle\FrameworkBundle\Model;

use Doctrine\ORM\Mapping as ORM;

/**
 * Log
 */
class Log implements LogInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="ip", type="text", nullable=true)
     */
    protected $ip;

    /**
     * @var string
     *
     * @ORM\Column(name="method", type="string", length=20)
     */
    protected $method;

    /**
     * @var string
     *
     * @ORM\Column(name="paramsattributes", type="text", nullable=true)
     */
    protected $paramsattributes;

    /**
     * @var string
     *
     * @ORM\Column(name="paramsfiles", type="text", nullable=true)
     */
    protected $paramsfiles;

    /**
     * @var string
     *
     * @ORM\Column(name="paramsquery", type="text", nullable=true)
     */
    protected $paramsquery;

    /**
     * @var string
     *
     * @ORM\Column(name="paramsrequest", type="text", nullable=true)
     */
    protected $paramsrequest;

    /**
     * @var string
     *
     * @ORM\Column(name="path", type="string", length=255)
     */
    protected $path;

    /**
     * @var string
     *
     * @ORM\Column(name="sessionId", type="string", length=255)
     */
    protected $sessionId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="time", type="datetime")
     */
    protected $time;

    /**
     * @var User
     */
    protected $user;

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function setId(int $id): Log
    {
        $this->id = $id;
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    function getIp(): string
    {
        return $this->ip;
    }

    /**
     * {@inheritdoc}
     */
    function setIp(string $ip): Log
    {
        $this->ip = $ip;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getMethod(): string
    {
        return $this->method;
    }

    /**
     * {@inheritdoc}
     */
    public function setMethod(string $method): Log
    {
        $this->method = $method;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getParamsattributes(): string
    {
        return $this->paramsattributes;
    }

    /**
     * {@inheritdoc}
     */
    public function setParamsattributes(string $paramsattributes): Log
    {
        $this->paramsattributes = $paramsattributes;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getParamsfiles(): string
    {
        return $this->paramsfiles;
    }

    /**
     * {@inheritdoc}
     */
    public function setParamsfiles(string $paramsfiles): Log
    {
        $this->paramsfiles = $paramsfiles;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getParamsquery(): string
    {
        return $this->paramsquery;
    }

    /**
     * {@inheritdoc}
     */
    public function setParamsquery(string $paramsquery): Log
    {
        $this->paramsquery = $paramsquery;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getParamsrequest(): string
    {
        return $this->paramsrequest;
    }

    /**
     * {@inheritdoc}
     */
    public function setParamsrequest(string $paramsrequest): Log
    {
        $this->paramsrequest = $paramsrequest;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * {@inheritdoc}
     */
    public function setPath(string $path): Log
    {
        $this->path = $path;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getSessionId(): string
    {
        return $this->sessionId;
    }

    /**
     * {@inheritdoc}
     */
    public function setSessionId(string $sessionId): Log
    {
        $this->sessionId = $sessionId;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getTime(): \DateTime
    {
        return $this->time;
    }

    /**
     * {@inheritdoc}
     */
    public function setTime(\DateTime $time): Log
    {
        $this->time = $time;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * {@inheritdoc}
     */
    public function setUser(User $user): Log
    {
        $this->user = $user;
        $this->time = new \DateTime();
        return $this;
    }
}

