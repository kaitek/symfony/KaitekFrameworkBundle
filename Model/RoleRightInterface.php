<?php
namespace Kaitek\Bundle\FrameworkBundle\Model;

/**
 * RoleRightInterface
 */
interface RoleRightInterface
{
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId();

    /**
     * Get role
     *
     * @return integer
     */
    function getRoleId();
    
    /**
     * Set role
     *
     * @param integer $roleId
     *
     * @return RoleRight
     */
    function setRoleId($roleId);
    
    /**
     * Set menuId
     *
     * @param integer $menuId
     * @return RoleRightInterface
     */
    public function setMenuId($menuId);

    /**
     * Get menuId
     *
     * @return integer 
     */
    public function getMenuId();

    /**
     * Set gRight
     *
     * @param boolean $gRight
     * @return RoleRightInterface
     */
    public function setGRight($gRight);

    /**
     * Get gRight
     *
     * @return boolean 
     */
    public function getGRight();

    /**
     * Set aRight
     *
     * @param boolean $aRight
     * @return RoleRightInterface
     */
    public function setARight($aRight);

    /**
     * Get aRight
     *
     * @return boolean 
     */
    public function getARight();
    
    /**
     * Set uRight
     *
     * @param boolean $uRight
     * @return RoleRightInterface
     */
    public function setURight($uRight);

    /**
     * Get uRight
     *
     * @return boolean 
     */
    public function getURight();
    
    /**
     * Set dRight
     *
     * @param boolean $dRight
     * @return RoleRightInterface
     */
    public function setDRight($dRight);

    /**
     * Get dRight
     *
     * @return boolean 
     */
    public function getDRight();

    /**
     * Set uOwnRight
     *
     * @param boolean $uOwnRight
     *
     * @return RoleRightInterface
     */
    public function setUOwnRight($uOwnRight);
    
    /**
     * Get uOwnRight
     *
     * @return boolean
     */
    public function getUOwnRight();
     
    /**
     * Set dOwnRight
     *
     * @param boolean $dOwnRight
     *
     * @return RoleRightInterface
     */
    public function setDOwnRight($dOwnRight);
    
    /**
     * Get dOwnRight
     *
     * @return boolean
     */
    public function getDOwnRight();
    
    
}
