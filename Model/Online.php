<?php

namespace Kaitek\Bundle\FrameworkBundle\Model;

/**
 * Online
 */
abstract class Online implements OnlineInterface
{
    /**
     * @var integer
     */
    protected $id;

    /**
     * @var User
     */
    protected $user;

    /**
     * @var string
     */
    protected $sessionId;

    /**
     * @var string
     */
    protected $ip;

    /**
     * @var string
     */
    protected $host;

    /**
     * @var integer
     */
    protected $active;

    /**
     * @var string
     */
    protected $outIp;

    /**
     * @var string
     */
    protected $userAgent;

    /**
     * @var \DateTime
     */
    protected $lastUpdate;


    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function getUser(): ?User
    {
        return $this->user;
    }

    /**
     * {@inheritdoc}
     */
    public function setUser(User $user): Online
    {
        $this->user = $user;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getSessionId()
    {
        return $this->sessionId;
    }

    /**
     * {@inheritdoc}
     */
    public function setSessionId($sessionId)
    {
        $this->sessionId = $sessionId;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * {@inheritdoc}
     */
    public function setIp($ip)
    {
        $this->ip = $ip;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getHost()
    {
        return $this->host;
    }

    /**
     * {@inheritdoc}
     */
    public function setHost($host)
    {
        $this->host = $host;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * {@inheritdoc}
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getOutIp()
    {
        return $this->outIp;
    }

    /**
     * {@inheritdoc}
     */
    public function setOutIp($outIp)
    {
        $this->outIp = $outIp;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getUserAgent()
    {
        return $this->userAgent;
    }

    /**
     * {@inheritdoc}
     */
    public function setUserAgent($userAgent)
    {
        $this->userAgent = $userAgent;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getLastUpdate()
    {
        return $this->lastUpdate;
    }

    /**
     * {@inheritdoc}
     */
    public function setLastUpdate($lastUpdate)
    {
        $this->lastUpdate = $lastUpdate;

        return $this;
    }
}
