<?php

namespace Kaitek\Bundle\FrameworkBundle\Model;

use Kaitek\Bundle\FrameworkBundle\Model\Base as ModelBase;
/**
 * UserRight
 */
abstract class UserRight extends ModelBase implements UserRightInterface
{
    /**
     * @var integer
     */
    protected $id;

    /**
     * @var integer
     */
    protected $userId;
    
    /**
     * @var integer
     */
    protected $userroleId;
    
    /**
     * @var integer
     */
    protected $menuId;

    /**
     * @var boolean
     */
    protected $gRight;

    /**
     * @var boolean
     */
    protected $aRight;
    
    /**
     * @var boolean
     */
    protected $uRight;

    /**
     * @var boolean
     */
    protected $dRight;

    /**
     * @var boolean
     */
    protected $uOwnRight;
    
    /**
     * @var boolean
     */
    protected $dOwnRight;
    
    public function __construct()
    {
        parent::__construct();
        $this->gRight = false;
        $this->aRight = false;
        $this->uRight = false;
        $this->dRight = false;
        $this->uOwnRight = false;
        $this->dOwnRight = false;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     *
     * @return UserRight
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set menuId
     *
     * @param integer $menuId
     *
     * @return UserRight
     */
    public function setMenuId($menuId)
    {
        $this->menuId = $menuId;

        return $this;
    }

    /**
     * Get menuId
     *
     * @return integer
     */
    public function getMenuId()
    {
        return $this->menuId;
    }

    /**
     * Set gRight
     *
     * @param boolean $gRight
     *
     * @return UserRight
     */
    public function setGRight($gRight)
    {
        $this->gRight = $gRight;

        return $this;
    }

    /**
     * Get gRight
     *
     * @return boolean
     */
    public function getGRight()
    {
        return $this->gRight;
    }

    /**
     * Set aRight
     *
     * @param boolean $aRight
     *
     * @return UserRight
     */
    public function setARight($aRight)
    {
        $this->aRight = $aRight;

        return $this;
    }

    /**
     * Get aRight
     *
     * @return boolean
     */
    public function getARight()
    {
        return $this->aRight;
    }
    
    /**
     * Set uRight
     *
     * @param boolean $uRight
     *
     * @return UserRight
     */
    public function setURight($uRight)
    {
        $this->uRight = $uRight;

        return $this;
    }

    /**
     * Get uRight
     *
     * @return boolean
     */
    public function getURight()
    {
        return $this->uRight;
    }
    
    /**
     * Set dRight
     *
     * @param boolean $dRight
     *
     * @return UserRight
     */
    public function setDRight($dRight)
    {
        $this->dRight = $dRight;

        return $this;
    }

    /**
     * Get dRight
     *
     * @return boolean
     */
    public function getDRight()
    {
        return $this->dRight;
    }

    /**
     * Set uOwnRight
     *
     * @param boolean $uOwnRight
     *
     * @return UserRight
     */
    public function setUOwnRight($uOwnRight)
    {
        $this->uOwnRight = $uOwnRight;

        return $this;
    }

    /**
     * Get uOwnRight
     *
     * @return boolean
     */
    public function getUOwnRight()
    {
        return $this->uOwnRight;
    }
    
    /**
     * Set dOwnRight
     *
     * @param boolean $dOwnRight
     *
     * @return UserRight
     */
    public function setDOwnRight($dOwnRight)
    {
        $this->dOwnRight = $dOwnRight;

        return $this;
    }

    /**
     * Get dOwnRight
     *
     * @return boolean
     */
    public function getDOwnRight()
    {
        return $this->dOwnRight;
    }
    
    function getUserroleId() {
        return $this->userroleId;
    }

    function setUserroleId($userroleId) {
        $this->userroleId = $userroleId;
    }
    
}
