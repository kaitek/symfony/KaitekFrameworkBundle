<?php
/**
 * Created by PhpStorm.
 * User: halil
 * Date: 1/24/17
 * Time: 4:28 PM
 */

namespace Kaitek\Bundle\FrameworkBundle\Model;


interface LogInterface
{
    /**
     * Get id
     *
     * @return int
     */
    public function getId();

    /**
     * Set id
     * @param int $id
     *
     * @return Log
     */
    public function setId(int $id): Log;

    /**
     * @return string
     */
    function getIp(): string;

    /**
     * @param string $ip
     *
     * @return Log
     */
    function setIp(string $ip): Log;

    /**
     * Get method
     *
     * @return string
     */
    public function getMethod(): string;

    /**
     * Set method
     *
     * @param string $method
     *
     * @return Log
     */
    public function setMethod(string $method): Log;

    /**
     * Get paramsattributes
     *
     * @return string
     */
    public function getParamsattributes(): string;

    /**
     * Set paramsattributes
     *
     * @param string $paramsattributes
     *
     * @return Log
     */
    public function setParamsattributes(string $paramsattributes): Log;

    /**
     * Get paramsfiles
     *
     * @return string
     */
    public function getParamsfiles(): string;

    /**
     * Set paramsfiles
     *
     * @param string $paramsfiles
     *
     * @return Log
     */
    public function setParamsfiles(string $paramsfiles): Log;

    /**
     * Get paramsquery
     *
     * @return string
     */
    public function getParamsquery(): string;

    /**
     * Set paramsquery
     *
     * @param string $paramsquery
     *
     * @return Log
     */
    public function setParamsquery(string $paramsquery): Log;

    /**
     * Get paramsrequest
     *
     * @return string
     */
    public function getParamsrequest(): string;

    /**
     * Set paramsrequest
     *
     * @param string $paramsrequest
     *
     * @return Log
     */
    public function setParamsrequest(string $paramsrequest): Log;

    /**
     * Get path
     *
     * @return string
     */
    public function getPath(): string;

    /**
     * Set path
     *
     * @param string $path
     *
     * @return Log
     */
    public function setPath(string $path): Log;

    /**
     * Get sessionId
     *
     * @return string
     */
    public function getSessionId(): string;

    /**
     * Set sessionId
     *
     * @param string $sessionId
     *
     * @return Log
     */
    public function setSessionId(string $sessionId): Log;

    /**
     * Get time
     *
     * @return \DateTime
     */
    public function getTime(): \DateTime;

    /**
     * Set time
     *
     * @param \DateTime $time
     *
     * @return Log
     */
    public function setTime(\DateTime $time): Log;
}