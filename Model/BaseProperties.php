<?php
namespace Kaitek\Bundle\FrameworkBundle\Model;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class BaseProperties
 * @package Kaitek\Bundle\FrameworkBundle\Model
 */
trait BaseProperties {
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="create_user_id", type="integer")
     * @Assert\NotBlank(message="general.not_blank")
     */
    protected $createuserId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     * @Assert\NotBlank(message="general.not_blank")
     */
    protected $created;


    /**
     * @var integer
     *
     * @ORM\Column(name="update_user_id", type="integer", nullable=true)
     */
    protected $updateuserId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    protected $updated;


    /**
     * @ORM\PrePersist
     */
    public function createuserIdOnPrePersist()
    {
        if(!$this->createuserId){
            $this->setCreateuserId(0);
        }
    }

    /**
     * @var integer
     *
     * @ORM\Column(name="delete_user_id", type="integer", nullable=true)
     */
    protected $deleteuserId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    protected $deleted;

    /**
     * @ORM\Column(type="integer")
     * @ORM\Version
     */
    protected $version;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set id
     * @param int $id
     *
     * @return Base
     */
    public function setId(?int $id):Base
    {
        $this->id = $id;
        return $this;
    }


    public function getVersion() {
        return $this->version;
    }

    public function setVersion($version) {
        $this->version = $version;
    }

    public function getCreateuserId() {
        return $this->createuserId;
    }

    public function getCreated() {
        return $this->created;
    }

    public function getUpdateuserId() {
        return $this->updateuserId;
    }

    public function getUpdated() {
        return $this->updated;
    }

    public function getDeleteuserId() {
        return $this->deleteuserId;
    }

    public function getDeleted() {
        return $this->deleted;
    }

    public function setCreateuserId($createuserId) {
        $this->created = new \DateTime();
        $this->createuserId = $createuserId;
        $this->updated = new \DateTime();
        $this->updateuserId = $createuserId;
    }

    public function setCreated(\DateTime $created) {
        $this->created = $created;
    }

    public function setUpdateuserId($updateuserId) {
        $this->updated = new \DateTime();
        $this->updateuserId = $updateuserId;
    }

    public function setUpdated(\DateTime $updated) {
        $this->updated = $updated;
    }

    public function setDeleteuserId($deleteuserId) {
        $this->deleted = new \DateTime();
        $this->deleteuserId = $deleteuserId;
    }

    /**
     * Set cdate
     *
     * @param \DateTime $cdate
     *
     * @return Consultant
     */
    public function setCdate($cdate)
    {
        $this->cdate = $cdate;

        return $this;
    }

    /**
     * Set ddate
     *
     * @param \DateTime $ddate
     *
     * @return Consultant
     */
    public function setDdate($ddate)
    {
        $this->ddate = $ddate;

        return $this;
    }
}