<?php

namespace Kaitek\Bundle\FrameworkBundle\Model;

use Kaitek\Bundle\FrameworkBundle\Model\Base;
/**
 * Menu
 */
abstract class Menu extends Base implements BaseInterface, MenuInterface
{
    /**
     * @var integer
     */
    protected $sira;

    /**
     * @var integer
     */
    protected $parentid = '0';

    /**
     * @var string
     */
    protected $menuId;

    /**
     * @var string
     */
    protected $title = '';

    /**
     * @var string
     */
    protected $url = '';

    /**
     * @var integer
     */
    protected $width = 800;

    /**
     * @var integer
     */
    protected $height = 500;

    /**
     * @var string
     */
    protected $modulename = '';

    /**
     * @var string
     */
    protected $iconcls = '';

    /**
     * @var boolean
     */
    protected $multiplewindow;

    /**
     * @var boolean
     */
    protected $maximizable = false;

    /**
     * @var boolean
     */
    protected $resizable = false;

    /**
     * @var boolean
     */
    protected $mobiledevice = false;

    /**
     * Set sira
     *
     * @param integer $sira
     *
     * @return Menu
     */
    public function setSira($sira)
    {
        $this->sira = $sira;
    
        return $this;
    }

    /**
     * Get sira
     *
     * @return integer
     */
    public function getSira()
    {
        return $this->sira;
    }

    /**
     * Set parentid
     *
     * @param integer $parentid
     *
     * @return Menu
     */
    public function setParentId($parentid)
    {
        $this->parentid = $parentid;
    
        return $this;
    }

    /**
     * Get parentid
     *
     * @return integer
     */
    public function getParentId()
    {
        return $this->parentid;
    }

    /**
     * Set menuId
     *
     * @param string $menuId
     *
     * @return Menu
     */
    public function setMenuId($menuId)
    {
        $this->menuId = $menuId;
    
        return $this;
    }

    /**
     * Get menuId
     *
     * @return string
     */
    public function getMenuId()
    {
        return $this->menuId;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Menu
     */
    public function setTitle($title)
    {
        $this->title = $title;
    
        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set url
     *
     * @param string $url
     *
     * @return Menu
     */
    public function setUrl($url)
    {
        $this->url = $url;
    
        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set width
     *
     * @param integer $width
     *
     * @return Menu
     */
    public function setWidth($width)
    {
        $this->width = $width;
    
        return $this;
    }

    /**
     * Get width
     *
     * @return integer
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * Set height
     *
     * @param integer $height
     *
     * @return Menu
     */
    public function setHeight($height)
    {
        $this->height = $height;
    
        return $this;
    }

    /**
     * Get height
     *
     * @return integer
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * Set modulename
     *
     * @param string $modulename
     *
     * @return Menu
     */
    public function setModuleName($modulename)
    {
        $this->modulename = $modulename;
    
        return $this;
    }

    /**
     * Get modulename
     *
     * @return string
     */
    public function getModuleName()
    {
        return $this->modulename;
    }

    /**
     * Set iconcls
     *
     * @param string $iconcls
     *
     * @return Menu
     */
    public function setIconCls($iconcls)
    {
        $this->iconcls = $iconcls;
    
        return $this;
    }

    /**
     * Get iconcls
     *
     * @return string
     */
    public function getIconCls()
    {
        return $this->iconcls;
    }

    /**
     * Set multiplewindow
     *
     * @param boolean $multiplewindow
     *
     * @return Menu
     */
    public function setMultipleWindow($multiplewindow)
    {
        $this->multiplewindow = $multiplewindow;
    
        return $this;
    }

    /**
     * Get multiplewindow
     *
     * @return boolean
     */
    public function getMultipleWindow()
    {
        return $this->multiplewindow;
    }

    /**
     * Set maximizable
     *
     * @param boolean $maximizable
     *
     * @return Menu
     */
    public function setMaximizable($maximizable)
    {
        $this->maximizable = $maximizable;
    
        return $this;
    }

    /**
     * Get maximizable
     *
     * @return boolean
     */
    public function getMaximizable()
    {
        return $this->maximizable;
    }

    /**
     * Set resizable
     *
     * @param boolean $resizable
     *
     * @return Menu
     */
    public function setResizable($resizable)
    {
        $this->resizable = $resizable;
    
        return $this;
    }

    /**
     * Get resizable
     *
     * @return boolean
     */
    public function getResizable()
    {
        return $this->resizable;
    }

    /**
     * Set mobiledevice
     *
     * @param boolean $mobiledevice
     *
     * @return Menu
     */
    public function setMobileDevice($mobiledevice)
    {
        $this->mobiledevice = $mobiledevice;
    
        return $this;
    }

    /**
     * Get mobiledevice
     *
     * @return boolean
     */
    public function getMobileDevice()
    {
        return $this->mobiledevice;
    }
}

