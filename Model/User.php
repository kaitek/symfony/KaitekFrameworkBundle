<?php

namespace Kaitek\Bundle\FrameworkBundle\Model;
use Doctrine\Common\Collections\ArrayCollection;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * User
 */
abstract class User extends BaseUser implements BaseInterface, UserInterface
{
    use BaseProperties;

    /**
     * @var string
     */
    protected $avatar;

    /**
     * @var \Doctrine\Common\Collections\Collection|CustomRight[]
     */
    private $custom_rights;

    /**
     * @var string
     */
    protected $fullname = 'İsim Girilmemiş';

    /**
     * @var \Doctrine\Common\Collections\Collection|LogDetail[]
     */
    protected $log_details;

    /**
     * @var \Doctrine\Common\Collections\Collection|LogException[]
     */
    protected $log_exceptions;

    /**
     * @var \Doctrine\Common\Collections\Collection|Online[]
     */
    protected $onlines;

    /**
     * @var \Doctrine\Common\Collections\Collection|Role[]
     */
    protected $user_roles;

    /**
     * @var \Doctrine\Common\Collections\Collection|UserRight[]
     */
    protected $user_rights;

    /**
     * User constructor.
     */
    public function __construct()
    {
        parent::__construct();
        // your own logic
        $this->user_roles = new ArrayCollection();
        $this->custom_rights = new ArrayCollection();
        $this->log_details = new ArrayCollection();
        $this->log_exceptions = new ArrayCollection();
        $this->onlines = new ArrayCollection();
        $this->user_rights = new ArrayCollection();

        //FOSUser ile oluşturulup ardından createUserId atandığı için notNull constraint'e takılmamak için eklendi.
        $this->setCreateuserId(0);
    }

    /**
     * Get avatar
     *
     * @return string
     */
    public function getAvatar(): ?string
    {
        return $this->avatar;
    }

    /**
     * Set avatar
     *
     * @param string $avatar
     *
     * @return User
     */
    public function setAvatar(string $avatar): User
    {
        $this->avatar = $avatar;

        return $this;
    }

    /**
     * Get fullname
     *
     * @return string
     */
    public function getFullname(): ?string
    {
        return $this->fullname;
    }

    /**
     * Set fullname
     *
     * @param string $fullname
     * @return User
     */
    public function setFullname(string $fullname): User
    {
        $this->fullname = $fullname;

        return $this;
    }

    /**
     * @param Role $role
     */
    public function addUserRole(Role $role)
    {
        if ($this->user_roles->contains($role)) {
            return;
        }
        $this->user_roles->add($role);
        $role->addUser($this);
    }

    /**
     * @param Role $role
     */
    public function removeUserRole(Role $role)
    {
        if (!$this->user_roles->contains($role)) {
            return;
        }
        $this->user_roles->removeElement($role);
        $role->removeUser($this);
    }
}
