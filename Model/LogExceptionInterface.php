<?php
/**
 * Created by PhpStorm.
 * User: halil
 * Date: 1/24/17
 * Time: 4:28 PM
 */

namespace Kaitek\Bundle\FrameworkBundle\Model;


interface LogExceptionInterface extends LogInterface
{
    /**
     * @return int
     */
    public function getCode():int;

    /**
     * @param int $code
     * @return LogException
     */
    public function setCode(int $code):LogException;

    /**
     * @return string
     */
    public function getFile(): string;

    /**
     * @param string $file
     * @return LogException
     */
    public function setFile(string $file): LogException;

    /**
     * @return int
     */
    public function getLine(): int;

    /**
     * @param int $line
     * @return LogException
     */
    public function setLine(int $line): LogException;

    /**
     * @return string
     */
    public function getMessage(): string;

    /**
     * @param string $message
     * @return LogException
     */
    public function setMessage(string $message): LogException;
}