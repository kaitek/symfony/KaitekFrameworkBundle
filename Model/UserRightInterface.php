<?php
namespace Kaitek\Bundle\FrameworkBundle\Model;

/**
 * UserRightInterface
 */
interface UserRightInterface
{
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId();

    /**
     * Set userId
     *
     * @param integer $userId
     * @return UserRightInterface
     */
    public function setUserId($userId);

    /**
     * Get userId
     *
     * @return integer 
     */
    public function getUserId();

    /**
     * Get userRole
     *
     * @return integer
     */
    function getUserRoleId();
    
    /**
     * Set userRole
     *
     * @param integer $userroleId
     *
     * @return UserRight
     */
    function setUserRoleId($userroleId);
    
    /**
     * Set menuId
     *
     * @param integer $menuId
     * @return UserRightInterface
     */
    public function setMenuId($menuId);

    /**
     * Get menuId
     *
     * @return integer 
     */
    public function getMenuId();

    /**
     * Set gRight
     *
     * @param boolean $gRight
     * @return UserRightInterface
     */
    public function setGRight($gRight);

    /**
     * Get gRight
     *
     * @return boolean 
     */
    public function getGRight();

    /**
     * Set aRight
     *
     * @param boolean $aRight
     * @return UserRightInterface
     */
    public function setARight($aRight);

    /**
     * Get aRight
     *
     * @return boolean 
     */
    public function getARight();
    
    /**
     * Set uRight
     *
     * @param boolean $uRight
     * @return UserRightInterface
     */
    public function setURight($uRight);

    /**
     * Get uRight
     *
     * @return boolean 
     */
    public function getURight();
    
    /**
     * Set dRight
     *
     * @param boolean $dRight
     * @return UserRightInterface
     */
    public function setDRight($dRight);

    /**
     * Get dRight
     *
     * @return boolean 
     */
    public function getDRight();

    /**
     * Set uOwnRight
     *
     * @param boolean $uOwnRight
     *
     * @return UserRightInterface
     */
    public function setUOwnRight($uOwnRight);
    
    /**
     * Get uOwnRight
     *
     * @return boolean
     */
    public function getUOwnRight();
     
    /**
     * Set dOwnRight
     *
     * @param boolean $dOwnRight
     *
     * @return UserRightInterface
     */
    public function setDOwnRight($dOwnRight);
    
    /**
     * Get dOwnRight
     *
     * @return boolean
     */
    public function getDOwnRight();
    
    
}
