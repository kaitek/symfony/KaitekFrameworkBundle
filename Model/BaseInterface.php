<?php

/* 
 * Copyright (c) 2015, oguzhan
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

namespace Kaitek\Bundle\FrameworkBundle\Model;

interface BaseInterface
{
    
    /**
     * Returns the unique id.
     *
     * @return mixed
     */
    public function getId();

    /**
     * Set id
     * @param int $id
     *
     * @return Base
     */
    public function setId(int $id):Base;
    
     /**
     * Returns the creator user id.
     *
     * @return integer
     */
    public function getCreateuserId();
    
    /**
     * Gets the create date.
     *
     * @return \DateTime
     */
    public function getCreated();
       
    /**
     * Returns the updater user id.
     *
     * @return integer
     */
    public function getUpdateuserId();
    
    /**
     * Gets the update date.
     *
     * @return \DateTime
     */
    public function getUpdated();
    
    /**
     * Returns the delete user id.
     *
     * @return integer
     */
    public function getDeleteuserId();
    
    /**
     * Gets the delete date.
     *
     * @return \DateTime
     */
    public function getDeleted();
    
    /**
     * Sets the create user Id.
     *
     * @param integer $cuserId
     *
     * @return self
     */
    public function setCreateuserId($cuserId);
    
    /**
     * Sets the update user Id.
     *
     * @param integer $duserId
     *
     * @return self
     */
    public function setUpdateuserId($duserId);
    
    /**
     * Sets the delete user Id.
     *
     * @param integer $duserId
     *
     * @return self
     */
    public function setDeleteuserId($duserId);

}