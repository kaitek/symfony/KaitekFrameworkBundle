<?php

namespace Kaitek\Bundle\FrameworkBundle\Model;

use Kaitek\Bundle\FrameworkBundle\Model\LogDetail;
use Doctrine\ORM\Mapping as ORM;

/**
 * LogException
 */
class LogException extends Log
{
    /**
     * @var int
     */
    protected $code;

    /**
     * @var string
     */
    protected $file;

    /**
     * @var int
     */
    protected $line;

    /**
     * @var string
     */
    protected $message;

    /**
     * {@inheritdoc}
     */
    public function getCode(): int
    {
        return $this->code;
    }

    /**
     * {@inheritdoc}
     */
    public function setCode(int $code): LogException
    {
        $this->code = $code;
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getFile(): string
    {
        return $this->file;
    }

    /**
     * {@inheritdoc}
     */
    public function setFile(string $file): LogException
    {
        $this->file = $file;
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getLine(): int
    {
        return $this->line;
    }

    /**
     * {@inheritdoc}
     */
    public function setLine(int $line): LogException
    {
        $this->line = $line;
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * {@inheritdoc}
     */
    public function setMessage(string $message): LogException
    {
        $this->message = $message;
        return $this;
    }
}

