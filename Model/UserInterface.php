<?php
namespace Kaitek\Bundle\FrameworkBundle\Model;

/**
 * Interface UserInterface
 * @package Kaitek\Bundle\FrameworkBundle\Model;
 */
interface UserInterface
{
    /**
     * Get avatar
     *
     * @return string
     */
    public function getAvatar(): ?string;

    /**
     * Set avatar
     *
     * @param string $avatar
     *
     * @return User
     */
    public function setAvatar(string $avatar): User;

    /**
     * Get fullname
     *
     * @return string
     */
    public function getFullname(): ?string;

    /**
     * Set fullname
     *
     * @param string $fullname
     *
     * @return User
     */
    public function setFullname(string $fullname): User;

    /**
     * @param Role $role
     */
    public function addUserRole(Role $role);

    /**
     * @param Role $role
     */
    public function removeUserRole(Role $role);
}
