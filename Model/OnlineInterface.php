<?php
namespace Kaitek\Bundle\FrameworkBundle\Model;

/**
 * OnlineInterface
 */
interface OnlineInterface
{
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId();

    /**
     * Set userId
     *
     * @param User $user
     *
     * @return Online
     */
    public function setUser(User $user): Online;

    /**
     * Get user
     *
     * @return User
     */
    public function getUser(): ?User;

    /**
     * Set sessionId
     *
     * @param string $sessionId
     * @return OnlineInterface
     */
    public function setSessionId($sessionId);

    /**
     * Get sessionId
     *
     * @return string 
     */
    public function getSessionId();

    /**
     * Set ip
     *
     * @param string $ip
     * @return OnlineInterface
     */
    public function setIp($ip);

    /**
     * Get ip
     *
     * @return string 
     */
    public function getIp();

    /**
     * Set host
     *
     * @param string $host
     * @return OnlineInterface
     */
    public function setHost($host);

    /**
     * Get host
     *
     * @return string 
     */
    public function getHost();

    /**
     * Set active
     *
     * @param integer $active
     * @return OnlineInterface
     */
    public function setActive($active);

    /**
     * Get active
     *
     * @return integer 
     */
    public function getActive();

    /**
     * Set outIp
     *
     * @param string $outIp
     * @return OnlineInterface
     */
    public function setOutIp($outIp);

    /**
     * Get outIp
     *
     * @return string 
     */
    public function getOutIp();

    /**
     * Set userAgent
     *
     * @param string $userAgent
     * @return OnlineInterface
     */
    public function setUserAgent($userAgent);

    /**
     * Get userAgent
     *
     * @return string 
     */
    public function getUserAgent();

    /**
     * Set lastUpdate
     *
     * @param \DateTime $lastUpdate
     * @return OnlineInterface
     */
    public function setLastUpdate($lastUpdate);

    /**
     * Get lastUpdate
     *
     * @return \DateTime 
     */
    public function getLastUpdate();
}
