<?php
namespace Kaitek\Bundle\FrameworkBundle\Model;

/**
 * MenuInterface
 */
interface MenuInterface
{
    /**
     * Set sira
     *
     * @param integer $sira
     * @return MenuInterface
     */
    public function setSira($sira);

    /**
     * Get sira
     *
     * @return integer
     */
    public function getSira();

    /**
     * Set parentid
     *
     * @param integer $parentid
     * @return MenuInterface
     */
    public function setParentId($parentid);

    /**
     * Get parentid
     *
     * @return integer
     */
    public function getParentId();

    /**
     * Set menuId
     *
     * @param string $menuId
     * @return MenuInterface
     */
    public function setMenuId($menuId);

    /**
     * Get menuId
     *
     * @return string
     */
    public function getMenuId();

    /**
     * Set title
     *
     * @param string $title
     * @return MenuInterface
     */
    public function setTitle($title);

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle();

    /**
     * Set url
     *
     * @param string $url
     * @return MenuInterface
     */
    public function setUrl($url);

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl();

    /**
     * Set width
     *
     * @param integer $width
     * @return MenuInterface
     */
    public function setWidth($width);

    /**
     * Get width
     *
     * @return integer
     */
    public function getWidth();

    /**
     * Set height
     *
     * @param integer $height
     * @return MenuInterface
     */
    public function setHeight($height);

    /**
     * Get height
     *
     * @return integer
     */
    public function getHeight();

    /**
     * Set modulename
     *
     * @param string $modulename
     * @return MenuInterface
     */
    public function setModuleName($modulename);

    /**
     * Get modulename
     *
     * @return string
     */
    public function getModuleName();

    /**
     * Set iconcls
     *
     * @param string $iconcls
     * @return MenuInterface
     */
    public function setIconCls($iconcls);

    /**
     * Get iconcls
     *
     * @return string
     */
    public function getIconCls();

    /**
     * Set multiplewindow
     *
     * @param boolean $multiplewindow
     * @return MenuInterface
     */
    public function setMultipleWindow($multiplewindow);

    /**
     * Get multiplewindow
     *
     * @return boolean
     */
    public function getMultipleWindow();

    /**
     * Set maximizable
     *
     * @param boolean $maximizable
     * @return MenuInterface
     */
    public function setMaximizable($maximizable);

    /**
     * Get maximizable
     *
     * @return boolean
     */
    public function getMaximizable();

    /**
     * Set resizable
     *
     * @param boolean $resizable
     * @return MenuInterface
     */
    public function setResizable($resizable);

    /**
     * Get resizable
     *
     * @return boolean
     */
    public function getResizable();

    /**
     * Set mobiledevice
     *
     * @param boolean $mobiledevice
     * @return MenuInterface
     */
    public function setMobileDevice($mobiledevice);

    /**
     * Get mobiledevice
     *
     * @return boolean 
     */
    public function getMobileDevice();
}
