<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Kaitek\Bundle\FrameworkBundle\Composer;

use Symfony\Component\Process\Process;
use Symfony\Component\Process\PhpExecutableFinder;
use Symfony\Component\Templating\PhpEngine;
use Symfony\Component\Templating\TemplateNameParser;
use Symfony\Component\Templating\Loader\FilesystemLoader;
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Yaml\Exception\ParseException;
use Composer\Script\Event;

/**
 * @author Jordi Boggiano <j.boggiano@seld.be>
 */
class ScriptHandler
{
    /**
     * Composer variables are declared static so that an event could update
     * a composer.json and set new options, making them immediately available
     * to forthcoming listeners.
     */
    protected static $options = array(
        'symfony-app-dir' => 'app',
        'symfony-web-dir' => 'web',
        'symfony-assets-install' => 'hard',
        'symfony-cache-warmup' => false,
    );

    protected static function hasDirectory(Event $event, $configName, $path, $actionName)
    {
        if (!is_dir($path)) {
            $event->getIO()->write(sprintf('The %s (%s) specified in composer.json was not found in %s, can not %s.', $configName, $path, getcwd(), $actionName));

            return false;
        }

        return true;
    }

   /**
    * Builds FrameworkBundle and FrontendBundle.
    *
    * @param Event $event
    */
   public static function initFrontend(Event $event)
   {
       $options = static::getOptions($event);
       $consoleDir = static::getConsoleDir($event, 'clear the cache');
       $rootDir = getcwd().'/';

       static::checkInstallation($event);

       if (null === $consoleDir) {
           return;
       }
       //if (!file_exists('gulpfile.js')) {
       //    static::executeCommand(
       //        $event, $consoleDir,
       //        'kaitek_frontend:setup  --csspre=sass --coffee=false '.
       //        '--no-interaction --src-dir=src/Resources/assets '.
       //        '--pipeline=webpack',
       //        $options['process-timeout']
       //      );
       //}
       //static::executeShellCommand($event, 'npm install', $rootDir, 600);

       static::executeCommand($event, $consoleDir, 'kai:build', $options['process-timeout']);
       static::executeCommand($event, $consoleDir, 'kai:build --dev', $options['process-timeout']);
   }

   /**
    * Checks FrameworkBundle installation steps and modifies if needed.
    *
    * @param Event $event
    */
   public static function checkInstallation(Event $event)
   {
       $options = static::getOptions($event);
       $appDir = $options['symfony-app-dir'];
       $dir = explode('vendor', __DIR__);
       $appRoot = basename($dir[0]);

       $configDir = "$appDir/config/";

       static::addKeyValueToYml($event, $configDir, 'config.yml', 'fos_user', array(
         'db_driver' => 'orm',
         'firewall_name' => 'main',
         'user_class' => 'Kaitek\Bundle\FrameworkBundle\Entity\User'
       ));

       static::checkYmlForImportArray($event, $configDir, 'config.yml', '@KaitekFrameworkBundle/Resources/config/config.yml');

       static::checkYmlForImportArray($event, $configDir, 'config_dev.yml', 'parameters_dev.yml');
       static::checkYmlForImportArray($event, $configDir, 'config_dev.yml', '@KaitekFrameworkBundle/Resources/config/config_dev.yml');

       static::checkYmlForImportArray($event, $configDir, 'config_prod.yml', 'parameters_prod.yml');
       static::checkYmlForImportArray($event, $configDir, 'config_prod.yml', '@KaitekFrameworkBundle/Resources/config/config_prod.yml');

       static::checkYmlAndPrepend($event, $configDir, 'routing.yml', 'kaitek_framework',
          array('resource' => '@KaitekFrameworkBundle/Resources/config/routing.yml'));

       $defaultParameters = array(
         'locale' => 'tr',
         'http_port' => '80',
         'https_port' => '443',
         'kaitek_framework.kullanimtipi' => 'intranet',
         'kaitek_framework.multiplesessions' => true,
         'kaitek_framework.recordsperpage' => '25',
         'router.request_context.host' => 'localhost',
         'router.request_context.scheme' => 'https',
         'compass_bin_path' => null,
       );
       static::addKeyValueToYml($event, $configDir, 'parameters.yml', 'parameters', $defaultParameters);
       static::addKeyValueToYml($event, $configDir, 'parameters.yml.dist', 'parameters', $defaultParameters);
       static::addKeyValueToYmlIfNotSet($event, $configDir, 'parameters_dev.yml', 'parameters',
          array('router.request_context.base_url' => "/$appRoot/web/app_dev.php"));
       static::addKeyValueToYmlIfNotSet($event, $configDir, 'parameters_prod.yml', 'parameters',
          array('router.request_context.base_url' => "/$appRoot/web"));

          $aclRules = array(
              '{ path: ^/api, roles: [ IS_AUTHENTICATED_FULLY ] }',
              '{ path: ^/, role: IS_AUTHENTICATED_ANONYMOUSLY }',
            );
          $file = "$configDir/security.yml";
          $yaml = static::getYml($file);
          if (is_array($yaml)) {
              if (!isset($yaml['security']['access_control'])) {
                  $yaml['security']['access_control'] = array();
              }
              foreach ($aclRules as $rule) {
                  $found = false;
                  $rule = Yaml::dump(Yaml::parse($rule), 0);
                  foreach ($yaml['security']['access_control'] as $ac) {
                      if (Yaml::dump($ac, 0) == $rule) {
                          $found = true;
                          break;
                      }
                  }
                  if (!$found) {
                      $yaml['security']['access_control'][] = Yaml::parse($rule);
                  }
              }
              $yaml = Yaml::dump($yaml, 3);
              file_put_contents($file, $yaml);
          } else {
              throw new \RuntimeException("Unable to parse $file");
          }

       return;
   }

   /**
    * Checks config{_dev|_prod}.yml for import key.
    *
    * @param Event $event
    */
   public static function checkYmlForImportArray(Event $event, $dir, $filename, $key)
   {
       $file = "$dir/$filename";
       try {
           $yaml = Yaml::parse(file_get_contents($file));
       } catch (ParseException $e) {
           throw new \RuntimeException(sprintf('Unable to parse the YAML string: %s in %s', $e->getMessage(), $file));
       }
       if (is_array($yaml)) {
           if (!isset($yaml['imports'])) {
               $yaml['imports'] = array();
           }
           $imports = $yaml['imports'];
           unset($yaml['imports']);
           $keyDoesntExists = true;
           foreach ($imports as $import) {
               if ($import['resource'] == $key) {
                   $keyDoesntExists = false;
                   break;
               }
           }
           if ($keyDoesntExists) {
               $imports[] = array('resource' => $key);
           }
           if (isset($yaml['doctrine']) && isset($yaml['doctrine']['dbal'])) {
               $yaml['doctrine']['dbal']['driver'] = 'pdo_pgsql';
           }

           $yaml = Yaml::dump(array('imports' => $imports), 2).Yaml::dump($yaml, 5);
           file_put_contents($file, $yaml);
       } else {
           throw new \RuntimeException("Unable to parse $file");
       }

       return true;
   }

   /**
    * Checks config{_dev|_prod}.yml for import key and replaces if exists.
    *
    * @param Event $event
    */
   public static function addKeyValueToYml(Event $event, $dir, $filename, $key, $values)
   {
       $file = "$dir/$filename";
       $yaml = static::getYml($file);
       if (is_array($yaml)) {
           if (!isset($yaml[$key])) {
               $yaml[$key] = array();
           }
           foreach ($values as $subKey => $value) {
               $yaml[$key][$subKey] = $value;
           }
           $yaml = Yaml::dump($yaml, 5);
           file_put_contents($file, $yaml);
       } else {
           throw new \RuntimeException("Unable to parse $file");
       }

       return true;
   }

    /**
     * Checks config{_dev|_prod}.yml for import key and keeps if exists.
     *
     * @param Event $event
     */
    public static function addKeyValueToYmlIfNotSet(Event $event, $dir, $filename, $key, $values)
    {
        $file = "$dir/$filename";
        $yaml = static::getYml($file);
        if (is_array($yaml)) {
            if (!isset($yaml[$key])) {
                $yaml[$key] = array();
            }
            foreach ($values as $subKey => $value) {
                if (!isset($yaml[$key][$subKey])) {
                    $yaml[$key][$subKey] = $value;
                }
            }
            $yaml = Yaml::dump($yaml, 5);
            file_put_contents($file, $yaml);
        } else {
            throw new \RuntimeException("Unable to parse $file");
        }

        return true;
    }

    /**
     * Checks {routing}.yml if bundle's routing.yml prepended to top of file.
     *
     * @param Event $event
     */
    public static function checkYmlAndPrepend(Event $event, $dir, $filename, $key, $values, $subLevel = 2)
    {
        $file = "$dir/$filename";
        $yaml = static::getYml($file);
        if (is_array($yaml)) {
            unset($yaml[$key]);
            $yaml = Yaml::dump(array($key => $values), $subLevel).Yaml::dump($yaml, $subLevel);
            file_put_contents($file, $yaml);
        } else {
            throw new \RuntimeException("Unable to parse $file");
        }

        return true;
    }

    public static function getYml($file)
    {
        if (file_exists($file)) {
            try {
                $yaml = Yaml::parse(file_get_contents($file));
            } catch (ParseException $e) {
                throw new \RuntimeException(sprintf('Unable to parse the YAML string: %s in %s', $e->getMessage(), $file));
            }
        } else {
            $yaml = array();
        }

        return $yaml;
    }

    protected static function executeCommand(Event $event, $consoleDir, $cmd, $timeout = 300)
    {
        $php = escapeshellarg(static::getPhp(false));
        $phpArgs = implode(' ', array_map('escapeshellarg', static::getPhpArguments()));
        $console = escapeshellarg($consoleDir.'/console');
        if ($event->getIO()->isDecorated()) {
            $console .= ' --ansi';
        }

        $process = new Process($php.($phpArgs ? ' '.$phpArgs : '').' '.$console.' '.$cmd, null, null, null, $timeout);
        $process->run(function ($type, $buffer) use ($event) { $event->getIO()->write($buffer, false); });
        if (!$process->isSuccessful()) {
            throw new \RuntimeException(sprintf("An error occurred when executing the \"%s\" command:\n\n%s\n\n%s.", escapeshellarg($cmd), $process->getOutput(), $process->getErrorOutput()));
        }
    }

    protected static function executeShellCommand(Event $event, $cmd, $cwd = null, $timeout = 300)
    {
        $process = new Process($cmd, $cwd, null, null, $timeout);
        $process->run(function ($type, $buffer) use ($event) { $event->getIO()->write($buffer, false); });
        if (!$process->isSuccessful()) {
            throw new \RuntimeException(sprintf("An error occurred when executing the \"%s\" command:\n\n%s\n\n%s.", escapeshellarg($cmd), $process->getOutput(), $process->getErrorOutput()));
        }
    }

    protected static function getOptions(Event $event)
    {
        $options = array_merge(static::$options, $event->getComposer()->getPackage()->getExtra());

        $options['symfony-assets-install'] = getenv('SYMFONY_ASSETS_INSTALL') ?: $options['symfony-assets-install'];
        $options['symfony-cache-warmup'] = getenv('SYMFONY_CACHE_WARMUP') ?: $options['symfony-cache-warmup'];

        $options['process-timeout'] = $event->getComposer()->getConfig()->get('process-timeout');

        return $options;
    }

    protected static function getPhp($includeArgs = true)
    {
        $phpFinder = new PhpExecutableFinder();
        if (!$phpPath = $phpFinder->find($includeArgs)) {
            throw new \RuntimeException('The php executable could not be found, add it to your PATH environment variable and try again');
        }

        return $phpPath;
    }

    protected static function getPhpArguments()
    {
        $arguments = array();

        $phpFinder = new PhpExecutableFinder();
        if (method_exists($phpFinder, 'findArguments')) {
            $arguments = $phpFinder->findArguments();
        }

        /*
        https://github.com/composer/composer/issues/5783#issuecomment-265133968

        if (false !== $ini = php_ini_loaded_file()) {
            $arguments[] = '--php-ini='.$ini;
        }
        */
        if ($env = strval(getenv('COMPOSER_ORIGINAL_INIS'))) {
            $paths = explode(PATH_SEPARATOR, $env);
            $ini = array_shift($paths);
        } else {
            $ini = php_ini_loaded_file();
        }

        if (!empty($ini)) {
            $arguments[] = '--php-ini='.$ini;
        }

        return $arguments;
    }

    /**
     * Returns a relative path to the directory that contains the `console` command.
     *
     * @param Event  $event      The command event
     * @param string $actionName The name of the action
     *
     * @return string|null The path to the console directory, null if not found.
     */
    protected static function getConsoleDir(Event $event, $actionName)
    {
        $options = static::getOptions($event);

        if (static::useNewDirectoryStructure($options)) {
            if (!static::hasDirectory($event, 'symfony-bin-dir', $options['symfony-bin-dir'], $actionName)) {
                return;
            }

            return $options['symfony-bin-dir'];
        }

        if (!static::hasDirectory($event, 'symfony-app-dir', $options['symfony-app-dir'], 'execute command')) {
            return;
        }

        return $options['symfony-app-dir'];
    }

    /**
     * Returns true if the new directory structure is used.
     *
     * @param array $options Composer options
     *
     * @return bool
     */
    protected static function useNewDirectoryStructure(array $options)
    {
        return isset($options['symfony-var-dir']) && is_dir($options['symfony-var-dir']);
    }
}
