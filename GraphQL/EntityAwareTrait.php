<?php

namespace Kaitek\Bundle\FrameworkBundle\GraphQL;

use Doctrine\Common\Persistence\Mapping\ClassMetadata;
use Doctrine\ORM\EntityManager;

trait EntityAwareTrait
{
    public $entity;

    /**
     * @param mixed $entity
     * @param EntityManager $em
     */
    public function setEntity($entity, EntityManager $em)
    {
        $this->entity = $entity;
        $this->em = $em;
        $this->entityMetadata = $this->em->getClassMetadata($this->entity);
    }

    /**
     * @var EntityManager
     */
    public $em;

    /**
     * @var ClassMetadata
     */
    public $entityMetadata;

    /**
     * @var EntityAwareTrait
     */
    private $parent;

    public function setParent($parent)
    {
        if($parent) {
            $this->parent = $parent;
        }
    }

    public function isNotCyclicWith($parentEntityName)
    {
        if(!$this->parent) {
            return true;
        }
        return $this->parent->entity !== $parentEntityName;
    }
}
