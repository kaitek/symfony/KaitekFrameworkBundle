<?php

namespace Kaitek\Bundle\FrameworkBundle\GraphQL;

use Youshido\GraphQL\Config\Object\InputObjectTypeConfig;
use Youshido\GraphQL\Type\InputObject\AbstractInputObjectType;
use Youshido\GraphQL\Type\Scalar\DateTimeType;
use Youshido\GraphQL\Type\Scalar\IntType;
use Youshido\GraphQL\Type\Scalar\StringType;

abstract class AbstractBaseInputObjectType extends AbstractInputObjectType
{
    public $entity; // Entity classname
    public $entityShortName; // Entity short classname
    public $className; // Entity short classname
    public $em;
    public $entityMetadata;

    public function __construct(array $config = array(), $entity = null, $em = null, $entityMetadata = null)
    {
        if ($entity) {
            $this->entity = $entity;
            $classArray = explode("\\", $entity);
            $this->entityShortName = ucfirst(end($classArray));
            $this->className = $this->entityShortName.'ParamsType';
            $this->em = $em;
            $this->entityMetadata = $entityMetadata;
        }
        parent::__construct($config);
    }

    /**
     * @param InputObjectTypeConfig $config
     */
    public function build($config)
    {
        if($this->entityMetadata) {
            $this->buildFieldsFromEntity();
        }
    }

    public function buildFieldsFromEntity()
    {
        $fields = array();
        foreach ($this->entityMetadata->fieldMappings as $fKey => $f) {
            if (!in_array($fKey, array('createuserId', 'created', 'updateuserId', 'updated', 'deleteuserId', 'deleted', 'version'))) {
                $type = null;
                if ($f['type'] === 'integer') $type = new IntType();
                if ($f['type'] === 'string') $type = new StringType();
                if ($f['type'] === 'datetime') $type = new DateTimeType();
                if ($type) {
                    $fields[$fKey] = $type;
                }
            }
        }
        if(sizeof($fields)>0) {
            $this->config->addFields($fields);
        }
    }

    public function getName()
    {
        return $this->className;
    }
}
