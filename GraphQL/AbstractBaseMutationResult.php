<?php

namespace Kaitek\Bundle\FrameworkBundle\GraphQL;

use Youshido\GraphQL\Type\AbstractType;
use Youshido\GraphQL\Type\Object\AbstractObjectType;
use Youshido\GraphQL\Type\Scalar\BooleanType;
use Youshido\GraphQL\Type\Scalar\StringType;

class AbstractBaseMutationResult extends AbstractObjectType
{
    private $listItemType;

    public function __construct(AbstractType $type)
    {
        parent::__construct(array(
            'name'        => sprintf('%sMutationResult', $type->getName()),
            'description' => sprintf('Returns %s mutation result', $type->getName())
        ));
    }

    public function build($config)
    {
        $config->addFields(array(
            'success' => new BooleanType(),
            'msg' => new StringType(),
        ));
    }

}