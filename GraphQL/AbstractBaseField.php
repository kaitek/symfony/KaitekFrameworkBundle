<?php

namespace Kaitek\Bundle\FrameworkBundle\GraphQL;

use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Kaitek\Bundle\FrameworkBundle\ORM\Mapping\FieldData;
use Symfony\Component\Translation\Translator;
use Youshido\GraphQL\Parser\Ast\Field;
use Youshido\GraphQL\Config\Field\FieldConfig;
use Youshido\GraphQL\Execution\ResolveInfo;
use Youshido\GraphQL\Type\AbstractType;
use Youshido\GraphQL\Type\ListType\ListType;
use Youshido\GraphQL\Type\Object\AbstractObjectType;
use Youshido\GraphQL\Type\Scalar\DateTimeType;
use Youshido\GraphQL\Type\Scalar\IdType;
use Youshido\GraphQL\Type\Scalar\IntType;
use Youshido\GraphQL\Type\Scalar\StringType;
use Youshido\GraphQLBundle\Field\AbstractContainerAwareField;
use Youshido\GraphQLExtension\Type\PaginatedResultType;
use Youshido\GraphQLExtension\Type\PagingParamsType;
use Youshido\GraphQLExtension\Type\Sorting\SortingParamsType;


abstract class AbstractBaseField extends AbstractContainerAwareField
{
    use EntityAwareTrait;
    use QueryAwareTrait;

    const MODE_FETCH = 1;
    const MODE_UPDATE = 3;
    const MODE_DELETE = 4;

    /**
     * @var AnnotationReader
     */
    protected $annotationReader;

    protected $mode;
    protected $type;
    protected $buildOverriden = false;
    private $_relationIndexes = array();
    private $_relationIdx = 1;

    public function __construct(array $config = array(), AbstractBaseObjectType $query = null, $parent = null, $entity = null, $mode = AbstractBaseField::MODE_FETCH)
    {
        $this->annotationReader = new AnnotationReader();
        $this->mode = $mode;
        if ($parent) { $this->setParent($parent); }
        if ($query) { $this->setQuery($query); }
        if ($query) { $this->setContainer($query->getContainer()); }
        if ($entity) {
            $this->setEntity($entity, $this->container->get('doctrine.orm.entity_manager'));
            $this->type = $this->query->generateNewTypeFromEntity($this->entity, $this);
        }
        // if($config['name'] && $this->mode === AbstractBaseField::MODE_FETCH) {
        if($config['name']) {
            $config['type'] = $this->getType();
        }
        parent::__construct($config);

        if ($entity) {
            // 'build' metodunun (bu sınıftan türetilmiş child sınıfta) override edilip edilmediğini kontrol et.
            $this->buildOverriden = $this->_methodOverriden('build');
        }
    }

    private function _getRelationIndex($qb, $fieldName) {
        if (array_key_exists($fieldName, $this->_relationIndexes)) {
            return $this->_relationIndexes[$fieldName];
        }
        $idx = 'f'.($this->_relationIdx++);
        $qb = $qb->leftJoin($fieldName, $idx);
        return $this->_relationIndexes[$fieldName] = $idx;
    }

    private function _methodOverriden($methodName) {
        $reflector = new \ReflectionMethod($this, $methodName);
        return ($reflector->getDeclaringClass()->getName() === get_class($this));
    }

    /**
     * @param Field[] $fields
     * @param string $tableAlias
     * @return string[]
     */
    private function _resolveFieldsAndRelationIndexes($qb, $fields, $tableAlias = 't') {
        $select = array($tableAlias);
        foreach ($fields as $field) {
            $subFields = $field->getFields();
            if ($subFields) {
                $relIdx = $this->_getRelationIndex($qb, $tableAlias.'.'.$field->getName());
                $select = array_merge($select, $this->_resolveFieldsAndRelationIndexes($qb, $subFields, $relIdx, $tableAlias.'.'.$field->getName()));
            }
        }
        return $select;
    }

    public function build(FieldConfig $config)
    {
        if(!$this->buildOverriden && $this->entityMetadata) {
            // auto build from entity if build method not overridden
            $this->buildArgumentsFromEntity($config);
        } else {
            // add arguments from base entity if build method overridden
            $config->addArguments(
                array(
                    'id' => array(
                        'type' => new IdType(),
                        'description' => 'Primary Key'
                    ),
                    'createuserId' => array(
                        'type' => new IdType(),
                        'description' => 'Who created?'
                    ),
                    'created' => array(
                        'type' => new DateTimeType(),
                        'description' => 'When created?'
                    ),
                    'updateuserId' => array(
                        'type' => new IdType(),
                        'description' => 'Who updated?'
                    ),
                    'updated' => array(
                        'type' => new DateTimeType(),
                        'description' => 'When updated?'
                    )
                )
            );
        }
    }

    public function buildArgumentsFromEntity(FieldConfig $config)
    {
        $arguments = array();
        $sortableFields = array();
        foreach ($this->entityMetadata->fieldMappings as $fKey => $f) {
            $type = null;
            if (!in_array($fKey, array('createuserId', 'created', 'updateuserId', 'updated', 'deleteuserId', 'deleted', 'version'))){
                $sortableFields[] = $fKey;
            }
            if ($f['type'] === 'integer') $type = new IntType();
            if ($f['type'] === 'string') $type = new StringType();
            if ($f['type'] === 'datetime') $type = new DateTimeType();
            if ($type) {
                $arguments[$fKey] = $type;
            }
        }
        foreach ($this->entityMetadata->associationMappings as $mapKey => $map) {
            if($mapKey !== 'documents') {
                if($map['type'] & \Doctrine\ORM\Mapping\ClassMetadataInfo::TO_MANY) {
                    $arguments[$mapKey] = new ListType($this->query->generateForeignParamsTypeFromEntity(
                        $map['targetEntity'],
                        $this->em,
                        $this->em->getClassMetadata($map['targetEntity'])
                    ));
                } else {
                    $arguments[$mapKey] = $this->query->generateForeignParamsTypeFromEntity(
                        $map['targetEntity'],
                        $this->em,
                        $this->em->getClassMetadata($map['targetEntity'])
                    );
                }
            }
        }

        $arguments['paging'] = new PagingParamsType();
        $arguments['sorting'] = new SortingParamsType($this->query->generateNewTypeFromEntity($this->entity, $this), $sortableFields);

        $config->addArguments($arguments);
    }

    /**
     * @return AbstractObjectType|AbstractType
     */
    public function getType()
    {
        if ($this->mode === AbstractBaseField::MODE_UPDATE) {
            return new AbstractBaseMutationResult($this->type);
        } else {
            return new PaginatedResultType($this->type);
        }
    }

    public function getUser()
    {
        if (!$this->container->has('security.token_storage')) {
            throw new \LogicException('The SecurityBundle is not registered in your application.');
        }

        if (null === $token = $this->container->get('security.token_storage')->getToken()) {
            return;
        }

        if (!is_object($user = $token->getUser())) {
            // e.g. anonymous authentication
            return;
        }

        return $user;
    }

    public function getValidateMessage($errors) {
        if (count($errors) > 0) {
            $errorsString="";
            foreach($errors as $error){
                $em = $this->em;
                $cd = $em->getClassMetadata(get_class($error->getRoot()));

                $reflectionProperty = new \ReflectionProperty($cd->name, $error->getPropertyPath());
                /** @var FieldData $propertyAnnotation */
                $propertyAnnotation = $this->annotationReader->getPropertyAnnotation($reflectionProperty, 'Kaitek\Bundle\FrameworkBundle\ORM\Mapping\FieldData');
                /** @var Column $propertyAnnotationORM */
                $propertyAnnotationORM = $this->annotationReader->getPropertyAnnotation($reflectionProperty, 'Doctrine\ORM\Mapping\Column');

                /** @var Translator $translator */
                $translator = $this->container->get('translator');
                if(true) {
                    if($propertyAnnotation && isset($propertyAnnotation->label)){
                        $fieldLabelTranslationId = explode('.', $propertyAnnotation->label);
                    } else {
                        $fieldLabelTranslationId = array($cd->name, $error->getPropertyPath());
                    }
                    if (count($fieldLabelTranslationId) > 1) {
                        $p1 = is_object($propertyAnnotation) ? $propertyAnnotation : new \stdClass();
                        if(!isset($propertyAnnotation->label)){
                            $name = explode('\\', $cd->getName());
                            $name = $name[count($name) - 1];
                            $p1->label = $name . '.' . $error->getPropertyPath();
                        }
                        $p2 = is_object($propertyAnnotationORM) ? $propertyAnnotationORM : new \stdClass();
                        $params = array_merge(get_object_vars($p1), get_object_vars($p2));
                        $formattedParams = array();
                        foreach ($params as $key => $param) {
                            if (is_array($param)) {
                                /**
                                 * TODO: Mevcut kod, parametreleri formattedParams altına alırken 2 alt kademeye kadar destekliyor. Daha fazlasına inmek gerekirse bu kısım recursive bir fonksiyona dönüştürülmeli.
                                 */
                                foreach ($param as $subKey => $value) {
                                    $formattedParams['{{ ' . $key . '.' . $subKey . ' }}'] = $translator->trans($value);
                                }
                            } else {
                                $formattedParams['{{ ' . $key . ' }}'] = $translator->trans($param, [], substr($param, 0, strpos($param, '.')));
                            }
                        }
                        $errorsString .= $translator->trans($error->getMessage(), $formattedParams, substr($param, 0, strpos($param, '.')));
                    } else {
                        // $errorsString .= "(" . $message . ") " . $error->getMessage() . "<br>";
                        $errorsString .= $error->getMessage() . "<br>";
                    }
                } else {
                    $errorsString .= "(" . $error->getPropertyPath() . ") " . $error->getMessage() . "<br>";
                }
            }
            return $this->msgError( $errorsString );
        }
        return false;
    }

    public function msgError($message="")
    {
        // TODO: AbstractBaseMutationResult sınıfından türeyen sonucun döndürüleceği hale getirilebilir.
        return array('success' => false, 'msg' => $message);
    }

    public function msgSuccess($message="")
    {
        // TODO: AbstractBaseMutationResult sınıfından türeyen sonucun döndürüleceği hale getirilebilir.
        return array('success' => true, 'msg' => $message);
    }

    public function resolve($value, array $args, ResolveInfo $info)
    {
        if($this->mode === AbstractBaseField::MODE_FETCH) {
            if (isset($args['paging'])) {
                $paging = $args['paging'];
                unset($args['paging']);
            }
            if (isset($args['sorting'])) {
                $sorting = $args['sorting'];
                unset($args['sorting']);
            }

            $qb = $this->container
                ->get('doctrine.orm.entity_manager')
                ->createQueryBuilder();

            $qb = $qb->from($this->entity, 't');

            $where = 'where';
            $subArgs = array();

            $select = array();

            $fields = $info->getFieldASTList();
            if ($fields[0] && $fields[0]->getName() == 'items') { // standart paging destekli resultset ise
                $select = array_merge($select, $this->_resolveFieldsAndRelationIndexes($qb, $fields[0]->getFields()));
            } else {
                // TODO: Paging dışında istek olacağı zaman düzenle!
                $select[] = 't';
            }

            foreach ($args as $argName => $argValue) {
                $argType = $info->getField()->getArgument($argName)->getConfig()->getType();
                if ($argType instanceof AbstractBaseInputObjectType) {
                    $fKey = $this->_getRelationIndex($qb, 't.'.$argName);
                    if (!in_array($fKey, $select)) {
                        $select[] = $fKey;
                    }
                    foreach ($argValue as $subArgName => $subArgValue) {
                        $qb = $qb->$where($fKey.'.'.$subArgName.' = :'.$fKey.'_'.$subArgName);
                        $subArgs[$fKey.'_'.$subArgName] = $subArgValue;
                    }
                    unset($args[$argName]);
                } else {
                    $isString = $argType instanceof StringType;
                    if ($isString && strpos($argValue, '%') !== false) {
                        $qb = $qb->$where('t.'.$argName.' like :'.$argName);
                    } else {
                        $qb = $qb->$where('t.'.$argName.' = :'.$argName);
                    }
                }
                $where = 'andWhere';
            }
            $qb = $qb->select(implode(',', $select));
            $qb = $qb->setParameters(array_merge($args, $subArgs));

            if (isset($sorting)) {
                $qb = $qb->orderBy('t.'.$sorting['field'], $sorting['order'] == 1 ? 'ASC' : 'DESC');
            }

            $pagingInfo = array();
            if (isset($paging)) {
                $qb = $qb->setFirstResult($paging['offset'])->setMaxResults($paging['limit']);
                $paginator = new Paginator($qb, $fetchJoinCollection = true);
                $pagingInfo['totalCount'] = count($paginator);
                $pagingInfo['limit'] = $paging['limit'];
                $pagingInfo['offset'] = $paging['offset'];

                $res = $paginator;
            } else {
                $res = $qb->getQuery()->getResult();
            }

            // TODO: PaginatedResultType sınıfından türeyen sonucun döndürüleceği hale getirilebilir.
            return array(
                'pagingInfo' => $pagingInfo,
                'items' => $res
            );
        } elseif ($this->mode === AbstractBaseField::MODE_UPDATE) {
            /** @var \Kaitek\Bundle\FrameworkBundle\Model\Base $entity */

            $user = $this->getUser();
            $userId = $user->getId();
            $mode = 'insert';
            if(array_key_exists('id', $args) && $args['id']){
                $mode = 'update';
                $entity = $this->container->get('doctrine')
                    ->getRepository($this->entity)
                    ->find($args['id']);
                $entity->setUpdateuserId($userId);
                unset($args['id']);
            } else {
                $entity = new $this->entity();
                $entity->setCreateuserId($userId);
            }

            foreach($args as $key=>$val){
                $defaultValue = null;
                try {
                    $reflectionProperty = new \ReflectionProperty($this->entity, $key);
                    $propertyAnnotation = $this->annotationReader->getPropertyAnnotation(
                        $reflectionProperty,
                        'Doctrine\ORM\Mapping\Column'
                    );
                    if (isset($propertyAnnotation->options['default'])) {
                        $defaultValue = $propertyAnnotation->options['default'];
                    }
                    $setterFn = "set".strtoupper(substr($key, 0, 1)).substr($key, 1);

                    $manyToOne = $this->annotationReader->getPropertyAnnotation(
                        $reflectionProperty,
                        'Doctrine\ORM\Mapping\ManyToOne'
                    );

                    $manyToMany = $this->annotationReader->getPropertyAnnotation(
                        $reflectionProperty,
                        'Doctrine\ORM\Mapping\ManyToMany'
                    );

                    if (isset($manyToOne) && isset($manyToOne->targetEntity)) {
                        $joinColumn = $this->annotationReader->getPropertyAnnotation(
                            $reflectionProperty,
                            'Doctrine\ORM\Mapping\JoinColumn'
                        );
                        if (isset($joinColumn) && isset($joinColumn->referencedColumnName)) {
                            $foreignRefVal = $val ? $val : $defaultValue;
                            $foreignReference = $this->container->get('doctrine')
                                ->getRepository($manyToOne->targetEntity)
                                ->findBy(array($joinColumn->referencedColumnName => $foreignRefVal[$joinColumn->referencedColumnName]));
                            if (is_array($foreignReference) && count($foreignReference) > 0) {
                                $foreignReference = $foreignReference[0];
                                $entity->$setterFn($foreignReference);
                            } else {
                                // kontrol gerekli mi?
                            }
                        }
                    } else if (isset($manyToMany) && isset($manyToMany->targetEntity)) {
                        $foreignEntities = new ArrayCollection();
                        foreach ($val as $v) {
                            // TODO: 'id' değeri dinamik hale getirilebilir
                            $foreignRefVal = $v['id'];
                            $foreignReference = $this->container->get('doctrine')
                                ->getRepository($manyToMany->targetEntity)
                                ->findBy(array('id' => $foreignRefVal));
                            if ($foreignReference) {
                                $foreignEntities->add($foreignReference[0]);
                            }
                        }
                        $entity->$setterFn($foreignEntities);
                    } else {
                        $val = $val ? $val : $defaultValue;
                        // if (isset($propertyAnnotation->type) && ($propertyAnnotation->type == 'datetime' || $propertyAnnotation->type == 'date')) {
                        //     $dVal = date_create_from_format("Y-m-d H:i:s", $val);
                        //     if($val!==null){
                        //         $entity->$setterFn($dVal);
                        //     }
                        // } else {
                            $entity->$setterFn($val);
                        // }
                    }
                } catch (\Exception $e) {
                    // kontrol gerekli mi?
                    $m1 = 1;
                }
            }

            $validator = $this->container->get('validator');
            $errors = $this->getValidateMessage($validator->validate($entity));
            if ($errors!==false)
                return $errors;

            $this->em->getConnection()->beginTransaction();
            try {
                $this->em->persist($entity);
                $this->em->flush();
                $this->em->getConnection()->commit();
            } catch (\Exception $e) {
                // Rollback the failed transaction attempt
                $this->em->getConnection()->rollback();
                //throw $e;
                return $this->msgError($e->getMessage());
            }

            return $this->msgSuccess('Kayıt başarıyla '.($mode === 'insert' ? 'eklendi.' : 'düzenlendi.'));
        }
    }
}
