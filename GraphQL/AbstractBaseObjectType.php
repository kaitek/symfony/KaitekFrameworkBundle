<?php

namespace Kaitek\Bundle\FrameworkBundle\GraphQL;


use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Youshido\GraphQL\Config\Object\ObjectTypeConfig;
use Youshido\GraphQL\Type\Object\AbstractObjectType;
use Youshido\GraphQL\Type\Scalar\DateTimeType;
use Youshido\GraphQL\Type\ListType\ListType;
use Youshido\GraphQL\Type\Scalar\IdType;
use Youshido\GraphQL\Type\Scalar\IntType;
use Youshido\GraphQL\Type\Scalar\StringType;


abstract class AbstractBaseObjectType extends AbstractObjectType
{
    use EntityAwareTrait;
    use QueryAwareTrait;
    use ContainerAwareTrait {
        setContainer as _traitSetContainer;
    }

    public function __construct(array $config = array(), AbstractBaseObjectType $query = null, $parent = null, $entity = null)
    {
        if ($query) {
            if ($parent) { $this->setParent($parent); }
            $this->setQuery($query);
            $this->setContainer($query->getContainer());
            if($entity) {
                $this->setEntity($entity, $this->container->get('doctrine.orm.entity_manager'));
            }
        }

        return parent::__construct($config);
    }

    public function build($config)
    {
        if ($this->entityMetadata) {
            $this->buildFieldsFromEntity($config);
        } else {
            $config->addFields(
                array(
                    'id' => new IdType(),
                    'createuserId' => new IdType(),
                    'created' => new DateTimeType(),
                    'updateuserId' => new IdType(),
                    'updated' => new DateTimeType()
                )
            );
        }
    }

    public function buildFieldsFromEntity(ObjectTypeConfig $config)
    {
        $fields = array();
        foreach ($this->entityMetadata->fieldMappings as $fKey => $f) {
            // TODO: Entity üzerinde annotation ile (ya da config.yml içinde) tanımlanarak dinamik hale getirilebilir.
            $forbidden = false;
            if(array_key_exists('declared', $f) && $f['declared'] == 'FOS\\UserBundle\\Model\\User') {
                if(in_array($f['fieldName'], array('password', 'salt'))) {
                    $forbidden = true;
                }
            }
            if (!$forbidden) {
                $type = null;
                $sortableFields[] = $fKey;
                if ($f['type'] === 'integer') {
                    $type = new IntType();
                }
                if ($f['type'] === 'string') {
                    $type = new StringType();
                }
                if ($f['type'] === 'datetime') {
                    $type = new DateTimeType();
                }
                if ($type) {
                    $fields[$fKey] = $type;
                }
            }
        }
        foreach ($this->entityMetadata->associationMappings as $mapKey => $map) {
            if($mapKey !== 'documents' && $this->isNotCyclicWith($map['targetEntity'])) {
                if($map['type'] & \Doctrine\ORM\Mapping\ClassMetadataInfo::TO_MANY) {
                    $fields[$mapKey] = new ListType(
                        $this->query->generateNewTypeFromEntity($map['targetEntity'], $this)
                    );
                } else {
                    $fields[$mapKey] = $this->query->generateNewTypeFromEntity($map['targetEntity'], $this);
                }
            }
        }

        $config->addFields($fields);
    }

    public function generateForeignParamsTypeFromEntity($entity = null, $em, $entityMetadata) {
        $result = new class (array(), $entity, $em, $entityMetadata) extends AbstractBaseInputObjectType {
        };
        return $result;
    }

    public function generateNewTypeFromEntity($typeClassName = null, $parent): AbstractBaseObjectType {
        $result = new class (array(), $this, $parent, $typeClassName) extends AbstractBaseObjectType {
            protected $className;

            public function __construct(array $config = array(), AbstractBaseObjectType $query = null, $parent = null, $entity = null)
            {
                $this->className = $query->getShortClassName($entity);
                return parent::__construct($config, $query, $parent, $entity);
            }

            public function getName() {
                return $this->className;
            }
        };
        return $result;
    }

    protected function generateNewFieldFromEntity($entityName, $mode = AbstractBaseField::MODE_FETCH) {
        $name = $this->getShortClassName($entityName);
        if ($mode === AbstractBaseField::MODE_UPDATE) {
            $name = 'addEdit'.$name;
        }

        return new class (array('name' => $name), $this, null, $entityName, $mode) extends AbstractBaseField {
        };
    }

    public function getContainer(): ContainerInterface {
        return $this->container;
    }

    public function getShortClassName($className) {
        $classArray = explode("\\", $className);
        return end($classArray);

    }

    public function setContainer(ContainerInterface $container = null)
    {
        $this->_traitSetContainer($container);
        if($this->config && $this->config->getFields()) {
            foreach ($this->config->getFields() as $fieldName => $field) {
                $field->setContainer($container);
            }
        }
    }
}