<?php

namespace Kaitek\Bundle\FrameworkBundle\GraphQL;

trait QueryAwareTrait
{

    /**
     * @var AbstractBaseObjectType
     */
    private $query;

    /**
     * @return AbstractBaseObjectType
     */
    public function getQuery()
    {
        return $this->query;
    }

    /**
     * @param AbstractBaseObjectType $query
     */
    public function setQuery($query)
    {
        $this->query = $query;
    }
}
