<?php

namespace Kaitek\Bundle\FrameworkBundle\GraphQL;

use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Youshido\GraphQL\Config\Schema\SchemaConfig;
use Youshido\GraphQL\Schema\AbstractSchema;
use Youshido\GraphQL\Type\Object\AbstractObjectType;


abstract class AbstractBaseSchema extends AbstractSchema implements ContainerAwareInterface {
    use ContainerAwareTrait {
        setContainer as _traitSetContainer;
    }

    var $query;
    var $mutation;

    public function __construct(array $config = [])
    {
        parent::__construct($config);
    }

    public function build(SchemaConfig $config)
    {
        // $this->config = $config;
        // $query = new Query(array());
        // $query->setContainer($this->container);
        // $config->setQuery($query);
        // $config->setMutation(new Mutation());
    }


    // Introspection query esnasında field'ların setContainer'ları çağırılmadığı için override edildi.
    public function setContainer(ContainerInterface $container = null)
    {
        if ($this->query) {
            $this->query->setContainer($container);
            $this->config->setQuery($this->query);
        }
        if ($this->mutation) {
            $this->mutation->setContainer($container);
            $this->config->setMutation($this->mutation);
        }
        // $fields = $this->config->getQuery()->getFields();
        // foreach($fields as $field) {
        //     $field->setContainer($this->container);
        // };
        $this->_traitSetContainer($container);
    }

    protected function setQuery(AbstractObjectType $query) {
        $this->query = $query;
    }

    protected function setMutation(AbstractObjectType $mutation) {
        $this->mutation = $mutation;
    }
}