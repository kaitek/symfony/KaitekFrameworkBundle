<?php

namespace Kaitek\Bundle\FrameworkBundle\Handler;

//use Kaitek\Bundle\FrameworkBundle\Entity\LogDetail;

use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\Security\Http\HttpUtils;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Routing\Router;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Security\Http\Authentication\DefaultAuthenticationFailureHandler;
// use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class LoginFailureHandler extends DefaultAuthenticationFailureHandler
{
    /**
     * @var EntityManager
     */
    protected $em;
    /**
     * @var Router $router
     */
    protected $router;

    /**
     * @var AuthorizationChecker $authChecker
     */
    protected $authChecker;

    /**
     * @var Container $container
     */
    protected $container;

    /**
     * @var TranslatorInterface $translator
     */
    protected $translator;

    protected $csrf_provider;

    /**
     * Constructor
     * @param HttpUtils $httpUtils
     * @param EntityManager $em
     * @param Router $router
     * @param AuthorizationChecker $authChecker
     * @param Container $container
     * @param $csrf_provider
     * @param array $options
     */
    public function __construct(HttpKernelInterface $httpKernel, HttpUtils $httpUtils, EntityManager $em, Router $router, AuthorizationChecker $authChecker, Container $container, $csrf_provider, TranslatorInterface $translator, array $options)
    {
        parent::__construct($httpKernel, $httpUtils, $options);
        $this->em = $em;
        $this->router = $router;
        $this->authChecker = $authChecker;
        $this->container = $container;
        $this->csrf_provider = $csrf_provider;
        $this->translator = $translator;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception): Response
    {
        #$_kt=$this->container->getParameter('kaitek_framework.kullanimtipi');
        $locale = $request->getSession()->get('_locale');
        $url = 'kaitek-framework-home-get';

        if($request->isXmlHttpRequest()) {
            $response = new JsonResponse(array( 'success' => false, 'message' => $this->translator->trans($exception->getMessageKey(), $exception->getMessageData(), 'KaitekFrameworkBundle'),'token'=>$this->csrf_provider->getToken('authenticate')->getValue() ), 417);
        } else {
            $response = new RedirectResponse($this->router->generate($url, array('_locale' => $locale)));
        }
        return $response;
        #$response = parent::onAuthenticationSuccess( $request, $token );
        #return $response;
    }
}
