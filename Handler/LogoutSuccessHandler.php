<?php

/*
 * Copyright (c) 2016, oguzhan
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

namespace Kaitek\Bundle\FrameworkBundle\Handler;

use Kaitek\Bundle\FrameworkBundle\Entity\LogDetail;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Symfony\Component\Security\Csrf\CsrfTokenManager;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Router;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Security\Http\HttpUtils;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Http\Logout\DefaultLogoutSuccessHandler;
use Doctrine\ORM\EntityManager;

class LogoutSuccessHandler extends DefaultLogoutSuccessHandler
{
    /**
     *
     * @var TokenStorageInterface
     */
    private $tokenStorage;
    /**
     * @var EntityManager
     */
    protected $em;
    /**
     * @var Router $router
     */
    protected $router;

    /**
     * Constructor
     * @param HttpUtils $httpUtils
     * @param EntityManager $em
     * @param string $targetUrl
     */
    public function __construct(HttpUtils $httpUtils, TokenStorageInterface $tokenStorage, EntityManager $em, Router $router, $targetUrl = '/')
    {
        parent::__construct($httpUtils, $targetUrl);
        $this->em = $em;
        $this->router = $router;
    }

    public function onLogoutSuccess(Request $request): ?Response
    {
        $locale = $request->getSession()->get('_locale');
        $sessionId=$request->getSession()->getId();
        $userId = 0;
        $onlineUser = $this->em->getRepository('KaitekFrameworkBundle:Online')->loadOnlineBySessionId($sessionId);
        $user = (null !== $onlineUser) ? $onlineUser->getUser() : 0;
        if ($user) {
            $lastUpdate = new \DateTime();
            if(isset($onlineUser)) {
                $onlineUser->setActive(0);
                $onlineUser->setLastUpdate($lastUpdate);
            }
            $this->em->flush();
            //$userId = $onlineUser->getUserId();
            //$user = $this->em->getRepository('KaitekFrameworkBundle:User')->findOneBy(array('id' => $userId));
            $log = new LogDetail();
            $log->setUser($user);
            $log->setSessionId($sessionId);
            $log->setMethod("POST");
            $log->setPath("/logout");
            $log->setParamsrequest("");
            $log->setParamsquery("");
            $log->setParamsattributes("");
            $log->setParamsfiles("");
            $log->setIp($request->getClientIp());
            $log->setTime(new \DateTime());

            $this->em->persist($log);
            $this->em->flush();
        }
        $url = 'kaitek-framework-home-get';
        if($request->getMethod() === Request::METHOD_POST) {
            $response = new JsonResponse(array( 'success' => true ), 102);
        } else {
            $response = new RedirectResponse($this->router->generate($url, array('_locale' => $locale)));
        }
        return $response;

        //return parent::onLogoutSuccess($request);
    }
}
