<?php

/*
 * Copyright (c) 2016, oguzhan
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

namespace Kaitek\Bundle\FrameworkBundle\Handler;

#use Symfony\Component\HttpFoundation\JsonResponse;
#use Symfony\Component\HttpFoundation\Response;
use Kaitek\Bundle\FrameworkBundle\Entity\Online;
use Kaitek\Bundle\FrameworkBundle\Entity\LogDetail;

use Symfony\Component\Security\Http\HttpUtils;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Routing\Router;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Security\Http\Authentication\DefaultAuthenticationSuccessHandler;

class LoginSuccessHandler extends DefaultAuthenticationSuccessHandler
{
    /**
     * @var EntityManager
     */
    protected $em;
    /**
     * @var Router $router
     */
    protected $router;

    /**
     * @var AuthorizationChecker $authChecker
     */
    protected $authChecker;

    /**
     * @var Container $container
     */
    protected $container;

    /**
     * Constructor
     * @param HttpUtils $httpUtils
     * @param EntityManager $em
     * @param Router $router
     * @param AuthorizationChecker $authChecker
     * @param Container $container
     * @param array $options
     */
    public function __construct(HttpUtils $httpUtils, EntityManager $em, Router $router, AuthorizationChecker $authChecker, Container $container, array $options)
    {
        parent::__construct($httpUtils, $options);
        $this->em = $em;
        $this->router = $router;
        $this->authChecker = $authChecker;
        $this->container = $container;
    }

    protected function setOnline(Request $request, TokenInterface $token)
    {
        $user = $token->getUser();
        $session = $request->getSession();
        $sessionId=$session->getId();
        $_ms=$this->container->getParameter('kaitek_framework.multiplesessions');
        $onlineUser = $this->em->getRepository('KaitekFrameworkBundle:Online')->loadOnlineByUserInfo($user, $sessionId, $_ms);
        if (null === $onlineUser/*$onlineUser instanceof OnlineInterface*/) {
            $userAgent=$request->headers->get('User-Agent');
            $ip=$request->getClientIp();
            $host=$request->getHost();
            $lastUpdate = new \DateTime();

            $online = new Online();
            if($user) {
                $online->setUser($user);
            }
            $online->setSessionId($sessionId);
            $online->setIp($ip);
            $online->setOutIp($ip);
            $online->setHost($host);
            $online->setActive(1);
            $online->setUserAgent($userAgent);
            $online->setLastUpdate($lastUpdate);

            $this->em->persist($online);
            $this->em->flush();

            $log = new LogDetail();
            if($user) {
                $log->setUser($user);
            }
            $log->setSessionId($sessionId);
            $log->setMethod("POST");
            $log->setPath("/login_check");
            $log->setParamsrequest("");
            $log->setParamsquery("");
            $log->setParamsattributes("");
            $log->setParamsfiles("");
            $log->setIp($request->getClientIp());
            $log->setTime(new \DateTime());

            $this->em->persist($log);
            $this->em->flush();
        }
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token): ?Response
    {
        /*if( $request->isXmlHttpRequest() ) {
            $response = new JsonResponse( array( 'success' => true, 'username' => $token->getUsername() ) );
        } else {
            $response = parent::onAuthenticationSuccess( $request, $token );
        }*/
        $this->setOnline($request, $token);
        #$_kt=$this->container->getParameter('kaitek_framework.kullanimtipi');
        $locale = $request->getSession()->get('_locale');
        //$url = $locale==''||$locale=='tr'?'kaitek_framework_home_get':'kaitek_framework_home_get_locale';
        $url = 'kaitek-framework-home-get';
        if($this->authChecker->isGranted('ROLE_USER')) {
            #if($_kt=="intranet"){
            #    $url = 'kaitek_user_home';
            #}else{
            //$url = $locale==''||$locale=='tr'?'kaitek_framework_home_get':'kaitek_framework_home_get_locale';
            $url = 'kaitek-framework-home-get';
            #}
        }
        if($this->authChecker->isGranted('ROLE_ADMIN')) {
            $_kt = $this->container->getParameter('kaitek_framework.kullanimtipi');
            $url = ($_kt == 'intranet' ? 'kaitek-framework-home-get' : 'kaitek-framework-admin-home-get');
        }
        if($request->isXmlHttpRequest()) {
            $user = $token->getUser();
            $menus=array();

            /*
             * controller servis olarak kullanılacak ise
            $sc=$this->container->get('kaiframework.servicecontroller');
            $menus=$sc->getMenu();
             */

            $repository=$this->em->getRepository('KaitekFrameworkBundle:Menu');
            $em = $this->em;
            $af = $this->authChecker->isGranted('IS_AUTHENTICATED_FULLY');
            $ru = $this->authChecker->isGranted('ROLE_USER');
            $ra = $this->authChecker->isGranted('ROLE_ADMIN');
            $tr = $this->container->get('translator');
            $userId = $user->getId();
            $menus = $repository->loadUserMenuQB($em, $af, $ru, $ra, $tr, $userId);

            $menuItem = false;
            if($user != null) {
                $fullname = $user->getFullname();
                $fullname = $fullname != "" && $fullname != null ? $fullname : $user->getUsername();
                $roles = $user->getRoles();

                $module = $request->get('editModule');
                if($module != "") {
                    foreach($menus as $menuI) {
                        if($menuI['modulename'] == $module) {
                            $menuItem = $menuI;
                            break;
                        } elseif(isset($menuI['items']) && count($menuI['items'])>0) {
                            foreach($menuI['items'] as $menuJ) {
                                if($menuJ['modulename'] == $module) {
                                    $menuItem = $menuJ;
                                    break;
                                }
                            }
                        }
                    }
                }
            }

            $response = new JsonResponse(array( 'success' => true, '_name'=>$fullname, '_role'=>$roles, '_avatar'=>$user->getAvatar(), '_loginId'=>$user->getUsername(),'menu'=>$menus,'editModule'=>$menuItem));
        } else {
            $response = new RedirectResponse($this->router->generate($url, array('_locale' => $locale)));
        }
        return $response;
        #$response = parent::onAuthenticationSuccess( $request, $token );
        #return $response;
    }
}
